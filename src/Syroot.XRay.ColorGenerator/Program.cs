﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace Syroot.XRay.ColorGenerator
{
    /// <summary>
    /// Main class of the program containing the application entry point.
    /// </summary>
    internal class Program
    {
        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        [STAThread]
        private static void Main(string[] args)
        {
            // Get the list of system color names to exclude them from the generated colors
            List<string> systemColorNames = new List<string>();
            foreach (PropertyInfo property in typeof(SystemColors).GetProperties())
            {
                if (property.PropertyType.FullName == "System.Drawing.Color")
                    systemColorNames.Add(property.Name);
            }

            StringBuilder sb = new StringBuilder();

            foreach (KnownColor knownColor in Enum.GetValues(typeof(KnownColor)))
            {
                string colorName = Enum.GetName(typeof(KnownColor), knownColor);
                if (systemColorNames.Contains(colorName))
                    continue;

                Color color = Color.FromKnownColor(knownColor);

                string rgbaText = String.Format("#{0:X2}{1:X2}{2:X2}{3:X2}",
                    color.R, color.G, color.B, color.A);
                float r = (float)Math.Round(color.R / 255f, 3);
                float g = (float)Math.Round(color.G / 255f, 3);
                float b = (float)Math.Round(color.B / 255f, 3);
                float a = (float)Math.Round(color.A / 255f, 3);

                sb.AppendLine("/// <summary>");
                sb.AppendLine(String.Format("/// A <see cref=\"Color\"/> with an RGBA value of {0}.", rgbaText));
                sb.AppendLine("/// </summary>");
                sb.Append(String.Format("public static readonly Color {0} = new Color(",
                    Enum.GetName(typeof(KnownColor), knownColor)));
                if (color.A == 255)
                    sb.Append($"{color.R}, {color.G}, {color.B}");
                else
                    sb.Append($"{color.R}, {color.G}, {color.B}, {color.A}");
                sb.AppendLine(");");
                sb.AppendLine();

                Clipboard.SetText(sb.ToString().Trim());
            }
        }
    }
}
