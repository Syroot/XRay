﻿#version 330

uniform sampler2D imageTexture;

in vec2 passTexCoords;
in vec4 passColor;

out vec4 outColor;

void main()
{
    outColor = texture(imageTexture, passTexCoords) * passColor;
}
