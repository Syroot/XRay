﻿#version 330

uniform mat4 viewMatrix; // The screen pixel transformation

layout(location = 0) in vec2 inPosition;
layout(location = 1) in vec2 inTexCoords;
layout(location = 2) in vec4 inColor;

out vec2 passTexCoords;
out vec4 passColor;

void main()
{
    // Pass through the texture coordinates and color.
    passTexCoords = inTexCoords;
    passColor = inColor;

    // Transform to pixel- and image-relative coordinates.
    gl_Position = viewMatrix * vec4(inPosition, 0.0f, 1.0f);
}
