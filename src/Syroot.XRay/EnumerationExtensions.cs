﻿using System;

namespace Syroot.XRay
{
    /// <summary>
    /// Represents a collection of static extension method for objects of the enumeration type.
    /// </summary>
    internal static class EnumerationExtensions
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Gets a value indicating whether the given flag set contains the provided value.
        /// </summary>
        /// <typeparam name="T">The type of the enumeration.</typeparam>
        /// <param name="type">The extended enumeration.</param>
        /// <param name="value">The value to check for.</param>
        /// <returns><c>true</c> if the flag set contains the value.</returns>
        internal static bool Has<T>(this Enum type, T value)
        {
            try
            {
                return ((int)(object)type & (int)(object)value) == (int)(object)value;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the given flag set is exactly of the provided value.
        /// </summary>
        /// <typeparam name="T">The type of the enumeration.</typeparam>
        /// <param name="type">The extended enumeration.</param>
        /// <param name="value">The value to check for.</param>
        /// <returns><c>true</c> if the flag set is exactly the value.</returns>
        internal static bool Is<T>(this Enum type, T value)
        {
            try
            {
                return (int)(object)type == (int)(object)value;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Adds a value to the given flag set and returns the resulting flag set.
        /// </summary>
        /// <typeparam name="T">The type of the enumeration.</typeparam>
        /// <param name="type">The extended enumeration.</param>
        /// <param name="value">The value to add.</param>
        /// <returns>The resulting flag set.</returns>
        internal static T Add<T>(this Enum type, T value)
        {
            try
            {
                return (T)(object)((int)(object)type | (int)(object)value);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(string.Format("Could not append value from enumerated type '{0}'.",
                    typeof(T).Name), ex);
            }
        }

        /// <summary>
        /// Removes a value from the given flag set and returns the resulting flag set.
        /// </summary>
        /// <typeparam name="T">The type of the enumeration.</typeparam>
        /// <param name="type">The extended enumeration.</param>
        /// <param name="value">The value to remove.</param>
        /// <returns>The resulting flag set.</returns>
        internal static T Remove<T>(this Enum type, T value)
        {
            try
            {
                return (T)(object)((int)(object)type & ~(int)(object)value);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(string.Format("Could not remove value from enumerated type '{0}'.",
                    typeof(T).Name), ex);
            }
        }
    }
}
