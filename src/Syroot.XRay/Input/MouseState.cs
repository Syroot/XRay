﻿using Syroot.Maths;

namespace Syroot.XRay.Input
{
    /// <summary>
    /// Represents the state of the mouse buttons of a mouse at a single frame.
    /// </summary>
    internal class MouseState
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the position of the mouse cursor.
        /// </summary>
        internal Vector2 Position;

        /// <summary>
        /// Gets the difference to the previous mouse cursor position.
        /// </summary>
        internal Vector2 Delta;

        /// <summary>
        /// Gets the position of the mouse without operating system specific cursor acceleration.
        /// </summary>
        internal Vector2 RawPosition;

        /// <summary>
        /// Gets the difference to the previous mouse position without operating system specific cursor acceleration.
        /// </summary>
        internal Vector2 RawDelta;

        /// <summary>
        /// Gets the array of pressed mouse button states.
        /// </summary>
        internal bool[] PressedStates;

        /// <summary>
        /// Gets the array of held down mouse button states.
        /// </summary>
        internal bool[] DownStates;

        /// <summary>
        /// Gets the array of released mouse button states.
        /// </summary>
        internal bool[] ReleasedStates;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="MouseState"/> class with all states being 0 or false.
        /// </summary>
        internal MouseState()
        {
            PressedStates = new bool[(int)MouseButton.LastButton];
            DownStates = new bool[(int)MouseButton.LastButton];
            ReleasedStates = new bool[(int)MouseButton.LastButton];
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Compares the currently stored states in this instance with the new <see cref="OpenTK.Input.MouseState"/>
        /// to retrieve a new instance of the <see cref="MouseState"/> class which represents the new mouse states.
        /// </summary>
        /// <param name="openTkMouseState">The <see cref="OpenTK.Input.MouseState"/> to compare the current instance
        /// with.</param>
        /// <param name="position">The operating system-accelerated, current mouse cursor position.</param>
        /// <returns>The resulting <see cref="MouseState"/> instance representing the current state.</returns>
        internal MouseState Compare(OpenTK.Input.MouseState openTkMouseState, Vector2 position)
        {
            MouseState newState = new MouseState();

            // Update the mouse coordinates and button states.
            for (int i = 0; i < (int)MouseButton.LastButton; i++)
            {
                // Position and delta to previous one.
                newState.Position = position;
                newState.Delta = newState.Position - Position;

                // Raw position and delta to previous one.
                newState.RawPosition = new Vector2(openTkMouseState.X, openTkMouseState.Y);
                newState.RawDelta = newState.RawPosition - RawPosition;
                
                OpenTK.Input.MouseButton openTkMouseButton = (OpenTK.Input.MouseButton)i;
                bool isDown = openTkMouseState.IsButtonDown(openTkMouseButton);
                bool isUp = openTkMouseState.IsButtonUp(openTkMouseButton);

                // Pressed state: Pressed for the first time.
                newState.PressedStates[i] = isDown && !DownStates[i];

                // Down state: Pressed for the first time or held down continuously.
                newState.DownStates[i] = isDown;

                // Released state: Not pressed anymore for the first time.
                newState.ReleasedStates[i] = DownStates[i] && isUp;
            }

            return newState;
        }

        /// <summary>
        /// Returns an instance with released mouse button states and deltas set to 0. Useful if the game window goes
        /// unfocussed, so only the previous cursor coordinates are kept.
        /// </summary>
        /// <returns>The instance with released mouse button states and deltas set to 0.</returns>
        public MouseState GetUnfocussedInstance()
        {
            MouseState newState = new MouseState()
            {
                Position = Position,
                RawPosition = RawPosition
            };
            return newState;
        }
    }
}