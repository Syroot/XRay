﻿using System;

namespace Syroot.XRay.Input
{
    /// <summary>
    /// Passed with the <see cref="Keyboard.KeyPressed"/> event.
    /// </summary>
    public class KeyPressedEventArgs : EventArgs
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyPressedEventArgs"/> class with the given parameters.
        /// </summary>
        /// <param name="character">The character pressed with the keyboard input.</param>
        public KeyPressedEventArgs(char character)
        {
            Character = character;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the character which was input by the user..
        /// </summary>
        public char Character
        {
            get;
            private set;
        }
    }
}
