﻿using System;
using OpenTK;

namespace Syroot.XRay.Input
{
    /// <summary>
    /// Represents the state of all keyboard combined which can be queried by the user.
    /// </summary>
    public static class Keyboard
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------
        
        private static KeyboardState _keyboardState;

        // ---- EVENTS -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Occurs when the user pressed a button to input a character of text.
        /// </summary>
        public static event EventHandler<KeyPressedEventArgs> KeyPressed;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets a value indicating whether the keyboard states are recorded globally even when the game window
        /// is not active, or only if the game window is focused.
        /// </summary>
        public static bool CatchGlobally
        {
            get;
            set;
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Gets a value indicating whether the key is pressed for the first time, and was not pressed in the previous
        /// frame.
        /// </summary>
        /// <param name="key">The <see cref="Key"/> to check if it is pressed.</param>
        /// <returns><c>true</c> if the key is pressed, otherwise <c>false</c>.</returns>
        public static bool IsPressed(Key key)
        {
            return _keyboardState.PressedStates[(int)key];
        }

        /// <summary>
        /// Gets a value indicating whether the key is held down for the first time or continuously.
        /// </summary>
        /// <param name="key">The <see cref="Key"/> to check if it is held down.</param>
        /// <returns><c>true</c> if the key is held down, otherwise <c>false</c>.</returns>
        public static bool IsDown(Key key)
        {
            return _keyboardState.DownStates[(int)key];
        }

        /// <summary>
        /// Gets a value indicating whether the key is released for the first time, and was held down in the previous
        /// frame.
        /// </summary>
        /// <param name="key">The <see cref="Key"/> to check if it is released.</param>
        /// <returns><c>true</c> if the key is released, otherwise <c>false</c>.</returns>
        public static bool IsReleased(Key key)
        {
            return _keyboardState.ReleasedStates[(int)key];
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Called when the <see cref="Game"/> is run and the window initialized.
        /// </summary>
        internal static void Initialize()
        {
            // Attach to the game.
            Game.Update += Game_Update;
            Game.Window.KeyPress += Window_KeyPress;

            // Get an initial state with all key states set to false.
            _keyboardState = new KeyboardState();
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void OnKeyPressed(char character)
        {
            KeyPressed?.Invoke(null, new KeyPressedEventArgs(character));
        }

        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

        private static void Game_Update(object sender, EventArgs e)
        {
            // If the window is inactive, simulate not retrieving keyboard input by setting all states to false.
            if (Game.Window.Focused || CatchGlobally)
            {
                _keyboardState = _keyboardState.Compare(OpenTK.Input.Keyboard.GetState());
            }
            else
            {
                _keyboardState = new KeyboardState();
            }
        }

        private static void Window_KeyPress(object sender, KeyPressEventArgs e)
        {
            OnKeyPressed(e.KeyChar);
        }
    }
}
