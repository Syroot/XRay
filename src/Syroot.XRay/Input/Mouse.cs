﻿using System;
using System.Drawing;
using Syroot.Maths;
using Syroot.XRay.Maths;

namespace Syroot.XRay.Input
{
    /// <summary>
    /// Represents the state of all mice combined which can be queried by the user.
    /// </summary>
    public static class Mouse
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static Vector2    _mousePosition;
        private static MouseState _mouseState;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets a value indicating whether the mouse states are recorded globally even when the game window is
        /// not active, or only if the game window is focused.
        /// </summary>
        public static bool CatchGlobally
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the operating system cursor is visible or hidden.
        /// </summary>
        public static bool CursorVisible
        {
            get { return Game.Window.CursorVisible; }
            set { Game.Window.CursorVisible = value; }
        }
        
        /// <summary>
        /// Gets or sets the position of the mouse cursor relative to the window client area. Coordinates are only
        /// updated when the cursor is inside the client window (even if <see cref="CatchGlobally"/> is set to
        /// <c>true</c>), e.g., the coordinates never lie outside of the window. Setting the cursor position only has
        /// effect if the window is focused, or if <see cref="CatchGlobally"/> is set to <c>true</c>.
        /// </summary>
        public static Vector2 Position
        {
            get
            {
                return _mouseState.Position;
            }
            set
            {
                if (Game.Window.Focused || CatchGlobally)
                {
                    Vector2 screenPosition = Game.Window.PointToScreen(value.ToDrawingPoint()).ToVector2();
                    OpenTK.Input.Mouse.SetPosition(screenPosition.X, screenPosition.Y);
                }
            }
        }

        /// <summary>
        /// Gets the difference to the previous mouse cursor position.
        /// </summary>
        public static Vector2 Delta
        {
            get { return _mouseState.Delta; }
        }

        /// <summary>
        /// Gets the mouse coordinates relative to the game window without operating system specific cursor
        /// acceleration.
        /// </summary>
        public static Vector2 RawPosition
        {
            get
            {
                return _mouseState.RawPosition;
            }
        }

        /// <summary>
        /// Gets the delta relative to the last mouse position. 
        /// </summary>
        public static Vector2 RawDelta
        {
            get
            {
                return _mouseState.RawDelta;
            }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Gets a value indicating whether the mouse button is pressed for the first time, and was not pressed in the
        /// previous frame.
        /// </summary>
        /// <param name="mouseButton">The <see cref="MouseButton"/> to check if it is pressed.</param>
        /// <returns><c>true</c> if the mouse button is pressed, otherwise <c>false</c>.</returns>
        public static bool IsPressed(MouseButton mouseButton)
        {
            return _mouseState.PressedStates[(int)mouseButton];
        }

        /// <summary>
        /// Gets a value indicating whether the mouse button is held down for the first time or continuously.
        /// </summary>
        /// <param name="mouseButton">The <see cref="MouseButton"/> to check if it is held down.</param>
        /// <returns><c>true</c> if the mouse button is held down, otherwise <c>false</c>.</returns>
        public static bool IsDown(MouseButton mouseButton)
        {
            return _mouseState.DownStates[(int)mouseButton];
        }

        /// <summary>
        /// Gets a value indicating whether the mouse button is released for the first time, and was held down in the previous
        /// frame.
        /// </summary>
        /// <param name="mouseButton">The <see cref="MouseButton"/> to check if it is released.</param>
        /// <returns><c>true</c> if the mouse button is released, otherwise <c>false</c>.</returns>
        public static bool IsReleased(MouseButton mouseButton)
        {
            return _mouseState.ReleasedStates[(int)mouseButton];
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Called when the <see cref="Game"/> is run and the window initialized.
        /// </summary>
        internal static void Initialize()
        {
            // Attach to the game.
            Game.Update += Game_Update;
            Game.Window.MouseMove += Window_MouseMove;

            // Get an initial state with all key states set to false.
            _mouseState = new MouseState();
        }

        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

        private static void Game_Update(object sender, EventArgs e)
        {
            // If the window is inactive, simulate not retrieving mouse input by setting all states to false.
            if (Game.Window.Focused || CatchGlobally)
            {
                _mouseState = _mouseState.Compare(OpenTK.Input.Mouse.GetState(), _mousePosition);
            }
            else
            {
                _mouseState = _mouseState.GetUnfocussedInstance();
            }
        }

        private static void Window_MouseMove(object sender, OpenTK.Input.MouseMoveEventArgs e)
        {
            _mousePosition = e.Position.ToVector2();
        }
    }
}
