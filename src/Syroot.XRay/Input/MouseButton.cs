﻿namespace Syroot.XRay.Input
{
    /// <summary>
    /// Represents the available mouse buttons.
    /// </summary>
    public enum MouseButton
    {
        /// <summary>
        /// The left mouse button.
        /// </summary>
        Left = 0,

        /// <summary>
        /// The middle mouse button.
        /// </summary>
        Middle = 1,

        /// <summary>
        /// The right mouse button.
        /// </summary>
        Right = 2,

        /// <summary>
        /// The first additional mouse button.
        /// </summary>
        Button1 = 3,

        /// <summary>
        /// The second additional mouse button.
        /// </summary>
        Button2 = 4,

        /// <summary>
        /// The third additional mouse button.
        /// </summary>
        Button3 = 5,

        /// <summary>
        /// The fourth additional mouse button.
        /// </summary>
        Button4 = 6,

        /// <summary>
        /// The fifth additional mouse button.
        /// </summary>
        Button5 = 7,

        /// <summary>
        /// The sixth additional mouse button.
        /// </summary>
        Button6 = 8,

        /// <summary>
        /// The seventh additional mouse button.
        /// </summary>
        Button7 = 9,

        /// <summary>
        /// The eighth additional mouse button.
        /// </summary>
        Button8 = 10,

        /// <summary>
        /// The ninth additional mouse button.
        /// </summary>
        Button9 = 11,

        /// <summary>
        /// Indicates the last available mouse button.
        /// </summary>
        LastButton = 12
    }
}
