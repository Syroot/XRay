﻿namespace Syroot.XRay.Input
{
    /// <summary>
    /// Represents the state of the keys of a keyboard at a single frame.
    /// </summary>
    internal class KeyboardState
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the array of pressed key states.
        /// </summary>
        internal bool[] PressedStates;

        /// <summary>
        /// Gets the array of held down key states.
        /// </summary>
        internal bool[] DownStates;

        /// <summary>
        /// Gets the array of released key states.
        /// </summary>
        internal bool[] ReleasedStates;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyboardState"/> class with all states being false.
        /// </summary>
        internal KeyboardState()
        {
            PressedStates = new bool[(int)Key.Last];
            DownStates = new bool[(int)Key.Last];
            ReleasedStates = new bool[(int)Key.Last];
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Compares the currently stored states in this instance with the new <see cref="OpenTK.Input.KeyboardState"/>
        /// to retrieve a new instance of the <see cref="KeyboardState"/> class which represents the new key states.
        /// </summary>
        /// <param name="openTkKeyboardState">The <see cref="OpenTK.Input.KeyboardState"/> to compare the current
        /// instance with.</param>
        /// <returns>The resulting <see cref="KeyboardState"/> instance representing the current state.</returns>
        internal KeyboardState Compare(OpenTK.Input.KeyboardState openTkKeyboardState)
        {
            KeyboardState newState = new KeyboardState();

            // Update the key states.
            for (int i = 0; i < (int)Key.Last; i++)
            {
                OpenTK.Input.Key openTkKey = (OpenTK.Input.Key)i;
                bool isDown = openTkKeyboardState.IsKeyDown(openTkKey);
                bool isUp = openTkKeyboardState.IsKeyUp(openTkKey);

                // Pressed state: Pressed for the first time.
                newState.PressedStates[i] = isDown && !DownStates[i];

                // Down state: Pressed for the first time or held down continuously.
                newState.DownStates[i] = isDown;

                // Released state: Not pressed anymore for the first time.
                newState.ReleasedStates[i] = DownStates[i] && isUp;
            }
            
            return newState;
        }
    }
}
