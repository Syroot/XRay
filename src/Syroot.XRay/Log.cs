﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Syroot.XRay.Maths;

namespace Syroot.XRay
{
    /// <summary>
    /// Represents a log keeping track of the most important changes in a game.
    /// </summary>
    internal static class Log
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static Dictionary<LogCategory, ConsoleColor> _categoryColors;
        private static int _headerWidth;
        private static int _longestCategoryNameWidth;

        private static readonly object _writeMutex = new object();

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets a value indicating whether logging is enabled in general or not.
        /// </summary>
        internal static bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets the set of categories which are currently being logged.
        /// </summary>
        internal static LogCategory EnabledCategories { get; set; }

        /// <summary>
        /// Gets or sets the set of priorities which are currently being logged.
        /// </summary>
        internal static LogPriority EnabledPriorities { get; set; }

        /// <summary>
        /// Gets or sets the maximum width a log message may have before it is line-wrapped.
        /// </summary>
        internal static int LineWidth { get; set; }

        /// <summary>
        /// Gets or sets the text used to delimit the formatted log columns.
        /// </summary>
        internal static string Separator { get; set; }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Called when the <see cref="Game"/> is run and the window initialized.
        /// </summary>
        internal static void Initialize()
        {
            // Set default values.
            Enabled = true;
#if DEBUG
            EnabledCategories = LogCategory.General | LogCategory.OpenAL;
            EnabledPriorities = LogPriority.Warning;
#endif
            LineWidth = 120;
            Separator = " ";

            _categoryColors = new Dictionary<LogCategory, ConsoleColor>
            {
                { LogCategory.General, ConsoleColor.White },
                { LogCategory.Audio, ConsoleColor.Yellow },
                { LogCategory.Video, ConsoleColor.Green },
                { LogCategory.OpenAL, ConsoleColor.DarkYellow },
                { LogCategory.OpenGL, ConsoleColor.DarkGreen }
            };

            // Find out the width of a row header.
            _headerWidth = 1/*Priority character*/ + Separator.Length + 9/*Time code*/ + Separator.Length;

            foreach (LogCategory category in (LogCategory[])Enum.GetValues(typeof(LogCategory)))
            {
                int nameLength = category.ToString().Length;
                if (_longestCategoryNameWidth < nameLength)
                {
                    _longestCategoryNameWidth = nameLength;
                }
            }
            _headerWidth += _longestCategoryNameWidth + Separator.Length;
        }

        /// <summary>
        /// Logs a message with the given details.
        /// </summary>
        /// <param name="priority">The <see cref="LogPriority"/> for this message.</param>
        /// <param name="category">The <see cref="LogCategory"/> the message falls in.</param>
        /// <param name="format">A composite format string as the message to write.</param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        [Conditional("DEBUG")]
        internal static void Write(LogPriority priority, LogCategory category, string format, params object[] args)
        {
            if (Enabled && EnabledCategories.Has(category) && EnabledPriorities.Has(priority))
            {
                List<string> lines = GetMessageLines(format, args);

                // Build a single string out of the lines.
                StringBuilder messageBuilder = new StringBuilder(GetHeaderText(priority, category));
                messageBuilder.AppendLine(lines[0]);
                for (int i = 1; i < lines.Count; i++)
                {
                    messageBuilder.Append(new string(' ', _headerWidth));
                    messageBuilder.AppendLine(lines[i]);
                }
                string message = messageBuilder.ToString();

                lock (_writeMutex)
                {
                    // Write to the debug panel.
                    Debug.Write(message);

                    // Write to the console (backup and restore the colors).
                    ConsoleColor previousForeColor = Console.ForegroundColor;
                    Console.ForegroundColor = _categoryColors[category];
                    Console.Write(message);
                    Console.ForegroundColor = previousForeColor;
                }
            }
        }

        /// <summary>
        /// Shortcut method to log an informational message with the given details.
        /// </summary>
        /// <param name="category">The <see cref="LogCategory"/> the message falls in.</param>
        /// <param name="format">A composite format string as the message to write.</param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        [Conditional("DEBUG")]
        internal static void WriteInfo(LogCategory category, string format, params object[] args)
        {
            Write(LogPriority.Information, category, format, args);
        }

        /// <summary>
        /// Shortcut method to log a warning message with the given details.
        /// </summary>
        /// <param name="category">The <see cref="LogCategory"/> the message falls in.</param>
        /// <param name="format">A composite format string as the message to write.</param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        [Conditional("DEBUG")]
        internal static void WriteWarn(LogCategory category, string format, params object[] args)
        {
            Write(LogPriority.Warning, category, format, args);
        }

        /// <summary>
        /// Shortcut method to log an error message with the given details.
        /// </summary>
        /// <param name="category">The <see cref="LogCategory"/> the message falls in.</param>
        /// <param name="format">A composite format string as the message to write.</param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        [Conditional("DEBUG")]
        internal static void WriteErr(LogCategory category, string format, params object[] args)
        {
            Write(LogPriority.Error, category, format, args);
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static T GetAllEnumFlags<T>()
        {
            int flagSet = 0;
            foreach (object value in Enum.GetValues(typeof(T)))
                flagSet += (int)value;
            return (T)(object)flagSet;
        }

        private static List<string> GetMessageLines(string format, object[] args)
        {
            // Split the lines of the message to log.
            string message = string.Format(format, args);
            List<string> lines = new List<string>(message.Split(new string[] { Environment.NewLine },
                StringSplitOptions.None));

            // Split up the message again to provide line wrapping.
            int messageWidth = LineWidth - _headerWidth;
            for (int i = 0; i < lines.Count; i++)
            {
                string line = lines[i];
                if (line.Length > messageWidth)
                {
                    // Wrap the overhang into a new (inserted) item in the list.
                    lines[i] = line.Substring(0, messageWidth);
                    lines.Insert(i + 1, line.Substring(messageWidth));
                }
            }

            return lines;
        }

        private static string GetHeaderText(LogPriority priority, LogCategory category)
        {
            StringBuilder header = new StringBuilder();

            // Priority
            header.Append(priority.ToString()[0]);
            header.Append(Separator);

            // Timecode
            header.Append(Time.SinceStart.ToString(@"mm\:ss\.fff"));
            header.Append(Separator);

            // Category
            header.Append(category.ToString().PadRight(_longestCategoryNameWidth));
            header.Append(Separator);

            return header.ToString();
        }
    }

    /// <summary>
    /// Represents the possible categories a message can have.
    /// </summary>
    [Flags]
    internal enum LogCategory
    {
        /// <summary>
        /// Represents general messages not related to any specific category.
        /// </summary>
        General = 1 << 0,

        /// <summary>
        /// Represents audio-related messages.
        /// </summary>
        Audio = 1 << 1,

        /// <summary>
        /// Represents video-related messages.
        /// </summary>
        Video = 1 << 2,

        /// <summary>
        /// Represents core-internal OpenGL messages.
        /// </summary>
        OpenGL = 1 << 3,

        /// <summary>
        /// Represents core-internal OpenAL messages.
        /// </summary>
        OpenAL = 1 << 4,
    }

    /// <summary>
    /// Represents the possible priorities a message can have.
    /// </summary>
    [Flags]
    internal enum LogPriority
    {
        /// <summary>
        /// Represents non-interrupting, informational messages.
        /// </summary>
        Information = 1 << 0,

        /// <summary>
        /// Represents errors which can be ignored but cause a sub-optimal state.
        /// </summary>
        Warning = 1 << 1,

        /// <summary>
        /// Represents errors which stop the game from executing as desired.
        /// </summary>
        Error = 1 << 2
    }
}
