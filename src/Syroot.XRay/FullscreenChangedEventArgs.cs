﻿using System;

namespace Syroot.XRay
{
    /// <summary>
    /// Passed with the <see cref="Game.FullscreenChanged"/> event.
    /// </summary>
    public class FullscreenChangedEventArgs : EventArgs
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="FullscreenChangedEventArgs"/> class with the given parameters.
        /// </summary>
        /// <param name="isFullscreen"><c>true</c> if the game is running in fullscreen now.</param>
        public FullscreenChangedEventArgs(bool isFullscreen)
        {
            IsFullscreen = isFullscreen;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets a value indicating whether the game is running in fullscreen now.
        /// </summary>
        public bool IsFullscreen
        {
            get;
            private set;
        }
    }
}
