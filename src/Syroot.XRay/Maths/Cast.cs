﻿using Syroot.Maths;

namespace Syroot.XRay.Maths
{
    /// <summary>
    /// Represents a collection of extension methods to convert engine types to API specific ones.
    /// </summary>
    internal static class Cast
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static System.Drawing.Point ToDrawingPoint(this Vector2 vector2)
        {
            return new System.Drawing.Point(vector2.X, vector2.Y);
        }

        internal static System.Drawing.Size ToDrawingSize(this Vector2 vector2)
        {
            return new System.Drawing.Size(vector2.X, vector2.Y);
        }

        internal static OpenTK.Graphics.Color4 ToOpenTKColor4(this ColorF colorF)
        {
            return new OpenTK.Graphics.Color4(colorF.R, colorF.G, colorF.B, colorF.A);
        }

        internal static OpenTK.Matrix3 ToOpenTKMatrix3(this Matrix3 matrix3)
        {
            return new OpenTK.Matrix3(
                matrix3.M11, matrix3.M12, matrix3.M13,
                matrix3.M21, matrix3.M22, matrix3.M23,
                matrix3.M31, matrix3.M32, matrix3.M33);
        }

        internal static OpenTK.Matrix4 ToOpenTKMatrix4(this Matrix4 matrix4)
        {
            return new OpenTK.Matrix4(
                matrix4.M11, matrix4.M12, matrix4.M13, matrix4.M14,
                matrix4.M21, matrix4.M22, matrix4.M23, matrix4.M24,
                matrix4.M31, matrix4.M32, matrix4.M33, matrix4.M34,
                matrix4.M41, matrix4.M42, matrix4.M43, matrix4.M44);
        }

        internal static OpenTK.Vector2 ToOpenTKVector2(this Vector2 vector2)
        {
            return new OpenTK.Vector2(vector2.X, vector2.Y);
        }

        internal static OpenTK.Vector2 ToOpenTKVector2(this Vector2F vector2f)
        {
            return new OpenTK.Vector2(vector2f.X, vector2f.Y);
        }

        internal static Vector2 ToVector2(this System.Drawing.Point point)
        {
            return new Vector2(point.X, point.Y);
        }

        internal static Vector2 ToVector2(this System.Drawing.Size size)
        {
            return new Vector2(size.Width, size.Height);
        }
    }
}
