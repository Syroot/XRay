﻿using System;
using System.Diagnostics;

namespace Syroot.XRay.Maths
{
    /// <summary>
    /// Represents a timekeeper providing information about the elapsed game time.
    /// </summary>
    public static class Time
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the framerate to which the game is targetted to run in.
        /// </summary>
        public const int OptimalFramesPerSecond = 60;

        /// <summary>
        /// Gets the <see cref="TimeSpan.Ticks"/> which would have passed in an optimal <see cref="UpdateTime"/> if the
        /// game runs at <see cref="OptimalFramesPerSecond"/>.
        /// </summary>
        public const long OptimalUpdateTicks = 10000000/*1 second*/ / OptimalFramesPerSecond/*FPS*/;

        /// <summary>
        /// Gets the optimal <see cref="UpdateTime"/> as if the game would be running fluently at
        /// <see cref="OptimalFramesPerSecond"/>.
        /// </summary>
        public static readonly TimeSpan OptimalUpdateTime = new TimeSpan(OptimalUpdateTicks);

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static Stopwatch _stopwatch;
        private static DateTime  _fpsMeasurementTime;
        private static int       _fpsCounter;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the amount of time elapsed since the game started running, e.g. between the call to
        /// <see cref="Game.Run()"/> and the last <see cref="Game.Update"/> event.
        /// </summary>
        public static TimeSpan SinceStart
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the amount of time since the last <see cref="Game.Update"/> event.
        /// </summary>
        public static TimeSpan SinceUpdate
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the point in UTC time when the game has started running, e.g. the <see cref="Game.Run()"/> method
        /// has been called.
        /// </summary>
        public static DateTime StartTime
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the point in UTC time of the last <see cref="Game.Update"/> event.
        /// </summary>
        /// <remarks>
        /// Do not use this to calculate the amount of time a frame needed, as <see cref="DateTime"/> is not exact
        /// enough for this. Use the <see cref="SinceUpdate"/> property instead, which calculates the TimeSpan with a
        /// <see cref="Stopwatch"/> instance internally and thus is much more exact.
        /// </remarks>
        public static DateTime UpdateTime
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the amount of frames which were rendered in the last second.
        /// </summary>
        public static float FramesPerSecond
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets a value being 1f if the game runs at steady <see cref="OptimalFramesPerSecond"/>. It is bigger
        /// when the game is running on less FPS.
        /// </summary>
        public static float Performance
        {
            get;
            private set;
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------
        
        /// <summary>
        /// Called when the <see cref="Game"/> is run and the window initialized.
        /// </summary>
        internal static void Initialize()
        {
            // Attach to the game.
            Game.Update += Game_Update;

            _stopwatch = new Stopwatch();
            StartTime = DateTime.UtcNow;
            UpdateTime = StartTime;
        }

        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

        private static void Game_Update(object sender, EventArgs e)
        {
            // Calculate TimeSpans since the last call to Update().
            // _stopwatch.Elapsed is the amount of time elapsed since the init of this class, rather than being
            // the milliseconds elapsed since the last call to Stopwatch.Start().
            _stopwatch.Stop();
            SinceUpdate = _stopwatch.Elapsed - SinceStart;
            SinceStart = _stopwatch.Elapsed;
            _stopwatch.Start();

            // Store the current point in time.
            UpdateTime = DateTime.UtcNow;

            // Measure the frames rendered in the last second.
            double secondsSinceFpsMeasurement = (UpdateTime - _fpsMeasurementTime).TotalSeconds;
            if (secondsSinceFpsMeasurement >= 1.0)
            {
                FramesPerSecond = (float)(_fpsCounter / secondsSinceFpsMeasurement);
                _fpsMeasurementTime = UpdateTime;
                _fpsCounter = 0;
            }
            _fpsCounter++;

            // Calculate the performance.
            Performance = SinceUpdate.Ticks / (float)OptimalUpdateTicks;
        }
    }
}
