﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Syroot.XRay.Audio;
using Syroot.XRay.Input;
using Syroot.XRay.Maths;
using Syroot.XRay.Video;
using Vector2 = Syroot.Maths.Vector2;

namespace Syroot.XRay
{
    /// <summary>
    /// Represents the core of the game application and contains the logic to run and update it.
    /// </summary>
    public static class Game
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static AudioSystem _audioSystem;

        private static Icon    _icon;
        private static string  _title;
        private static bool    _resizable;
        private static bool    _fullscreen;
        private static Vector2 _resolution;

        private static bool        _isInFullscreenResolution;
        private static WindowState _windowedState;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes static members of the <see cref="Game"/> class.
        /// </summary>
        static Game()
        {
            Resources = new ResourceLoader();

            // Set default values.
            _icon = Resources.GetIcon("Icon.ico");
            _title = "Syroot XRay Game";
            _resizable = true;
            _resolution = new Vector2(800, 600);
        }

        // ---- EVENTS -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Occurs when the game's underlying engine has been started and static resources are about to be loaded.
        /// </summary>
        public static event EventHandler Start;

        /// <summary>
        /// Occurs when physics and the state of objects are updated.
        /// </summary>
        public static event EventHandler Update;

        /// <summary>
        /// Occurs when objects are about to be drawn on screen each frame.
        /// </summary>
        public static event EventHandler Render;

        /// <summary>
        /// Occurs when the fullscreen state has changed.
        /// </summary>
        public static event EventHandler<FullscreenChangedEventArgs> FullscreenChanged;

        /// <summary>
        /// Occurs when the game resolution has changed.
        /// </summary>
        public static event EventHandler<ResolutionChangedEventArgs> ResolutionChanged;

        /// <summary>
        /// Occurs when the game engine is about to shut down and managers are destroyed.
        /// </summary>
        internal static event EventHandler Disposing;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the icon displayed in windows created by the game.
        /// </summary>
        public static Icon Icon
        {
            get
            {
                return _icon;
            }
            set
            {
                _icon = value;
                if (Window != null)
                {
                    Window.Icon = _icon;
                }
            }
        }

        /// <summary>
        /// Gets or sets the text displayed in windows created by the game.
        /// </summary>
        public static string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
                if (Window != null)
                {
                    Window.Title = _title;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the game window can be resized.
        /// </summary>
        public static bool Resizable
        {
            get
            {
                return _resizable;
            }
            set
            {
                _resizable = value;
                if (Window != null)
                {
                    Window.WindowBorder = _resizable ? WindowBorder.Resizable : WindowBorder.Fixed;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the game is running in fullscreen or windowed mode.
        /// </summary>
        public static bool Fullscreen
        {
            get
            {
                return _fullscreen;
            }
            set
            {
                if (_fullscreen != value)
                {
                    _fullscreen = value;

                    SetFullscreen(_fullscreen);
                    OnFullscreenChanged(_fullscreen);
                }
            }
        }

        /// <summary>
        /// Gets or sets the screen resolution of this game in pixels.
        /// </summary>
        public static Vector2 Resolution
        {
            get
            {
                return _resolution;
            }
            set
            {
                if (_resolution != value)
                {
                    Vector2 oldResolution = _resolution;
                    _resolution = value;

                    SetResolution(_resolution);
                    OnResolutionChanged(oldResolution, _resolution);
                }
            }
        }

        /// <summary>
        /// Gets the window in which graphical contents are output (DEBUG). 
        /// </summary>
        public static GameWindow Window
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Gets the <see cref="Resources"/> which loads engine-specific resources.
        /// </summary>
        internal static ResourceLoader Resources
        {
            get;
            private set;
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Runs the game which at first raises the <see cref="Start"/> event and then starts the main loop which raises
        /// the <see cref="Update"/> and <see cref="Render"/> events.
        /// </summary>
        public static void Run()
        {
            // Initialize the game window.
            Window = new GameWindow(Resolution.X, Resolution.Y, GraphicsMode.Default, Title, GameWindowFlags.Default,
                DisplayDevice.Default)
            {
                Icon = _icon,
                Title = _title
            };
            SetResizable(_resizable);
            SetFullscreen(_fullscreen);

            Window.Load += Window_Load;
            Window.UpdateFrame += Window_UpdateFrame;
            Window.RenderFrame += Window_RenderFrame;
            Window.FocusedChanged += Window_FocusedChanged;
            Window.Resize += Window_Resize;
            Window.Closed += Window_Closed;

            // Raise the engine-internal initializing event to set up game-dependent components and managers.
            Log.Initialize();
            Time.Initialize();
            VideoManager.Initialize();
            _audioSystem = new AudioSystem();
            Keyboard.Initialize();
            Mouse.Initialize();

            // Run the game loop at 60 updates per second.
            Window.Run(Time.OptimalFramesPerSecond, Time.OptimalFramesPerSecond);
        }

        /// <summary>
        /// Stops the execution of the game render loop.
        /// </summary>
        public static void Exit()
        {
            Window.Close();
        }
        
        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void SetResizable(bool resizable)
        {
        }

        private static void SetFullscreen(bool fullscreen)
        {
            if (Window != null)
            {
                // Switch the window state.
                if (fullscreen)
                {
                    // Switching to fullscreen. Try to get a matching resolution and only change when one is available.
                    DisplayResolution matchingResolution = DisplayDevice.Default.SelectResolution(
                        _resolution.X, _resolution.Y, 32, 0/*any*/);
                    if (matchingResolution != null)
                    {
                        _isInFullscreenResolution = true;
                        DisplayDevice.Default.ChangeResolution(matchingResolution);
                    }

                    // Remember the current window state to restore it later on.
                    _windowedState = Window.WindowState;
                    Window.WindowState = WindowState.Fullscreen;
                }
                else
                {
                    // Switching back to windowed mode. Restore the previous desktop resolution if it was changed.
                    if (_isInFullscreenResolution)
                    {
                        _isInFullscreenResolution = false;
                        DisplayDevice.Default.RestoreResolution();
                    }

                    Window.WindowState = _windowedState;
                }
            }
        }

        private static void SetResolution(Vector2 resolution)
        {
            if (Window != null)
            {
                // If in fullscreen, try to find a new matching resolution, otherwise, desktop resolution is applied.
                if (_isInFullscreenResolution)
                {
                    DisplayResolution matchingResolution = DisplayDevice.Default.SelectResolution(
                        _resolution.X, _resolution.Y, 32, 0/*any*/);
                    if (matchingResolution == null)
                    {
                        _isInFullscreenResolution = false;
                        DisplayDevice.Default.RestoreResolution();
                    }
                    else
                    {
                        _isInFullscreenResolution = true;
                        DisplayDevice.Default.ChangeResolution(matchingResolution);
                    }
                }

                // Resize the window.
                Window.ClientSize = resolution.ToDrawingSize();
                // Set the OpenGL viewport output to the inner window size.
                Log.WriteInfo(LogCategory.OpenGL, "Viewport({0})", Window.ClientRectangle);
                GL.Viewport(Window.ClientRectangle);
            }
        }
        
        private static void OnStart()
        {
            Start?.Invoke(null, EventArgs.Empty);
        }

        private static void OnUpdate()
        {
            // No check for registered handlers due to performance.
            Update(null, EventArgs.Empty);
        }

        private static void OnRender()
        {
            // No check for registered handlers due to performance.
            Render(null, EventArgs.Empty);
        }

        private static void OnFullscreenChanged(bool isFullscreen)
        {
            FullscreenChanged?.Invoke(null, new FullscreenChangedEventArgs(isFullscreen));
        }

        private static void OnResolutionChanged(Vector2 oldResolution, Vector2 newResolution)
        {
            ResolutionChanged?.Invoke(null, new ResolutionChangedEventArgs(oldResolution, newResolution));
        }

        private static void OnDisposing()
        {
            // No check for registered handlers, since managers must have been created in the constructor.
            Disposing(null, EventArgs.Empty);
        }

        private static void Dispose()
        {
            // Raise the event to give manager classes a chance to shut down.
            OnDisposing();

            // Dispose new non-static systems.
            _audioSystem.Dispose();

            // Destroy the game window.
            Window.Exit();
            Window.Dispose();
        }

        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

        private static void Window_Load(object sender, EventArgs e)
        {
            OnStart();
        }

        private static void Window_UpdateFrame(object sender, FrameEventArgs e)
        {
            OnUpdate();
        }

        private static void Window_RenderFrame(object sender, FrameEventArgs e)
        {
            OnRender();
        }

        private static void Window_FocusedChanged(object sender, EventArgs e)   
        {
            // TODO: Rewamp fullscreen switching...
            if (Fullscreen)
            {
                if (Window.Focused)
                {
                    DisplayResolution matchingResolution = DisplayDevice.Default.SelectResolution(
                        _resolution.X, _resolution.Y, 32, 0/*any*/);
                    //// Got focus for fullscreen, get the fullscreen resolution.
                    DisplayDevice.Default.ChangeResolution(matchingResolution);
                }
                else
                {
                    // Lost focus on fullscreen, restore any desktop resolution.
                    Window.WindowState = WindowState.Minimized;
                    DisplayDevice.Default.RestoreResolution();
                }
            }
        }

        private static void Window_Resize(object sender, EventArgs e)
        {
            Resolution = Window.ClientSize.ToVector2();
        }

        private static void Window_Closed(object sender, EventArgs e)
        {
            // Restore the desktop resolution, if another one was set.
            if (_isInFullscreenResolution)
            {
                _isInFullscreenResolution = false;
                DisplayDevice.Default.RestoreResolution();
            }

            Dispose();
        }
    }
}