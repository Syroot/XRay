﻿namespace Syroot.XRay.Audio
{
    /// <summary>
    /// Represents an event the update thread of the <see cref="AudioSystem"/> has to handle.
    /// </summary>
    internal class AudioThreadEvent
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="AudioThreadEvent"/> class with the given data.
        /// </summary>
        /// <param name="type">The action the update thread should take.</param>
        /// <param name="music">A possible affected <see cref="Music"/> instance or <c>null</c>.</param>
        /// <param name="data">Optional event data required for specific actions.</param>
        internal AudioThreadEvent(AudioThreadEventType type, Music music, object data)
        {
            Type = type;
            Music = music;
            Data = data;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the action the update thread should take.
        /// </summary>
        internal AudioThreadEventType Type
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the possibly affected <see cref="Music"/> instance or <c>null</c>.
        /// </summary>
        internal Music Music
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets optional event data required for specific actions.
        /// </summary>
        internal object Data
        {
            get;
            private set;
        }
    }

    /// <summary>
    /// Represents the possible actions the update thread can handle.
    /// </summary>
    internal enum AudioThreadEventType
    {
        /// <summary>
        /// Occurs when a <see cref="Music"/> instance is created and has to be memorized in the instance list.
        /// </summary>
        SystemRegisterMusic,

        /// <summary>
        /// Occurs when a <see cref="Music"/> instance is disposed and has to removed from the instance list.
        /// </summary>
        SystemUnregisterMusic,

        /// <summary>
        /// Occurs when the <see cref="AudioSystem"/> shuts down and tells the update thread to stop all sources.
        /// </summary>
        SystemExit,

        /// <summary>
        /// Occurs when a <see cref="Music"/> instance should start playing.
        /// </summary>
        Play,

        /// <summary>
        /// Occurs when a <see cref="Music"/> instance should pause its playback.
        /// </summary>
        Pause,

        /// <summary>
        /// Occurs when a <see cref="Music"/> instance should stop its playback.
        /// </summary>
        Stop,

        /// <summary>
        /// Occurs when a <see cref="Music"/> instance sets if it loops its playback or not.
        /// </summary>
        Loop,

        /// <summary>
        /// Occurs when a <see cref="Music"/> instance sets the loop sample to which playback is rewound.
        /// </summary>
        LoopSample,

        /// <summary>
        /// Occurs when a <see cref="Music"/> instance sets the volume of the playback.
        /// </summary>
        Volume,

        /// <summary>
        /// Occurs when a <see cref="Music"/> instance sets the speed of the playback.
        /// </summary>
        Pitch,

        /// <summary>
        /// Occurs when a <see cref="Music"/> instance is about to get disposed by the update thread.
        /// </summary>
        Dispose
    }
}
