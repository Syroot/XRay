﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading;
using OpenTK.Audio;
using OpenTK.Audio.OpenAL;

namespace Syroot.XRay.Audio
{
    /// <summary>
    /// Represents an audio source manager allowing the creation of streamed audio sources which are updated in a
    /// dedicated thread. The communication with the thread is handled through an event queue.
    /// </summary>
    public class AudioSystem : IDisposable
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _updateThreadDelay = 50;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private AudioContext _audioContext;

        private List<Music> _musicInstances;
        private List<Sound> _soundInstances;

        private Thread _updateThread;
        private Queue<AudioThreadEvent> _queue;
        private readonly object _queueMutex;

        private bool _isDisposed;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="AudioSystem"/> class with the <see cref="DefaultDevice"/> as
        /// the audio output device.
        /// </summary>
        public AudioSystem() : this(DefaultDevice, 0) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="AudioSystem"/> class with the specified
        /// <see cref="AudioDevice"/> as the audio output device.
        /// </summary>
        /// <param name="audioDevice">The <see cref="AudioDevice"/> to use for playback.</param>
        public AudioSystem(AudioDevice audioDevice) : this(audioDevice, 0) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="AudioSystem"/> class with the specified
        /// <see cref="AudioDevice"/> as the audio output device playing in the given sample rate.
        /// </summary>
        /// <param name="audioDevice">The <see cref="AudioDevice"/> to use for playback.</param>
        /// <param name="frequency">The sample rate at which playback will happen.</param>
        public AudioSystem(AudioDevice audioDevice, int frequency)
        {
            // Only one instance of this class is allowed.
            if (Instance != null)
                throw new InvalidOperationException("Only one instance of the AudioSystem class can be instantiated.");
            Instance = this;

            // Set the initial buffer length to twice the update time of the thread, should be enough with 3 buffers.
            StreamBufferLengthMs = _updateThreadDelay * 2;

            // Initialize an event queue to do synchronized thread communication with.
            _queue = new Queue<AudioThreadEvent>();
            _queueMutex = new object();

            // Create a new audio context and raise an exception if initialization failed.
            _audioContext = new AudioContext(audioDevice.Identifier, frequency);
            _audioContext.CheckErrors();

            // Create the book-keeping lists.
            _musicInstances = new List<Music>(); // After this, the list is managed by the update thread.
            _soundInstances = new List<Sound>();

            // Start the update thread.
            _updateThread = new Thread(UpdateThread)
            {
                Name = "AudioSystem.UpdateThread",
                Priority = ThreadPriority.AboveNormal
            };
            _updateThread.Start();
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the default audio output device on this system.
        /// </summary>
        public static AudioDevice DefaultDevice
        {
            get => new AudioDevice(AudioContext.DefaultDevice, true);
        }

        /// <summary>
        /// Gets a list of installed audio devices on this system.
        /// </summary>
        public static ReadOnlyCollection<AudioDevice> InstalledDevices
        {
            get
            {
                List<AudioDevice> installedDevices = new List<AudioDevice>();
                foreach (string identifier in AudioContext.AvailableDevices)
                    installedDevices.Add(new AudioDevice(identifier, AudioContext.DefaultDevice == identifier));
                return installedDevices.AsReadOnly();
            }
        }

        /// <summary>
        /// Gets the singleton instance of this class. Multiple instances are not supported due to the overhead required
        /// to manage multiple <see cref="AudioContext"/> instances, which is rarely required anyway.
        /// </summary>
        internal static AudioSystem Instance { get; private set; }

        /// <summary>
        /// Gets or sets the buffer length for a streamed music source in milliseconds. If music playback detects
        /// prematurely stopped sources, it increases this buffer length to prevent this from happening again.
        /// </summary>
        internal int StreamBufferLengthMs { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Registers the given <see cref="Music"/> and adds it to the list of tracked instances.
        /// </summary>
        /// <param name="music">The <see cref="Music"/> to add.</param>
        internal void Register(Music music)
        {
            // The thread has to update the list.
            if (_musicInstances.Contains(music))
                throw new InvalidOperationException("Music already registered.");
            else
                EnqueueEvent(AudioThreadEventType.SystemRegisterMusic, music, null);
        }

        /// <summary>
        /// Registers the given <see cref="Sound"/> and adds it to the list of tracked instances.
        /// </summary>
        /// <param name="sound">The <see cref="Sound"/> to add.</param>
        internal void Register(Sound sound)
        {
            if (_soundInstances.Contains(sound))
                throw new InvalidOperationException("Sound already registered.");
            else
                _soundInstances.Add(sound);
        }

        /// <summary>
        /// Deregisters the given <see cref="Music"/> and removes it from the list of tracked instances.
        /// </summary>
        /// <param name="music">The <see cref="Music"/> to add.</param>
        internal void Unregister(Music music)
        {
            // The thread has to update the list.
            EnqueueEvent(AudioThreadEventType.SystemUnregisterMusic, music, null);
        }

        /// <summary>
        /// Deregisters the given <see cref="Sound"/> and removes it from the list of tracked instances.
        /// </summary>
        /// <param name="sound">The <see cref="Sound"/> to add.</param>
        internal void Unregister(Sound sound)
        {
            Debug.Assert(_soundInstances.Contains(sound), "Sound to remove was not registered.");
            _soundInstances.Remove(sound);
        }

        /// <summary>
        /// Queues a new event created with the specified properties which the updating thread handles as soon as
        /// possible.
        /// </summary>
        /// <param name="type">The action the update thread should take.</param>
        /// <param name="music">A possible affected <see cref="Music"/> instance or <c>null</c>.</param>
        /// <param name="data">Optional event data required for specific actions.</param>
        internal void EnqueueEvent(AudioThreadEventType type, Music music, object data)
        {
            lock (_queueMutex)
                _queue.Enqueue(new AudioThreadEvent(type, music, data));
        }

        /// <summary>
        /// Checks for any OpenAL errors and raises an <see cref="InvalidOperationException"/> if one occured.
        /// </summary>
        internal void CheckErrors()
        {
            ALError error = AL.GetError();
            if (error != ALError.NoError)
                throw new InvalidOperationException(AL.GetErrorString(error));
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release
        /// only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    // Post an exit event to let the audio system shut down and dispose music instances.
                    EnqueueEvent(AudioThreadEventType.SystemExit, null, null);
                    // Wait for the update thread to shut down before continuing.
                    _updateThread.Join();

                    _audioContext.Dispose();
                }

                _audioContext = null;

                _isDisposed = true;
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private AudioThreadEvent DequeueEvent()
        {
            lock (_queueMutex)
            {
                if (_queue.Count > 0)
                    return _queue.Dequeue();
            }
            return null;
        }

        private void UpdateThread(object obj)
        {
            while (true)
            {
                // Get jobs from the event queue to update the music before updating its buffers.
                AudioThreadEvent audioEvent;
                while ((audioEvent = DequeueEvent()) != null)
                {
                    switch (audioEvent.Type)
                    {
                        case AudioThreadEventType.SystemRegisterMusic:
                            HandleEventSystemRegister(audioEvent.Music);
                            break;
                        case AudioThreadEventType.SystemUnregisterMusic:
                            HandleEventSystemUnregister(audioEvent.Music);
                            break;
                        case AudioThreadEventType.SystemExit:
                            HandleEventSystemExit();
                            return;
                        default:
                            audioEvent.Music.HandleEvent(audioEvent);
                            break;
                    }
                }

                // Update the buffers of the music instances.
                foreach (Music music in _musicInstances)
                    music.UpdateBuffers();

                Thread.Sleep(_updateThreadDelay);
            }
        }

        private void HandleEventSystemRegister(Music music)
        {
            _musicInstances.Add(music);
        }

        private void HandleEventSystemUnregister(Music music)
        {
            Debug.Assert(_musicInstances.Contains(music), "Music to remove was not registered.");
            music.DisposeInternal();
            _musicInstances.Remove(music);
        }

        private void HandleEventSystemExit()
        {
            foreach (Music music in _musicInstances)
                music.DisposeInternal();
        }
    }
}
