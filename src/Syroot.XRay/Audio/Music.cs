﻿using System;
using System.IO;
using OpenTK.Audio.OpenAL;
using Syroot.XRay.Audio.Decoders;
using Syroot.XRay.Audio.OpenAL;
using Buffer = Syroot.XRay.Audio.OpenAL.Buffer;

namespace Syroot.XRay.Audio
{
    /// <summary>
    /// Represents a sound source streamed at playback time.
    /// </summary>
    public class Music : IDisposable
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _bufferCount = 3;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Stream _stream;
        private AudioDecoderBase _audioDecoder;

        private Source _source;
        private readonly Buffer[] _buffers;
        private int _bufferSamples;
        private float _pitch;
        private bool _loop;
        private int _loopSample;
        private bool _stoppedOnPurpose;

        // "Shadow" states to immediately report back user-set properties before the update thread really sets them.
        private AudioState _userState;
        private readonly object _userStateMutex;
        private float _userVolume;
        private float _userPitch;
        private bool _userLoop;
        private int _userLoopSample;

        // Buffer variables for read samples as members to keep GC impact low.
        private float[] _oggSamples;
        private short[] _pcmSamples;

        private bool _isDisposed;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="Music"/> class reading the samples from the given stream.
        /// </summary>
        /// <param name="stream">The stream to load the audio data from.</param>
        public Music(Stream stream)
        {
            // Set the initial state variable values.
            _pitch = 1f;
            _loop = true;
            _userState = AudioState.Stopped;
            _userStateMutex = new object(); // Might be set from the update thread if the source state changes.
            _userVolume = 1f;
            _userPitch = 1f;
            _userLoop = true;

            // Initialize the OGG Vorbis reader with which samples are decoded.
            _stream = stream;
            _audioDecoder = AudioDecoders.GetDecoder(_stream);
            InitializeSampleBuffers();

            // Create 3 buffers to queue and update for the streaming and one source to play them.
            _buffers = Buffer.GenerateBuffers(_bufferCount);
            _source = new Source();

            AudioSystem.Instance.Register(this);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the current playback state of this music.
        /// </summary>
        public AudioState State
        {
            get
            {
                // Needs lock since it is the only state which might be set from the update thread.
                lock (_userStateMutex)
                {
                    return _userState;
                }
            }
            set
            {
                switch (_userState)
                {
                    case AudioState.Playing:
                        Play();
                        break;
                    case AudioState.Paused:
                        Pause();
                        break;
                    case AudioState.Stopped:
                        Stop();
                        break;
                    default:
                        throw new InvalidOperationException("Cannot set invalid audio state.");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the playback is rewound to the set <see cref="LoopSample"/> after it
        /// reached the end of the file.
        /// </summary>
        public bool IsLooping
        {
            get
            {
                return _userLoop;
            }
            set
            {
                _userLoop = value;
                AudioSystem.Instance.EnqueueEvent(AudioThreadEventType.Loop, this, _userLoop);
            }
        }

        /// <summary>
        /// Gets or sets the sample at which playback is rewound to if <see cref="IsLooping"/> is set to true.
        /// </summary>
        public int LoopSample
        {
            get
            {
                return _userLoopSample;
            }
            set
            {
                _userLoopSample = value;
                AudioSystem.Instance.EnqueueEvent(AudioThreadEventType.LoopSample, this, _userLoopSample);
            }
        }

        /// <summary>
        /// Gets or sets the volume at which the music plays, where 1f is default volume and 0.5 half volume.
        /// </summary>
        public float Volume
        {
            get
            {
                return _userVolume;
            }
            set
            {
                _userVolume = value;
                AudioSystem.Instance.EnqueueEvent(AudioThreadEventType.Volume, this, _userVolume);
            }
        }

        /// <summary>
        /// Gets or sets the playback speed, where 1f is default playback speed and 0.5f is half speed.
        /// </summary>
        public float Pitch
        {
            get
            {
                return _userPitch;
            }
            set
            {
                _userPitch = value;
                AudioSystem.Instance.EnqueueEvent(AudioThreadEventType.Pitch, this, _userPitch);
            }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Starts or resumes playback of this instance.
        /// </summary>
        public void Play()
        {
            _userState = AudioState.Playing;
            AudioSystem.Instance.EnqueueEvent(AudioThreadEventType.Play, this, null);
        }

        /// <summary>
        /// Pauses the playback of this instance at the current playback position.
        /// </summary>
        public void Pause()
        {
            _userState = AudioState.Paused;
            AudioSystem.Instance.EnqueueEvent(AudioThreadEventType.Pause, this, null);
        }

        /// <summary>
        /// Stops the playback of this instance at the current playback position.
        /// </summary>
        public void Stop()
        {
            _userState = AudioState.Stopped;
            AudioSystem.Instance.EnqueueEvent(AudioThreadEventType.Stop, this, null);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Handles the given <see cref="AudioThreadEvent"/> which has been dequeued from the audio system for this music
        /// instance.
        /// </summary>
        /// <param name="audioEvent">The <see cref="AudioThreadEvent"/> to handle.</param>
        internal void HandleEvent(AudioThreadEvent audioEvent)
        {
            switch (audioEvent.Type)
            {
                case AudioThreadEventType.Play:
                    HandleEventStart();
                    break;
                case AudioThreadEventType.Pause:
                    HandleEventPause();
                    break;
                case AudioThreadEventType.Stop:
                    HandleEventStop();
                    break;
                case AudioThreadEventType.Loop:
                    HandleEventLoop((bool)audioEvent.Data);
                    break;
                case AudioThreadEventType.LoopSample:
                    HandleEventLoopSample((int)audioEvent.Data);
                    break;
                case AudioThreadEventType.Volume:
                    HandleEventVolume((float)audioEvent.Data);
                    break;
                case AudioThreadEventType.Pitch:
                    HandleEventPitch((float)audioEvent.Data);
                    break;
                case AudioThreadEventType.Dispose:
                    HandleEventDispose();
                    break;
            }
        }

        /// <summary>
        /// Updates the music buffers. Runs in the audio system update thread.
        /// </summary>
        internal void UpdateBuffers()
        {
            // No updates are needed if the source is not playing.
            ALSourceState sourceState = _source.State;

            // Check if the source has been accidentally been stopped when the thread did not update fast enough.
            bool prematurelyStopped = sourceState == ALSourceState.Stopped && !_stoppedOnPurpose;

            if (sourceState == ALSourceState.Playing || prematurelyStopped)
            {
                // If source stopped erroneously, increase base buffer size by 50ms to prevent this problem.
                if (prematurelyStopped)
                {
                    AudioSystem.Instance.StreamBufferLengthMs += 50;
                    InitializeSampleBuffers();
                }

                // Unqueue the number of processed buffers.
                int buffersProcessed = _source.BuffersProcessed;
                // Write new data in the outdated buffers and queue them back.
                if (buffersProcessed > 0)
                {
                    Buffer[] unqueuedBuffers = _source.UnqueueBuffers(buffersProcessed);
                    foreach (Buffer unqueuedBuffer in unqueuedBuffers)
                    {
                        if (FillAndQueueBuffer(unqueuedBuffer))
                        {
                            break;
                        }
                    }
                }

                // Resume the source if it was prematurely stopped.
                if (prematurelyStopped)
                {
                    _source.Play();
                }
            }
        }

        /// <summary>
        /// Disposes the music ressources on request from the update thread. Do not call this method on any other
        /// thread.
        /// </summary>
        internal void DisposeInternal()
        {
            StopAndUnqueueAllBuffers();
            Buffer.DisposeBuffers(_buffers);
            _source.Dispose();
            _audioDecoder.Dispose();
            _stream.Dispose();

            _audioDecoder = null;
            _stream = null;
            _source = null;
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release
        /// only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    // Do not really dispose here, as the update thread has to handle this.
                    AudioSystem.Instance.Unregister(this);
                }

                _isDisposed = true;
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void InitializeSampleBuffers()
        {
            // Get a buffer length making sense for the current playback rate (longer if faster, shorter if slower).
            float bufferLengthInSeconds = AudioSystem.Instance.StreamBufferLengthMs * _pitch / 1000;
            _bufferSamples = (int)(_audioDecoder.SampleRate * bufferLengthInSeconds) * _audioDecoder.Channels;

            _oggSamples = new float[_bufferSamples];
            _pcmSamples = new short[_bufferSamples];
        }

        private void HandleEventStart()
        {
            // Reset the flag to detect prematurely stopped sources if updating is too slow.
            _stoppedOnPurpose = false;

            ALSourceState sourceState = _source.State;
            // If the source was previously played, rewind the decoder and unqueue old buffers.
            if (sourceState == ALSourceState.Stopped)
            {
                _audioDecoder.Position = 0;
                if (_source.BuffersProcessed > 0)
                {
                    _source.UnqueueBuffers(_source.BuffersProcessed);
                }
            }
            // If the source was stopped or not even played yet, create new buffers and queue them before starting.
            if (sourceState == ALSourceState.Initial || sourceState == ALSourceState.Stopped)
            {
                foreach (Buffer buffer in _buffers)
                {
                    if (FillAndQueueBuffer(buffer))
                    {
                        break;
                    }
                }
            }

            // Start playing the source and thus consuming the queued buffers.
            _source.Play();
        }

        private void HandleEventPause()
        {
            _source.Pause();
        }

        private void HandleEventStop()
        {
            StopAndUnqueueAllBuffers();
            _audioDecoder.Position = 0;
        }

        private void HandleEventLoop(bool loop)
        {
            _loop = loop;
        }

        private void HandleEventLoopSample(int loopSample)
        {
            _loopSample = loopSample;
        }

        private void HandleEventVolume(float volume)
        {
            _source.Volume = volume;
        }

        private void HandleEventPitch(float pitch)
        {
            _pitch = pitch;
            _source.Pitch = _pitch;

            // Resize the buffers since they are consumed at a different speed now.
            InitializeSampleBuffers();
        }

        private void HandleEventDispose()
        {
            StopAndUnqueueAllBuffers();

            // Delete the source and the buffers.
            _source.Dispose();
            Buffer.DisposeBuffers(_buffers);
        }

        private void StopAndUnqueueAllBuffers()
        {
            // Set the flag to detect prematurely stopped sources if updating is too slow.
            _stoppedOnPurpose = true;

            // When a source gets stopped, all buffers are considered processed according to OpenAL documentation.
            // However, if this method gets called multipled times, the buffers might have been unqueued already.
            _source.Stop();
            int processedBuffers = _source.BuffersProcessed;
            if (processedBuffers > 0)
            {
                _source.UnqueueBuffers(processedBuffers);
            }
        }

        private bool FillAndQueueBuffer(Buffer buffer)
        {
            // Read in the buffer float sample data.
            int samplesRead = _audioDecoder.ReadSamples(_pcmSamples, 0, _bufferSamples);

            // If read sample count is less than requested one, and music loops, merge start data behind the remaining
            // end data (if the music is very short for whatever reasons, this needs to be done multiple times).
            bool endOfData = false;
            while (samplesRead < _bufferSamples)
            {
                if (_loop)
                {
                    _audioDecoder.Position = _loopSample;
                    samplesRead += _audioDecoder.ReadSamples(_pcmSamples, samplesRead, _bufferSamples - samplesRead);
                }
                else
                {
                    // No more data (music does not loop and does not restart), end updating the buffers.
                    endOfData = true;
                    _stoppedOnPurpose = true;
                    lock (_userStateMutex)
                    {
                        _userState = AudioState.Stopped;
                    }
                    break;
                }
            }

            // Only update and queue buffers if there was at least one sample read.
            if (samplesRead > 0)
            {
                // Set the samples as the buffer data.
                buffer.SetData(_audioDecoder.Channels == 1 ? ALFormat.Mono16 : ALFormat.Stereo16, _pcmSamples,
                    samplesRead * sizeof(short), _audioDecoder.SampleRate);
                // Queue the buffer to be played.
                _source.QueueBuffer(buffer);
            }

            return endOfData;
        }
    }
}
