﻿using System;
using System.IO;

namespace Syroot.XRay.Audio.Decoders
{
    /// <summary>
    /// Represents the base class for an audio decoder which implementation can handle a specific audio format.
    /// </summary>
    /// <remarks>
    /// A default constructor with no parameters has to be included to instantiate the audio decoder class for
    /// probing if it supports decoding specific audio data.
    /// </remarks>
    internal abstract class AudioDecoderBase : IDisposable
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the number of channels which make up a full sample.
        /// </summary>
        internal abstract int Channels { get; }

        /// <summary>
        /// Gets or sets the current sample from which samples will be read.
        /// </summary>
        internal abstract long Position { get; set; }

        /// <summary>
        /// Gets or sets the number of samples per second.
        /// </summary>
        internal abstract int SampleRate { get; }

        /// <summary>
        /// Gets the total amount of samples in the audio data for each channel. That means if you want to read 100
        /// samples of two channels, you have to read 200 samples.
        /// </summary>
        internal abstract long ChannelSamples { get; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Gets a new instance of this instances class if it supports the audio data in the given <see cref="Stream"/>
        /// or returns <c>null</c> if the data in the stream cannot be decoded by instances of this class.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to read the data from.</param>
        /// <returns>A new instance of this class if the data can be decoded, or <c>null</c>.</returns>
        internal abstract AudioDecoderBase GetInstance(Stream stream);
        
        /// <summary>
        /// Reads the given amount of samples relative to the current sample position into the given buffer, starting at
        /// the specified offset, and returns the number of samples read.
        /// </summary>
        /// <param name="buffer">The data buffer to write the data in.</param>
        /// <param name="offset">The index offset in the buffer at which to start writing the data.</param>
        /// <param name="count">The number of samples to read.</param>
        /// <returns>The number of samples read.</returns>
        internal abstract int ReadSamples(short[] buffer, int offset, int count);

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release
        /// only unmanaged resources.</param>
        protected abstract void Dispose(bool disposing);
    }
}
