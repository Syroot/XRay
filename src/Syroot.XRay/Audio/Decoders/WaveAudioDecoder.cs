﻿using System;
using System.IO;
using Syroot.BinaryData;

namespace Syroot.XRay.Audio.Decoders
{
    /// <summary>
    /// Represents a decoder which handles the RIFF wave format of sound data.
    /// </summary>
    internal class WaveAudioDecoder : AudioDecoderBase, IDisposable
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Stream _stream;

        private int _channels;
        private int _sampleRate;
        private int _bitsPerSample;
        private int _dataChunkStart;
        private int _dataChunkEnd;
        private int _totalSamples;

        // Members to try to keep GC work low if the number of samples doesn't differ.
        private byte[] _byteSamples;
        private short[] _shortSamples;

        private bool _isDisposed;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        private WaveAudioDecoder() { }

        private WaveAudioDecoder(Stream stream)
        {
            _stream = stream;

            _byteSamples = new byte[0];
            _shortSamples = new short[0];

            // Check the RIFF WAVE header.
            if (_stream.ReadString(4) != "RIFF")
            {
                throw new AudioDecoderException("Wave audio file has an invalid RIFF header.");
            }
            _stream.Seek(sizeof(int)); // Skip the file length - 4 bytes.
            if (_stream.ReadString(4) != "WAVE")
            {
                throw new AudioDecoderException("Wave audio file has an invalid WAVE header.");
            }

            // Continue depending on the chunk.
            while (!_stream.IsEndOfStream())
            {
                string chunkName = _stream.ReadString(4);
                int chunkLength = _stream.ReadInt32();
                switch (chunkName.ToUpperInvariant())
                {
                    case "FMT ":
                        LoadFormatChunk(_stream, chunkLength);
                        break;

                    case "DATA":
                        LoadDataChunk(_stream, chunkLength);
                        break;

                    default:
                        // Skip unknown chunks.
                        _stream.Seek(chunkLength);
                        break;
                }
            }

            // Skip back to sample 0.
            Position = 0;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the number of channels which make up a full sample.
        /// </summary>
        internal override int Channels => _channels;

        /// <summary>
        /// Gets or sets the current sample from which samples will be read.
        /// </summary>
        internal override long Position
        {
            get
            {
                return (_stream.Position - _dataChunkStart) / (_bitsPerSample / 8 * Channels);
            }
            set
            {
                _stream.Seek(_dataChunkStart + value * (_bitsPerSample / 8 * Channels), SeekOrigin.Begin);
            }
        }

        /// <summary>
        /// Gets or sets the number of samples per second.
        /// </summary>
        internal override int SampleRate => _sampleRate;

        /// <summary>
        /// Gets the total amount of samples in the audio data for each channel. That means if you want to read 100
        /// samples of two channels, you have to read 200 samples.
        /// </summary>
        internal override long ChannelSamples => _totalSamples;

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Gets a new instance of this instances class if it supports the audio data in the given <see cref="Stream" />
        /// or returns <c>null</c> if the data in the stream cannot be decoded by instances of this class.
        /// </summary>
        /// <param name="stream">The <see cref="Stream" /> to read the data from.</param>
        /// <returns>A new instance of this class if the data can be decoded, or <c>null</c>.</returns>
        internal override AudioDecoderBase GetInstance(Stream stream)
        {
            // Check for a valid RIFF WAVE header.
            bool canRead = false;
            using (stream.TemporarySeek(0))
            {
                if (stream.ReadString(4) == "RIFF")
                {
                    stream.Seek(sizeof(int)); // Skip the file length (4 bytes).
                    if (stream.ReadString(4) == "WAVE")
                        canRead = true;
                    // There surely should be a fmt and data block in it, but we do not check for it now.
                }
            }

            // Create a new instance if possible.
            return canRead ? new WaveAudioDecoder(stream) : null;
        }

        /// <summary>
        /// Reads the given amount of samples relative to the current sample position into the given buffer, starting at
        /// the specified offset, and returns the number of samples read.
        /// </summary>
        /// <param name="buffer">The data buffer to write the data in.</param>
        /// <param name="offset">The index offset in the buffer at which to start writing the data.</param>
        /// <param name="count">The number of samples to read.</param>
        /// <returns>The number of samples read.</returns>
        internal override int ReadSamples(short[] buffer, int offset, int count)
        {
            // Nothing to read, nothing to do.
            if (count == 0) return 0;

            // Read in the samples.
            int samplesToRead = Math.Min((int)(_totalSamples - Position), count) * Channels;
            if (_bitsPerSample == 8 * sizeof(byte))
            {
                // Convert byte samples to shorts.
                if (_byteSamples.Length < samplesToRead)
                    _byteSamples = new byte[samplesToRead];
                if (_shortSamples.Length < samplesToRead)
                    _shortSamples = new short[samplesToRead];
                _stream.Read(_byteSamples, 0, samplesToRead);
                ConvertByteBuffer(_byteSamples, _shortSamples, samplesToRead);
            }
            else if (_bitsPerSample == 8 * sizeof(short))
            {
                // Read the short samples directly.
                if (_shortSamples.Length < samplesToRead)
                    _shortSamples = new short[samplesToRead];
                for (int i = 0; i < samplesToRead; i++)
                    _shortSamples[i] = _stream.ReadInt16();
            }
            else
            {
                throw new AudioDecoderException("Unsupported sample rate of " + _bitsPerSample);
            }

            // Copy the shorts into the given buffer.
            Buffer.BlockCopy(_shortSamples, 0, buffer, offset * sizeof(short), samplesToRead * sizeof(short));

            return samplesToRead;
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release
        /// only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                    _stream.Dispose();

                _stream = null;
                _byteSamples = null;
                _shortSamples = null;

                _isDisposed = true;
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void LoadFormatChunk(Stream stream, int chunkLength)
        {
            int chunkStart = (int)stream.Position;

            // Wave files must be uncompressed PCM.
            int waveFormatEncoding = stream.ReadInt16();
            if (waveFormatEncoding != 1/*PCM*/)
                throw new AudioDecoderException("Unsupported wave format encoding of \"{0}\".", waveFormatEncoding);

            // Read the audio format details.
            _channels = stream.ReadInt16();
            _sampleRate = stream.ReadInt32();
            int averageBytesPerSecond = stream.ReadInt32(); // PCM = (BitsPerSample / 8) * Channels * SampleRate
            int frameSize = stream.ReadInt16(); // (BitsPerSample / 8) * Channels
            _bitsPerSample = stream.ReadInt16();

            // Skip remaining format data we cannot parse.
            stream.Seek(chunkLength - (stream.Position - chunkStart));
        }

        private void LoadDataChunk(Stream stream, int chunkLength)
        {
            // Remember the start and end of this chunk.
            _dataChunkStart = (int)stream.Position;
            _dataChunkEnd = _dataChunkStart + chunkLength;

            // Calculate the total amount of samples (a sample spans multiple channels).
            _totalSamples = (_dataChunkEnd - _dataChunkStart) / (_bitsPerSample / 8 * Channels);

            // Seek to the end of this chunk for further loading of the file.
            stream.Seek(chunkLength);
        }

        private static void ConvertByteBuffer(byte[] byteBuffer, short[] shortBuffer, int count)
        {
            // Convert each byte sample into a short.
            for (int i = 0; i < count; i++)
                shortBuffer[i] = (short)(byteBuffer[i] << 8);
        }
    }
}
