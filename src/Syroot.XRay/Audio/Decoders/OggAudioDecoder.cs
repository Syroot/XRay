﻿using System;
using System.IO;
using NVorbis;
using Syroot.BinaryData;

namespace Syroot.XRay.Audio.Decoders
{
    /// <summary>
    /// Represents a decoder which handles the OGG Vorbis format of sound data.
    /// </summary>
    internal class OggAudioDecoder : AudioDecoderBase, IDisposable
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Stream _stream;
        private VorbisReader _vorbisReader;

        // Members to try to keep GC work low if the number of samples doesn't differ.
        private float[] _floatSamples;
        private short[] _shortSamples;

        private bool _isDisposed;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        private OggAudioDecoder() { }

        private OggAudioDecoder(Stream stream)
        {
            _stream = stream;
            _vorbisReader = new VorbisReader(_stream, false);

            _floatSamples = new float[0];
            _shortSamples = new short[0];
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the number of channels which make up a full sample.
        /// </summary>
        internal override int Channels => _vorbisReader.Channels;

        /// <summary>
        /// Gets or sets the current sample from which samples will be read.
        /// </summary>
        internal override long Position
        {
            get => _vorbisReader.DecodedPosition;
            set => _vorbisReader.DecodedPosition = value;
        }

        /// <summary>
        /// Gets or sets the number of samples per second.
        /// </summary>
        internal override int SampleRate => _vorbisReader.SampleRate;

        /// <summary>
        /// Gets the total amount of samples in the audio data for each channel. That means if you want to read 100
        /// samples of two channels, you have to read 200 samples.
        /// </summary>
        internal override long ChannelSamples => _vorbisReader.TotalSamples;

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Gets a new instance of this instances class if it supports the audio data in the given <see cref="Stream" />
        /// or returns <c>null</c> if the data in the stream cannot be decoded by instances of this class.
        /// </summary>
        /// <param name="stream">The <see cref="Stream" /> to read the data from.</param>
        /// <returns>A new instance of this class if the data can be decoded, or <c>null</c>.</returns>
        internal override AudioDecoderBase GetInstance(Stream stream)
        {
            // Only check the header for now.
            bool canRead;
            using (stream.TemporarySeek(0))
                canRead = stream.ReadString(4) == "OggS";

            // Create a new instance if possible.
            return canRead ? new OggAudioDecoder(stream) : null;
        }

        /// <summary>
        /// Reads the given amount of samples relative to the current sample position into the given buffer, starting at
        /// the specified offset, and returns the number of samples read.
        /// </summary>
        /// <param name="buffer">The data buffer to write the data in.</param>
        /// <param name="offset">The index offset in the buffer at which to start writing the data.</param>
        /// <param name="count">The number of samples to read.</param>
        /// <returns>The number of samples read.</returns>
        internal override int ReadSamples(short[] buffer, int offset, int count)
        {
            // Nothing to read, nothing to do.
            if (count == 0) return 0;

            // Read in the vorbis float samples.
            if (_floatSamples.Length < count)
            {
                _floatSamples = new float[count];
            }
            int samplesRead = _vorbisReader.ReadSamples(_floatSamples, 0, count);

            // Convert them to a short buffer.
            if (_shortSamples.Length < samplesRead)
            {
                _shortSamples = new short[samplesRead];
            }
            ConvertFloatBuffer(_floatSamples, _shortSamples, samplesRead);

            // Copy the shorts into the given buffer.
            Buffer.BlockCopy(_shortSamples, 0, buffer, offset * sizeof(short), samplesRead * sizeof(short));

            return samplesRead;
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release
        /// only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    _vorbisReader.Dispose();
                    _stream.Dispose();
                }

                _vorbisReader = null;
                _stream = null;
                _floatSamples = null;
                _shortSamples = null;

                _isDisposed = true;
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void ConvertFloatBuffer(float[] floatBuffer, short[] shortBuffer, int count)
        {
            // Convert each float sample into a short.
            for (int i = 0; i < count; i++)
            {
                int sampleShort = (int)(32767f * floatBuffer[i]);

                // Needs clamping since conversion might be out of short bounds.
                if (sampleShort > Int16.MaxValue) sampleShort = Int16.MaxValue;
                else if (sampleShort < Int16.MinValue) sampleShort = Int16.MinValue;

                shortBuffer[i] = (short)sampleShort;
            }
        }
    }
}
