﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Syroot.XRay.Audio.Decoders
{
    /// <summary>
    /// Represents a static class containing methods to instantiate <see cref="AudioDecoderBase"/> instances which are
    /// tested for handling different kinds of audio data.
    /// </summary>
    internal static class AudioDecoders
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static List<AudioDecoderBase> _decoders;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        private static List<AudioDecoderBase> Decoders
        {
            get
            {
                if (_decoders == null)
                {
                    // Create instances of audio decoder classes for probing.
                    _decoders = new List<AudioDecoderBase>();
                    Type decoderBaseType = typeof(AudioDecoderBase);
                    foreach (Type type in Assembly.GetAssembly(decoderBaseType).GetTypes())
                    {
                        // Check if the type found in the assembly implements this base and is not the base itself.
                        if (decoderBaseType.IsAssignableFrom(type) && decoderBaseType != type)
                        {
                            // Instantiate it and remember it in the decoder list.
                            _decoders.Add((AudioDecoderBase)Activator.CreateInstance(type, true));
                        }
                    }
                }
                return _decoders;
            }
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Instantiates a correctly typed <see cref="AudioDecoderBase"/> object, which is capable of parsing the data
        /// available from the given <see cref="Stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to read the data from.</param>
        /// <returns>The instantiated <see cref="AudioDecoderBase"/>.</returns>
        internal static AudioDecoderBase GetDecoder(Stream stream)
        {
            long dataStartPosition = stream.Position;
            foreach (AudioDecoderBase decoder in Decoders)
            {
                // Try to get a new instance dedicated to decode the data in this stream.
                AudioDecoderBase dedicatedInstance = decoder.GetInstance(stream);
                if (dedicatedInstance != null)
                {
                    return dedicatedInstance;
                }
                stream.Position = dataStartPosition;
            }

            // No implementation found which supports the audio data format.
            throw new AudioDecoderException("Unsupported audio file format.");
        }
    }
}