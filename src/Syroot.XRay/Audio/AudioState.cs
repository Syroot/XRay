﻿using System;
using OpenTK.Audio.OpenAL;

namespace Syroot.XRay.Audio
{
    /// <summary>
    /// Represents the different states an audio playback can be in.
    /// </summary>
    public enum AudioState
    {
        /// <summary>
        /// The playback is currently running.
        /// </summary>
        Playing,

        /// <summary>
        /// The playback is paused at the most recently played position.
        /// </summary>
        Paused,

        /// <summary>
        /// The playback has been stopped and rewound back to the start position.
        /// </summary>
        Stopped
    }

    /// <summary>
    /// Represents a collection of static extension methods for <see cref="ALSourceState"/> objects.
    /// </summary>
    internal static class ALSourceStateExtensions
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Converts the <see cref="ALSourceState"/> value to a matching one of <see cref="AudioState"/>.
        /// </summary>
        /// <param name="sourceState">The extended <see cref="ALSourceState"/>.</param>
        /// <returns>The converted <see cref="AudioState"/>.</returns>
        internal static AudioState ToAudioState(this ALSourceState sourceState)
        {
            switch (sourceState)
            {
                case ALSourceState.Initial:
                case ALSourceState.Stopped:
                    return AudioState.Stopped;
                case ALSourceState.Paused:
                    return AudioState.Paused;
                case ALSourceState.Playing:
                    return AudioState.Playing;
                default:
                    throw new ArgumentOutOfRangeException("sourceState");
            }
        }
    }
}
