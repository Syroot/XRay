﻿using System;
using OpenTK.Audio.OpenAL;

namespace Syroot.XRay.Audio.OpenAL
{
    /// <summary>
    /// Represents an OpenAL source which can play data from <see cref="Buffer"/> instances.
    /// </summary>
    internal class Source : IDisposable
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------
        
        private bool _isDisposing;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="Source"/> class.
        /// </summary>
        internal Source()
        {
            Handle = AL.GenSource();
            AudioSystem.Instance.CheckErrors();
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the OpenAL handle of this source.
        /// </summary>
        internal int Handle
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the number of fully consumed buffers with previously played data. This is always 0 if
        /// <see cref="IsLooping"/> is <c>true</c>.
        /// </summary>
        internal int BuffersProcessed
        {
            get
            {
                AL.GetSource(Handle, ALGetSourcei.BuffersProcessed, out int buffersProcessed);
                AudioSystem.Instance.CheckErrors();
                return buffersProcessed;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the playback of this source loops. Do not set this for source which
        /// use streamed buffers, as buffers will never get processed then.
        /// </summary>
        internal bool IsLooping
        {
            get
            {
                AL.GetSource(Handle, ALSourceb.Looping, out bool isLooping);
                AudioSystem.Instance.CheckErrors();
                return isLooping;
            }
            set
            {
                AL.Source(Handle, ALSourceb.Looping, value);
                AudioSystem.Instance.CheckErrors();
            }
        }

        /// <summary>
        /// Gets the current <see cref="ALSourceState"/> of this instance.
        /// </summary>
        internal ALSourceState State
        {
            get
            {
                ALSourceState sourceState = AL.GetSourceState(Handle);
                AudioSystem.Instance.CheckErrors();
                return sourceState;
            }
        }

        /// <summary>
        /// Gets or sets the playback rate of the source, where 1f is default speed and 0.5f half speed.
        /// </summary>
        internal float Pitch
        {
            get
            {
                AL.GetSource(Handle, ALSourcef.Pitch, out float pitch);
                AudioSystem.Instance.CheckErrors();
                return pitch;
            }
            set
            {
                AL.Source(Handle, ALSourcef.Pitch, value);
                AudioSystem.Instance.CheckErrors();
            }
        }

        /// <summary>
        /// Gets or sets the volume gain of the source, where 1f is default volume and 0.5f half volume.
        /// </summary>
        internal float Volume
        {
            get
            {
                AL.GetSource(Handle, ALSourcef.Gain, out float volume);
                AudioSystem.Instance.CheckErrors();
                return volume;
            }
            set
            {
                AL.Source(Handle, ALSourcef.Gain, value);
                AudioSystem.Instance.CheckErrors();
            }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Starts or resumes playback of this source. If the source is already playing, the playback will be restarted
        /// from the first buffer.
        /// </summary>
        internal void Play()
        {
            AL.SourcePlay(Handle);
            AudioSystem.Instance.CheckErrors();
        }

        /// <summary>
        /// Pauses playback at the current position. If the source is already paused, nothing will happen.
        /// </summary>
        internal void Pause()
        {
            AL.SourcePause(Handle);
            AudioSystem.Instance.CheckErrors();
        }

        /// <summary>
        /// Stops playback and rewinds the position to the beginning of the buffers. If the source is already stopped,
        /// nothing will happen.
        /// </summary>
        internal void Stop()
        {
            AL.SourceStop(Handle);
            AudioSystem.Instance.CheckErrors();
        }

        /// <summary>
        /// Queues the given <see cref="Buffer"/> instance for playback.
        /// </summary>
        /// <param name="buffer">The <see cref="Buffer"/> instance to queue for playback.</param>
        internal void QueueBuffer(Buffer buffer)
        {
            AL.SourceQueueBuffer(Handle, buffer.Handle);
            AudioSystem.Instance.CheckErrors();
        }

        /// <summary>
        /// Tries to unqueue the given amount of <see cref="Buffer"/> instances.
        /// </summary>
        /// <remarks>
        /// The <see cref="Buffer"/> instances are not the same as those which might have been created previously. They
        /// are new instances representing the OpenAL buffer with the same handle.
        /// </remarks>
        /// <param name="count">The amount of <see cref="Buffer"/> instances to unqueue.</param>
        /// <returns>An array holding the unqueued <see cref="Buffer"/> instances.</returns>
        internal Buffer[] UnqueueBuffers(int count)
        {
            int[] unqueuedBufferHandles = AL.SourceUnqueueBuffers(Handle, count);
            AudioSystem.Instance.CheckErrors();

            Buffer[] unqueuedBuffers = new Buffer[unqueuedBufferHandles.Length];
            for (int i = 0; i < unqueuedBuffers.Length; i++)
            {
                unqueuedBuffers[i] = new Buffer(unqueuedBufferHandles[i]);
            }

            return unqueuedBuffers;
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release
        /// only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposing)
            {
                if (disposing)
                {
                    AL.DeleteSource(Handle);
                    AudioSystem.Instance.CheckErrors();
                }

                Handle = 0;

                _isDisposing = true;
            }
        }
    }
}
