﻿using System;
using OpenTK.Audio.OpenAL;

namespace Syroot.XRay.Audio.OpenAL
{
    /// <summary>
    /// Represents an OpenAL buffer holding the audio data and can be attached to a <see cref="Source"/>.
    /// </summary>
    internal class Buffer : IDisposable
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------
        
        private bool _isDisposing;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="Buffer"/> class representing a new OpenAL buffer.
        /// </summary>
        internal Buffer()
        {
            Handle = AL.GenBuffer();
            AudioSystem.Instance.CheckErrors();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Buffer"/> class representing the previously created OpenAL
        /// buffer with the given handle.
        /// </summary>
        /// <param name="handle">The handle of the previously created OpenAL buffer to represent.</param>
        internal Buffer(int handle)
        {
            Handle = handle;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the OpenAL handle of this buffer.
        /// </summary>
        internal int Handle
        {
            get;
            private set;
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Sets the data held by this buffer with the given format.
        /// </summary>
        /// <typeparam name="T">The type of the elements in the data array.</typeparam>
        /// <param name="alFormat">The <see cref="ALFormat"/> of the buffer data.</param>
        /// <param name="buffer">The data array.</param>
        /// <param name="sizeInBytes">The size of the data array in bytes.</param>
        /// <param name="frequency">The frequency of the buffer data.</param>
        internal void SetData<T>(ALFormat alFormat, T[] buffer, int sizeInBytes, int frequency)
            where T : struct
        {
            AL.BufferData(Handle, alFormat, buffer, sizeInBytes, frequency);
            AudioSystem.Instance.CheckErrors();
        }

        /// <summary>
        /// Generates and returns the given amount of buffers.
        /// </summary>
        /// <param name="count">The amount of buffers to generate and return.</param>
        /// <returns>The generated and returned buffers.</returns>
        internal static Buffer[] GenerateBuffers(int count)
        {
            int[] bufferHandles = AL.GenBuffers(count); AudioSystem.Instance.CheckErrors();

            Buffer[] buffers = new Buffer[bufferHandles.Length];
            for (int i = 0; i < buffers.Length; i++)
            {
                buffers[i] = new Buffer(bufferHandles[i]);
            }

            return buffers;
        }

        /// <summary>
        /// Disposes the <see cref="Buffer"/> instances in the given array.
        /// </summary>
        /// <param name="buffers">The array holding <see cref="Buffer"/> instances to dispose.</param>
        internal static void DisposeBuffers(Buffer[] buffers)
        {
            foreach (Buffer buffer in buffers)
            {
                buffer.Dispose();
            }
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release
        /// only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposing)
            {
                if (disposing)
                {
                    AL.DeleteBuffer(Handle);
                    AudioSystem.Instance.CheckErrors();
                }

                Handle = 0;

                _isDisposing = true;
            }
        }
    }
}
