﻿using System;
using System.Globalization;
using System.Runtime.Serialization;
using OpenTK.Audio.OpenAL;

namespace Syroot.XRay.Audio.OpenAL
{
    /// <summary>
    /// Represents an exception caused by OpenAL function calls.
    /// </summary>
    [Serializable]
    internal class OpenALException : Exception
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenALException"/> class.
        /// </summary>
        internal OpenALException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenALException"/> class for the given OpenAL error.
        /// </summary>
        /// <param name="alError">The OpenAL error to represent.</param>
        internal OpenALException(ALError alError)
            : base(AL.GetErrorString(alError))
        {
            ALError = alError;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenALException"/> class with the given message.
        /// </summary>
        /// <param name="message">The message provided with the exception.</param>
        internal OpenALException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenALException"/> class with the given message and inner
        /// exception.
        /// </summary>
        /// <param name="message">The message provided with the exception.</param>
        /// <param name="innerException">The inner exception.</param>
        internal OpenALException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenALException"/> class with the given message.
        /// </summary>
        /// <param name="format">A composite format string representing the message.</param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        internal OpenALException(string format, params object[] args)
            : base(string.Format(CultureInfo.CurrentCulture, format, args))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenALException"/> class with serialized data.
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> that holds the serialized object data about the
        /// exception being thrown.</param>
        /// <param name="context">The <see cref="StreamingContext"/> that contains contextual information about the
        /// source or destination.</param>
        /// <exception cref="ArgumentNullException">The info parameter is null.</exception>
        /// <exception cref="SerializationException">The class name is null or <see cref="System.Exception.HResult"/> is
        /// zero (0).</exception>
        protected OpenALException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the internal <see cref="ALError"/> instance representing this exception.
        /// </summary>
        internal ALError ALError
        {
            get;
            private set;
        }
    }
}
