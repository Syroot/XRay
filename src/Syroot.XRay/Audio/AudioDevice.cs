﻿using System;

namespace Syroot.XRay.Audio
{
    /// <summary>
    /// Represents an audio output device installed in the system.
    /// </summary>
    public class AudioDevice
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="AudioDevice"/> class for the device with the given identifier.
        /// </summary>
        /// <param name="identifier">The unique identifier of the device.</param>
        /// <param name="isDefault">If set to <c>true</c>, this is the default device.</param>
        internal AudioDevice(string identifier, bool isDefault)
        {
            Identifier = identifier;
            IsDefault = isDefault;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets a <see cref="String"/> uniquely identifying this device.
        /// </summary>
        public string Identifier
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether this device is the default audio output device.
        /// </summary>
        public bool IsDefault
        {
            get;
            private set;
        }
    }
}
