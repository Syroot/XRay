﻿using System;
using System.IO;
using OpenTK.Audio.OpenAL;
using Syroot.XRay.Audio.Decoders;
using Syroot.XRay.Audio.OpenAL;
using Buffer = Syroot.XRay.Audio.OpenAL.Buffer;

namespace Syroot.XRay.Audio
{
    /// <summary>
    /// Represents a short to medium-long effect stored completely in memory and is not streamed.
    /// </summary>
    public class Sound : IDisposable
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------
        
        private Source _source;
        private Buffer _buffer;

        private bool _isDisposed;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="Sound"/> class reading the samples from the given stream.
        /// </summary>
        /// <param name="stream">The stream to load the audio data from.</param>
        public Sound(Stream stream)
        {
            // Create a new playback source and data buffer.
            _source = new Source();
            _buffer = new Buffer();

            // Instantiate a decoder and load all samples into the buffer.
            using (AudioDecoderBase decoder = AudioDecoders.GetDecoder(stream))
            {
                short[] samples = new short[decoder.ChannelSamples * decoder.Channels];
                decoder.ReadSamples(samples, 0, (int)samples.Length);
                _buffer.SetData(decoder.Channels == 1 ? ALFormat.Mono16 : ALFormat.Stereo16, samples,
                    samples.Length * sizeof(short), decoder.SampleRate);
            }

            // Queue the buffer data in the source.
            _source.QueueBuffer(_buffer);

            AudioSystem.Instance.Register(this);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the current playback state of the audio.
        /// </summary>
        public AudioState State
        {
            get { return _source.State.ToAudioState(); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether audio is rewound to the beginning and continues to play after it
        /// reached the end of the stream.
        /// </summary>
        public bool IsLooping
        {
            get
            {
                return _source.IsLooping;
            }
            set
            {
                _source.IsLooping = value;
            }
        }

        /// <summary>
        /// Gets or sets the pitch which affects the playback rate and speed the audio is played with.
        /// </summary>
        public float Pitch
        {
            get { return _source.Pitch; }
            set { _source.Pitch = value; }
        }

        /// <summary>
        /// Gets or sets the volume which affects the loudness of the audio, which has to be between 0f and 1f.
        /// </summary>
        public float Volume
        {
            get { return _source.Volume; }
            set { _source.Volume = value; }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------
        
        /// <summary>
        /// Starts or resumes playing the audio.
        /// </summary>
        public void Play()
        {
            _source.Play();
        }

        /// <summary>
        /// Pauses the playback of the audio.
        /// </summary>
        public void Pause()
        {
            _source.Pause();
        }

        /// <summary>
        /// Stops the playback of the audio and rewinds to the first sample.
        /// </summary>
        public void Stop()
        {
            _source.Stop();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release
        /// only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    _source.Dispose();
                    _buffer.Dispose();

                    AudioSystem.Instance.Unregister(this);
                }

                _source = null;
                _buffer = null;

                _isDisposed = true;
            }
        }
    }
}
