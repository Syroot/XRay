﻿using System;
using Syroot.Maths;

namespace Syroot.XRay
{
    /// <summary>
    /// Passed with the <see cref="Game.ResolutionChanged"/> event and provides information about the previous and
    /// current resolution.
    /// </summary>
    public class ResolutionChangedEventArgs : EventArgs
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="ResolutionChangedEventArgs"/> class with the information about
        /// the previous and current resolution of the game.
        /// </summary>
        /// <param name="oldResolution">The previous resolution of the game.</param>
        /// <param name="newResolution">The current resolution of the game.</param>
        public ResolutionChangedEventArgs(Vector2 oldResolution, Vector2 newResolution)
        {
            OldResolution = oldResolution;
            NewResolution = newResolution;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the previous resolution of the game.
        /// </summary>
        public Vector2 OldResolution
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the current resolution of the game.
        /// </summary>
        public Vector2 NewResolution
        {
            get;
            private set;
        }
    }
}
