﻿using System;
using System.Drawing;
using System.Xml.Serialization;

namespace Syroot.XRay.Video.BMFont
{
    /// <summary>
    /// Represents information on how the font was generated.
    /// </summary>
    [Serializable]
    public class FontInfo
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Rectangle _padding;
        private Point     _spacing;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the name of the true type font.
        /// </summary>
        [XmlAttribute("face")]
        public string Face
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the size of the true type font.
        /// </summary>
        [XmlAttribute("size")]
        public int Size
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this font is bold.
        /// </summary>
        [XmlAttribute("bold")]
        public bool Bold
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this font is italic.
        /// </summary>
        [XmlAttribute("italic")]
        public bool Italic
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the OEM charset used (when not unicode).
        /// </summary>
        [XmlAttribute("charset")]
        public string Charset
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether it is the unicode charset.
        /// </summary>
        [XmlAttribute("unicode")]
        public bool Unicode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the font height stretch in percentage. 100% means no stretch.
        /// </summary>
        [XmlAttribute("stretchH")]
        public int StretchHeight
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether smoothing was turned on.
        /// </summary>
        [XmlAttribute("smooth")]
        public bool Smooth
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the super-sampling level used. 1 means no super-sampling was used.
        /// </summary>
        [XmlAttribute("aa")]
        public int SuperSampling
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the padding for each character (up, right, down, left).
        /// </summary>
        [XmlAttribute("padding")]
        public string Padding
        {
            get
            {
                return _padding.X + "," + _padding.Y + "," + _padding.Width + "," + _padding.Height;
            }
            set
            {
                string[] padding = value.Split(',');
                _padding = new Rectangle(int.Parse(padding[0]), int.Parse(padding[1]), int.Parse(padding[2]),
                    int.Parse(padding[3]));
            }
        }

        /// <summary>
        /// Gets or sets the spacing for each character (horizontal, vertical).
        /// </summary>
        [XmlAttribute("spacing")]
        public string Spacing
        {
            get
            {
                return _spacing.X + "," + _spacing.Y;
            }
            set
            {
                string[] spacing = value.Split(',');
                _spacing = new Point(int.Parse(spacing[0]), int.Parse(spacing[1]));
            }
        }

        /// <summary>
        /// Gets or sets the outline thickness for the characters.
        /// </summary>
        [XmlAttribute("outline")]
        public int Outline
        {
            get;
            set;
        }
    }
}
