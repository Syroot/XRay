﻿using System;
using System.Xml.Serialization;

namespace Syroot.XRay.Video.BMFont
{
    /// <summary>
    /// Represents kerning information which is used to adjust the distance between certain characters, e.g. some
    /// characters should be placed closer to each other than others.
    /// </summary>
    [Serializable]
    public class FontKerning
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the first character ID.
        /// </summary>
        [XmlAttribute("first")]
        public int First
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the second character ID.
        /// </summary>
        [XmlAttribute("second")]
        public int Second
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets how much the X position should be adjusted when drawing the second character immediately
        /// following the first.
        /// </summary>
        [XmlAttribute("amount")]
        public int Amount
        {
            get;
            set;
        }
    }
}
