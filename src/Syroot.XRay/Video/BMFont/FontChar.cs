﻿using System;
using System.Diagnostics;
using System.Xml.Serialization;

namespace Syroot.XRay.Video.BMFont
{
    /// <summary>
    /// Represents characters in the font. There is one for each included character in the font.
    /// </summary>
    [Serializable]
    [DebuggerDisplay("CharID = {CharID}, X = {X}, Y = {Y}, Width = {Width}, Height = {Height}")]
    public class FontChar
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------
        
        /// <summary>
        /// Gets or sets the character ID, being the ASCII / unicode code for the mapped character.
        /// </summary>
        [XmlAttribute("id")]
        public int ID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the left position of the character image in the texture.
        /// </summary>
        [XmlAttribute("x")]
        public int X
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the top position of the character image in the texture.
        /// </summary>
        [XmlAttribute("y")]
        public int Y
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the width of the character image in the texture.
        /// </summary>
        [XmlAttribute("width")]
        public int Width
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the height of the character image in the texture.
        /// </summary>
        [XmlAttribute("height")]
        public int Height
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets how much the current position should be offset when copying the image from the texture to the
        /// screen.
        /// </summary>
        [XmlAttribute("xoffset")]
        public int XOffset
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets how much the current position should be offset when copying the image from the texture to the
        /// screen.
        /// </summary>
        [XmlAttribute("yoffset")]
        public int YOffset
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets how much the current position should be advanced after drawing the character.
        /// </summary>
        [XmlAttribute("xadvance")]
        public int XAdvance
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the page ID where the character image is found.
        /// </summary>
        [XmlAttribute("page")]
        public int Page
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the texture channel where the character image is found.
        /// </summary>
        [XmlAttribute("chnl")]
        public CharacterChannel Channel
        {
            get;
            set;
        }

        [XmlIgnore]
        private char CharID
        {
            get { return (char)ID; }
        }
    }

    /// <summary>
    /// Represents the list of possible color channels in which character information is stored.
    /// </summary>
    [Flags]
    public enum CharacterChannel
    {
        /// <summary>
        /// The character image is found in the blue channel.
        /// </summary>
        [XmlEnum("1")]
        Blue = 1,

        /// <summary>
        /// The character image is found in the green channel.
        /// </summary>
        [XmlEnum("2")]
        Green = 2,

        /// <summary>
        /// The character image is found in the red channel.
        /// </summary>
        [XmlEnum("4")]
        Red = 4,

        /// <summary>
        /// The character image is found in the alpha channel.
        /// </summary>
        [XmlEnum("8")]
        Alpha = 8,

        /// <summary>
        /// The character image is found in all channels.
        /// </summary>
        [XmlEnum("15")]
        All = Blue | Green | Red | Alpha
    }
}
