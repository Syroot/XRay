﻿using System;
using System.Xml.Serialization;

namespace Syroot.XRay.Video.BMFont
{
    /// <summary>
    /// Gets or sets the name of a texture file. There is one for each page in the font.
    /// </summary>
    [Serializable]
    public class FontPage
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------
        
        /// <summary>
        /// Gets or sets the page ID.
        /// </summary>
        [XmlAttribute("id")]
        public int ID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the texture file name.
        /// </summary>
        [XmlAttribute("file")]
        public string File
        {
            get;
            set;
        }
    }
}
