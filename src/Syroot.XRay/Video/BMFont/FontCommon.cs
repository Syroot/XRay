﻿using System;
using System.Xml.Serialization;

namespace Syroot.XRay.Video.BMFont
{
    /// <summary>
    /// Represents information common to all characters.
    /// </summary>
    [Serializable]
    public class FontCommon
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------
        
        /// <summary>
        /// Gets or sets the distance in pixels between each line of text.
        /// </summary>
        [XmlAttribute("lineHeight")]
        public int LineHeight
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the number of pixels from the absolute top of the line to the base of the characters.
        /// </summary>
        [XmlAttribute("base")]
        public int Base
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the width of the texture, normally used to scale the X position of the character image.
        /// </summary>
        [XmlAttribute("scaleW")]
        public int ScaleW
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the height of the texture, normally used to scale the Y position of the character image.
        /// </summary>
        [XmlAttribute("scaleH")]
        public int ScaleH
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the number of texture pages included in the font.
        /// </summary>
        [XmlAttribute("pages")]
        public int Pages
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the monochrome characters have been packed into each of the texture
        /// channels. In this case <see cref="AlphaChannel"/> describes what is stored in each channel.
        /// </summary>
        [XmlAttribute("packed")]
        public bool Packed
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value set to 0 if the channel holds glyph data, 1 if it holds the outline, 2 if it holds the
        /// glyph and the outline, 3 if it is set to zero, and 4 if it is set to one.
        /// </summary>
        [XmlAttribute("alphaChnl")]
        public ChannelType AlphaChannel
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value set to 0 if the channel holds glyph data, 1 if it holds the outline, 2 if it holds the
        /// glyph and the outline, 3 if it is set to zero, and 4 if it is set to one.
        /// </summary>
        [XmlAttribute("redChnl")]
        public ChannelType RedChannel
        {
            get;
            set;
        }
        
        /// <summary>
        /// Gets or sets a value set to 0 if the channel holds glyph data, 1 if it holds the outline, 2 if it holds the
        /// glyph and the outline, 3 if it is set to zero, and 4 if it is set to one.
        /// </summary>
        [XmlAttribute("greenChnl")]
        public ChannelType GreenChannel
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value set to 0 if the channel holds glyph data, 1 if it holds the outline, 2 if it holds the
        /// glyph and the outline, 3 if it is set to zero, and 4 if it is set to one.
        /// </summary>
        [XmlAttribute("blueChnl")]
        public ChannelType BlueChannel
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Represents the possible data types a channel can contain.
    /// </summary>
    public enum ChannelType
    {
        /// <summary>
        /// The channel contains glyph data.
        /// </summary>
        [XmlEnum("0")]
        Glyph = 0,

        /// <summary>
        /// The channel contains outline data.
        /// </summary>
        [XmlEnum("1")]
        Outline = 1,

        /// <summary>
        /// The channel contains glyph and outline data.
        /// </summary>
        [XmlEnum("2")]
        GlyphAndOutline = 2,

        /// <summary>
        /// The channel is set to 1.
        /// </summary>
        [XmlEnum("3")]
        Zero = 3,

        /// <summary>
        /// The channel is set to 0.
        /// </summary>
        [XmlEnum("4")]
        One = 4
    }
}
