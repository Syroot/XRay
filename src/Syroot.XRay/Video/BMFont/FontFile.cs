﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Syroot.XRay.Video.BMFont
{
    /// <summary>
    /// Represents a BMFont file. S. <see href="http://www.angelcode.com/products/bmfont/doc/file_format.html"/> for
    /// more details.
    /// </summary>
    [Serializable]
    [XmlRoot("font")]
    public class FontFile
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets information on how the font was generated.
        /// </summary>
        [XmlElement("info")]
        public FontInfo Info
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets information common to all characters.
        /// </summary>
        [XmlElement("common")]
        public FontCommon Common
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of a texture file. There is one for each page in the font.
        /// </summary>
        [XmlArray("pages")]
        [XmlArrayItem("page")]
        public List<FontPage> Pages
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets descriptions on characters in the font. There is one for each included character in the font.
        /// </summary>
        [XmlArray("chars")]
        [XmlArrayItem("char")]
        public List<FontChar> Chars
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets kerning information which is used to adjust the distance between certain characters, e.g. some
        /// characters should be placed closer to each other than others.
        /// </summary>
        [XmlArray("kernings")]
        [XmlArrayItem("kerning")]
        public List<FontKerning> Kernings
        {
            get;
            set;
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Creates a new instance of the <see cref="FontFile"/> class, reading the BMFont mapping data from the given
        /// <see cref="Stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to read the BMFont data from.</param>
        /// <returns>A new <see cref="FontFile"/> instance.</returns>
        public static FontFile Instantiate(Stream stream)
        {
            // Deserialize an instance.
            XmlSerializer deserializer = new XmlSerializer(typeof(FontFile));
            using (StreamReader textReader = new StreamReader(stream))
            {
                return (FontFile)deserializer.Deserialize(stream);
            }
        }
    }
}
