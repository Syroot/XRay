﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;
using Syroot.Maths;

namespace Syroot.XRay.Video.Sprite
{
    /// <summary>
    /// Represents an edge point of a <see cref="SpriteInfo"/> net, including the information about this point.
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = SizeInBytes)]
    internal struct SpriteVertex : IEquatable<SpriteVertex>, Syroot.Maths.INearlyEquatable<SpriteVertex>
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the amount of value types required to represent this structure.
        /// </summary>
        internal const int ValueCount = Vector2F.ValueCount + Vector2F.ValueCount + ColorF.ValueCount;

        /// <summary>
        /// Gets the size of this structure.
        /// </summary>
        internal const int SizeInBytes = Vector2F.SizeInBytes + Vector2F.SizeInBytes + ColorF.SizeInBytes;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        /// <summary>
        /// The position at which this <see cref="SpriteVertex"/> is placed.
        /// </summary>
        [FieldOffset(0)]
        public Vector2F Position;

        /// <summary>
        /// The texture position at this point.
        /// </summary>
        [FieldOffset(8)]
        public Vector2F TextureCoordinate;

        /// <summary>
        /// The color at this point.
        /// </summary>
        [FieldOffset(16)]
        public ColorF Color;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="SpriteVertex"/> struct at the given position and texture
        /// coordinate.
        /// </summary>
        /// <param name="position">The position at which the <see cref="SpriteVertex"/> is placed.</param>
        /// <param name="textureCoordinate">The texture position at this point.</param>
        /// <param name="color">The color at this point.</param>
        public SpriteVertex(Vector2F position, Vector2F textureCoordinate, ColorF color)
        {
            Position = position;
            TextureCoordinate = textureCoordinate;
            Color = color;
        }

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets a value indicating whether the components of the first specified <see cref="SpriteVertex"/> are the
        /// same as the components of the second specified <see cref="SpriteVertex"/>.
        /// </summary>
        /// <param name="a">The first <see cref="SpriteVertex"/> to compare.</param>
        /// <param name="b">The second <see cref="SpriteVertex"/> to compare.</param>
        /// <returns>true, if the components of both <see cref="SpriteVertex"/> are the same.</returns>
        public static bool operator ==(SpriteVertex a, SpriteVertex b)
        {
            return a.Position == b.Position && a.TextureCoordinate == b.TextureCoordinate && a.Color == b.Color;
        }

        /// <summary>
        /// Gets a value indicating whether the components of the first specified <see cref="SpriteVertex"/> are not the
        /// same as the components of the second specified <see cref="SpriteVertex"/>.
        /// </summary>
        /// <param name="a">The first <see cref="SpriteVertex"/> to compare.</param>
        /// <param name="b">The second <see cref="SpriteVertex"/> to compare.</param>
        /// <returns>true, if the components of both <see cref="SpriteVertex"/> are not the same.</returns>
        public static bool operator !=(SpriteVertex a, SpriteVertex b)
        {
            return a.Position != b.Position || a.TextureCoordinate != b.TextureCoordinate || a.Color != b.Color;
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Gets a value indicating whether the components of this <see cref="SpriteVertex"/> are the same as the
        /// components of the second specified <see cref="SpriteVertex"/>.
        /// </summary>
        /// <param name="obj">The object to compare, if it is a <see cref="SpriteVertex"/>.</param>
        /// <returns>true, if the components of both <see cref="SpriteVertex"/> are the same.</returns>
        public override bool Equals(object obj)
        {
            SpriteVertex? spriteVertex = obj as SpriteVertex?;
            if (spriteVertex.HasValue)
            {
                return spriteVertex == this;
            }
            return false;
        }

        /// <summary>
        /// Gets a hash code as an indication for object equality.
        /// </summary>
        /// <returns>The hash code.</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 461;
                hash *= 419 + Position.GetHashCode();
                hash *= 419 + TextureCoordinate.GetHashCode();
                hash *= 419 + Color.GetHashCode();
                return hash;
            }
        }

        /// <summary>
        /// Gets a string describing the components of this <see cref="SpriteVertex"/>.
        /// </summary>
        /// <returns>A string describing this <see cref="SpriteVertex"/>.</returns>
        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture,
                "{{Position={{{0}}},TextureCoordinate={{{1}}},Color={{{2}}}}}", Position, TextureCoordinate, Color);
        }

        /// <summary>
        /// Indicates whether the current <see cref="SpriteVertex"/> is equal to another <see cref="SpriteVertex"/>.
        /// </summary>
        /// <param name="other">A <see cref="SpriteVertex"/> to compare with this <see cref="SpriteVertex"/>.</param>
        /// <returns>true if the current <see cref="SpriteVertex"/> is equal to the other parameter; otherwise, false.
        /// </returns>
        public bool Equals(SpriteVertex other)
        {
            return this == other;
        }

        /// <summary>
        /// Indicates whether the current <see cref="SpriteVertex"/> is nearly equal to another
        /// <see cref="SpriteVertex"/>.
        /// </summary>
        /// <param name="other">A <see cref="SpriteVertex"/> to compare with this <see cref="SpriteVertex"/>.</param>
        /// <returns>true if the current <see cref="SpriteVertex"/> is nearly equal to the other parameter; otherwise, false.
        /// </returns>
        public bool NearlyEquals(SpriteVertex other)
        {
            return Position.NearlyEquals(other.Position) && TextureCoordinate.NearlyEquals(other.TextureCoordinate)
                && Color.NearlyEquals(other.Color);
        }
    }
}
