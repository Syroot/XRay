﻿using Syroot.Maths;
using Syroot.XRay.Video.OpenGL.Textures;

namespace Syroot.XRay.Video.Sprite
{
    /// <summary>
    /// Represents the information required to draw a sprite in the <see cref="SpriteBatch"/> drawing progress.
    /// </summary>
    internal class SpriteInfo
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="SpriteInfo"/> class with the given drawing parameters.
        /// </summary>
        /// <param name="texture">The <see cref="Texture2D"/> holding the texture data.</param>
        /// <param name="textureSize">The size of the texture in pixel.</param>
        /// <param name="origin">The origin in the image to which the position is relative, normalized.</param>
        /// <param name="rectangle">The area to fit the image in, in pixel.</param>
        /// <param name="rotationOrigin">The origin in the image around which will be rotated, normalized.</param>
        /// <param name="rotation">The rotation angle in radians.</param>
        /// <param name="tint">The colorization of the image, supporting the alpha component.</param>
        /// <param name="crop">The crop determining the part of the texture to be visible, in pixel.</param>
        internal SpriteInfo(Texture2D texture, Vector2 textureSize, Vector2F origin, RectangleF rectangle,
            Vector2F rotationOrigin, float rotation, ColorF tint, RectangleF crop)
        {
            Texture = texture;
            TextureSize = textureSize;
            Origin = origin;
            Rectangle = rectangle;

            RotationOrigin = rotationOrigin;
            RotationOriginAbsolute = RotationOrigin * Rectangle.Size;

            Rotation = rotation;
            Tint = tint;

            Crop = crop;
            CropRelative = new RectangleF(Crop.X / TextureSize.X, Crop.Y / TextureSize.Y,
                Crop.Width / TextureSize.X, Crop.Height / TextureSize.Y);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the <see cref="Texture2D"/> holding the texture data.
        /// </summary>
        internal Texture2D Texture
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the size of the texture in pixel.
        /// </summary>
        internal Vector2 TextureSize
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the origin in the image to which the position is relative, normalized.
        /// </summary>
        internal Vector2F Origin
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the are to fit the image in, in pixel.
        /// </summary>
        internal RectangleF Rectangle
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the origin in the image around which will be rotated, normalized.
        /// </summary>
        internal Vector2F RotationOrigin
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the origin in the image around which will be rotated, in pixel.
        /// </summary>
        internal Vector2F RotationOriginAbsolute
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Gets the rotation angle in radians.
        /// </summary>
        internal float Rotation
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the colorization of the image, supporting the alpha component.
        /// </summary>
        internal ColorF Tint
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the crop determining the part of the texture to be visible.
        /// </summary>
        internal RectangleF Crop
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the UV-based coordinate rectangle of the part of the texture which will be displayed.
        /// </summary>
        internal RectangleF CropRelative
        {
            get;
            private set;
        }
    }
}
