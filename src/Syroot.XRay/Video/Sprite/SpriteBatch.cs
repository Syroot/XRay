﻿using OpenTK.Graphics.OpenGL;
using Syroot.Maths;
using Syroot.XRay.Video.OpenGL;
using Syroot.XRay.Video.OpenGL.Buffers;
using Syroot.XRay.Video.OpenGL.Shaders;
using Syroot.XRay.Video.OpenGL.Textures;

namespace Syroot.XRay.Video.Sprite
{
    /// <summary>
    /// Represents a batch collecting <see cref="Image"/> draw calls to draw them in an optimized routine at once.
    /// </summary>
    internal class SpriteBatch
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _queueCapacity = 1024;
        private const int _verticesEachSprite = 4;
        private const int _indicesEachSprite = 6;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private SpriteInfo[] _queue;
        private int _queueIndex;

        private readonly SpriteVertex[] _vertices;
        private readonly short[] _indices;
        private VertexArrayObject _vertexArrayObject;
        private Buffer<float> _vertexBuffer;
        private Buffer<short> _indexBuffer;

        private ShaderProgram _shaderProgram;
        private Matrix4 _viewMatrix;
        private UniformVariable _viewMatrixUniform;
        private Sampler _textureSampler;
        private TextureFiltering _textureFiltering;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="SpriteBatch"/> class.
        /// </summary>
        internal SpriteBatch()
        {
            // Attach to the game.
            Game.ResolutionChanged += Game_ResolutionChanged;

            // Initialize the queue holding the draw calls and the arrays holding the data.
            _queue = new SpriteInfo[_queueCapacity];
            _vertices = new SpriteVertex[_queueCapacity * _verticesEachSprite];
            _indices = new short[_queueCapacity * _indicesEachSprite];

            // Create the vertex array object remembering the resources required to draw.
            _vertexArrayObject = new VertexArrayObject();
            using (_vertexArrayObject.TemporaryBind())
            {
                // Create the vertex buffer and bind it to tell the VAO about it.
                _vertexBuffer = new Buffer<float>(BufferTarget.ArrayBuffer, BufferUsageHint.DynamicDraw);
                _vertexBuffer.Bind();

                // Get the vertex array format attributes and enable these to tell the VAO about them.
                new VertexAttribute(0, VertexAttribPointerType.Float, 2, SpriteVertex.SizeInBytes, 0 * sizeof(float)).Enable();
                new VertexAttribute(1, VertexAttribPointerType.Float, 2, SpriteVertex.SizeInBytes, 2 * sizeof(float)).Enable();
                new VertexAttribute(2, VertexAttribPointerType.Float, 4, SpriteVertex.SizeInBytes, 4 * sizeof(float)).Enable();

                // Create the index buffer and bind it to tell the VAO about it.
                _indexBuffer = new Buffer<short>(BufferTarget.ElementArrayBuffer, BufferUsageHint.DynamicDraw);
                _indexBuffer.Bind();
            }
            // Unbinding the VAO also unbinds the index buffer, which the engine does not see.
            _indexBuffer.UnbindAny(); // This manual call fixes it in the engines state.

            // Create image shader program.
            using (VertexShader vertexShader = new VertexShader(Game.Resources.GetString("Shaders.Image.vert")))
            using (FragmentShader fragmentShader = new FragmentShader(Game.Resources.GetString("Shaders.Image.frag")))
            {
                _shaderProgram = new ShaderProgram(vertexShader, fragmentShader);
                _shaderProgram.Link();
            }
            // Set up the uniforms.
            _viewMatrixUniform = _shaderProgram.Uniforms["viewMatrix"];
            _shaderProgram.Uniforms["imageTexture"].SetData(0); // Just set it to use texture unit 0.
            UpdateViewMatrix(Game.Resolution);

            // Create the texture sampler, which is bound to texture unit 0.
            _textureFiltering = TextureFiltering.Linear;
            _textureSampler = new Sampler()
            {
                MagnificationFilter = TextureMagFilter.Linear,
                MinificationFilter = TextureMinFilter.Linear,
                WrapMode = TextureWrapMode.ClampToEdge // To get rid of anti-aliasing bug.
            };
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the <see cref="TextureFiltering"/> quality used to draw images and text.
        /// </summary>
        internal TextureFiltering Filtering
        {
            get => _textureFiltering;
            set
            {
                if (_textureFiltering != value)
                {
                    _textureFiltering = value;
                    _textureSampler.MagnificationFilter = _textureFiltering.ToTextureMagFilter();
                    _textureSampler.MinificationFilter = _textureFiltering.ToTextureMinFilter();
                }
            }
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Creates a <see cref="SpriteInfo"/> instance and queues it to be drawn with the given settings.
        /// </summary>
        /// <param name="texture">The texture holding the image.</param>
        /// <param name="textureSize">The size of the texture in pixel.</param>
        /// <param name="origin">The origin in the image to which the position is relative, normalized.</param>
        /// <param name="rectangle">The area to fit the image in, in pixel.</param>param>
        /// <param name="rotationOrigin">The origin in the image around which will be rotated, normalized.</param>
        /// <param name="rotation">The rotation in radians.</param>
        /// <param name="tint">The colorization of the image, supporting the alpha component.</param>
        /// <param name="crop">The crop determining the part of the texture to be visible, in pixel.</param>
        internal void Draw(Texture2D texture, Vector2 textureSize, Vector2F origin, RectangleF rectangle,
            Vector2F rotationOrigin, float rotation, ColorF tint, RectangleF crop)
        {
            // Create a sprite info out of the parameters and put it into the array.
            _queue[_queueIndex++] = new SpriteInfo(texture, textureSize, origin, rectangle, rotationOrigin, rotation,
                tint, crop);

            // If the queue ran full, flush it to make space for new draw commands.
            if (_queueIndex == _queueCapacity)
            {
                Present();
            }
        }

        /// <summary>
        /// Removes any queued drawing calls.
        /// </summary>
        internal void Clear()
        {
            _queue = new SpriteInfo[_queueCapacity];
            _queueIndex = 0;
        }

        /// <summary>
        /// Renders all the collected <see cref="SpriteInfo"/> instances. These might not be all of those created by
        /// previous <see cref="Draw"/> calls, since the rendering may be issued if the internal vertex buffer ran full.
        /// </summary>
        internal void Present()
        {
            // Do not do anything if we have nothing to draw.
            if (_queueIndex == 0)
            {
                return;
            }

            // Go through all queued SpriteInfo instances.
            int startIndex = 0;
            Texture2D previousTexture = null;
            for (int i = 0; i < _queueIndex; i++)
            {
                SpriteInfo spriteInfo = _queue[i];

                // Check if the new sprite info has another texture than previous ones.
                if (spriteInfo.Texture != previousTexture)
                {
                    // Do not issue a draw command for the very first sprite (since previousTexture was null).
                    if (previousTexture != null)
                    {
                        // Render a slice of the batch with the previous texture.
                        PresentSlice(startIndex, i - startIndex);
                    }
                    startIndex = i;
                    previousTexture = spriteInfo.Texture;
                }
            }

            // Draw the remaining sprites and then clear the queue.
            PresentSlice(startIndex, _queueIndex - startIndex);
            Clear();
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void UpdateViewMatrix(Vector2 resolution)
        {
            // Update the 2D projection matrix to map coordinates to the screen.
            _viewMatrix = Matrix4.GetOrthographicMatrix(0f, resolution.X, resolution.Y, 0f, -1f, 1f);
            _viewMatrixUniform.SetData(ref _viewMatrix);
        }

        private void PresentSlice(int startIndex, int count)
        {
            // Go through each SpriteInfo in the slice.
            for (int i = 0; i < count; i++)
            {
                SpriteInfo spriteInfo = _queue[i + startIndex];

                // Calculate the final position and size vertices.
                RectangleF rect = spriteInfo.Rectangle;
                Vector2F origin = spriteInfo.Origin;
                Vector2F size = spriteInfo.Rectangle.Size;

                // Rotate around the default position first, then translate.
                Vector2F topLeft = new Vector2F(0f, 0f);
                Vector2F topRight = new Vector2F(rect.Width, 0f);
                Vector2F bottomLeft = new Vector2F(0f, rect.Height);
                Vector2F bottomRight = new Vector2F(rect.Width, rect.Height);

                topLeft = RotatePoint(topLeft, spriteInfo.RotationOriginAbsolute, spriteInfo.Rotation);
                topRight = RotatePoint(topRight, spriteInfo.RotationOriginAbsolute, spriteInfo.Rotation);
                bottomLeft = RotatePoint(bottomLeft, spriteInfo.RotationOriginAbsolute, spriteInfo.Rotation);
                bottomRight = RotatePoint(bottomRight, spriteInfo.RotationOriginAbsolute, spriteInfo.Rotation);

                Vector2F translation = rect.Position - origin * size;
                topLeft += translation;
                topRight += translation;
                bottomLeft += translation;
                bottomRight += translation;

                // Add the vertices to the array.
                int verticesArrayOffset = i * _verticesEachSprite;
                _vertices[verticesArrayOffset + 0] = new SpriteVertex(new Vector2F(topLeft.X, topLeft.Y),
                    new Vector2F(spriteInfo.CropRelative.X, spriteInfo.CropRelative.Y), spriteInfo.Tint);
                _vertices[verticesArrayOffset + 1] = new SpriteVertex(new Vector2F(topRight.X, topRight.Y),
                    new Vector2F(spriteInfo.CropRelative.X2, spriteInfo.CropRelative.Y), spriteInfo.Tint);
                _vertices[verticesArrayOffset + 2] = new SpriteVertex(new Vector2F(bottomLeft.X, bottomLeft.Y),
                    new Vector2F(spriteInfo.CropRelative.X, spriteInfo.CropRelative.Y2), spriteInfo.Tint);
                _vertices[verticesArrayOffset + 3] = new SpriteVertex(new Vector2F(bottomRight.X, bottomRight.Y),
                    new Vector2F(spriteInfo.CropRelative.X2, spriteInfo.CropRelative.Y2), spriteInfo.Tint);

                // Add the indices to the array
                int indicesArrayOffset = i * _indicesEachSprite;
                // Top left triangle (◤)
                _indices[indicesArrayOffset + 0] = (short)(verticesArrayOffset + 0);
                _indices[indicesArrayOffset + 1] = (short)(verticesArrayOffset + 1);
                _indices[indicesArrayOffset + 2] = (short)(verticesArrayOffset + 2);
                // Bottom right triangle (◢)
                _indices[indicesArrayOffset + 3] = (short)(verticesArrayOffset + 1);
                _indices[indicesArrayOffset + 4] = (short)(verticesArrayOffset + 2);
                _indices[indicesArrayOffset + 5] = (short)(verticesArrayOffset + 3);
            }

            // Get unique texture for SpriteInfo instances and bind them to texture unit 0 with the sampler.
            Rendering.ActiveTextureUnit = 0;
            _textureSampler.Bind(0);
            _queue[startIndex].Texture.Bind();

            // Bind the VAO of the mesh to load the vertex buffer locations, vertex array formats and index buffer.
            using (_vertexArrayObject.TemporaryBind())
            {
                // Send the prepared vertex and index data to the buffers.
                _vertexBuffer.SetData(_vertices);
                _indexBuffer.SetData(_indices);

                // Draw the vertices referenced through the indices.
                Log.WriteInfo(LogCategory.OpenGL, "DrawElements({0}, {1}, {2}, {3})", PrimitiveType.Triangles,
                    count * _indicesEachSprite, DrawElementsType.UnsignedShort, 0/*unused*/);
                GL.DrawElements(PrimitiveType.Triangles, count * _indicesEachSprite, DrawElementsType.UnsignedShort,
                    0/*unused*/);
            }
        }

        private Vector2F RotatePoint(Vector2F point, Vector2F origin, float radians)
        {
            // Translate the point back to origin.
            point.X -= origin.X;
            point.Y -= origin.Y;

            // Rotate clockwise.
            float sin = Algebra.Sin(radians);
            float cos = Algebra.Cos(radians);
            float xNew = point.X * cos + point.Y * sin;
            float yNew = -point.X * sin + point.Y * cos;

            // Translate point back.
            point.X = xNew + origin.X;
            point.Y = yNew + origin.Y;

            return point;
        }

        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

        private void Game_ResolutionChanged(object sender, ResolutionChangedEventArgs e)
        {
            UpdateViewMatrix(e.NewResolution);
        }
    }
}
