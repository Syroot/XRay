﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using Syroot.Maths;
using Syroot.XRay.Video.BMFont;

namespace Syroot.XRay.Video
{
    /// <summary>
    /// Represents the typeface of a font which is used to draw text.
    /// A typeface can be loaded from a BMFont binary file, zipped together with the referenced page textures.
    /// S. <see href="http://www.angelcode.com/products/bmfont/"/> for more details.
    /// </summary>
    public class Font : IDisposable
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private bool _isDisposed;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="Font"/> class from the given <see cref="Stream"/> with ZIP
        /// contents of the BMFont binary declaration file (extension FNT) and referenced page textures.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to create the font from.</param>
        public Font(Stream stream)
        {
            ZipArchive zipArchive = new ZipArchive(stream, ZipArchiveMode.Read);
            // Find the FNT file entry and deserialize it.
            foreach (ZipArchiveEntry entry in zipArchive.Entries)
            {
                if (Path.GetExtension(entry.Name).ToUpperInvariant() == ".FNT")
                {
                    ReadMapping(entry);
                    CacheCharTexCoords();
                    return;
                }
            }
            throw new InvalidOperationException("No FNT mapping file found in the stream to create the font from.");
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the name of the typeface.
        /// </summary>
        public string FontName
        {
            get { return FontFile.Info.Face; }
        }

        /// <summary>
        /// Gets the pixel-based size at which this typeface looks best.
        /// </summary>
        public int FontSize
        {
            get { return FontFile.Info.Size; }
        }

        /// <summary>
        /// Gets the style which modifies the appearance of this typeface.
        /// </summary>
        public FontStyle FontStyle
        {
            get
            {
                FontStyle fontStyle = FontStyle.None;
                if (FontFile.Info.Bold) fontStyle |= FontStyle.Bold;
                if (FontFile.Info.Italic) fontStyle |= FontStyle.Italic;
                return fontStyle;
            }
        }

        /// <summary>
        /// Gets the height of each line.
        /// </summary>
        public int LineHeight
        {
            get { return FontFile.Common.LineHeight; }
        }

        /// <summary>
        /// Gets the <see cref="FontFile"/> which contains the font mapping and rendering information.
        /// </summary>
        internal FontFile FontFile
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the font page textures containing the glyphs.
        /// </summary>
        internal Image[] PageImages
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the UV mapping coordinates of each character.
        /// </summary>
        internal Dictionary<FontChar, RectangleF> CharTexCoords
        {
            get;
            private set;
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Draws the given text into the specified rectangle with the given alignment and color tint.
        /// </summary>
        /// <param name="text">The text to draw.</param>
        /// <param name="rectangle">The rectangle in which the text will be aligned.</param>
        /// <param name="hAlign">The horizontal positioning method of the drawn text.</param>
        /// <param name="vAlign">The vertical positioning method of the drawn text.</param>
        /// <param name="tint">The color of the text.</param>
        public void Draw(string text, Rectangle? rectangle = null, Alignment hAlign = Alignment.Near,
            Alignment vAlign = Alignment.Near, ColorF? tint = null)
        {
            RectangleF finalRectangle = rectangle ?? new RectangleF(Vector2F.Zero, Vector2F.Zero);

            Draw(text, finalRectangle.Position, finalRectangle.Size, hAlign, vAlign, tint);
        }
        
        /// <summary>
        /// Draws the given text into the specified rectangle with the given alignment and color tint.
        /// </summary>
        /// <param name="text">The text to draw.</param>
        /// <param name="rectangle">The rectangle in which the text will be aligned.</param>
        /// <param name="hAlign">The horizontal positioning method of the drawn text.</param>
        /// <param name="vAlign">The vertical positioning method of the drawn text.</param>
        /// <param name="tint">The color of the text.</param>
        public void Draw(string text, RectangleF? rectangle = null, Alignment hAlign = Alignment.Near,
            Alignment vAlign = Alignment.Near, ColorF? tint = null)
        {
            RectangleF finalRectangle = rectangle ?? new RectangleF(Vector2F.Zero, Vector2F.Zero);

            Draw(text, finalRectangle.Position, finalRectangle.Size, hAlign, vAlign, tint);
        }

        /// <summary>
        /// Draws the given text at the specified position with the given alignment and color tint.
        /// </summary>
        /// <param name="text">The text to draw.</param>
        /// <param name="position">The position at which the text will be drawn.</param>
        /// <param name="size">The size to which the text will be aligned.</param>
        /// <param name="hAlign">The horizontal positioning method of the drawn text.</param>
        /// <param name="vAlign">The vertical positioning method of the drawn text.</param>
        /// <param name="tint">The color of the text.</param>
        public void Draw(string text, Vector2? position = null, Vector2? size = null,
            Alignment hAlign = Alignment.Near, Alignment vAlign = Alignment.Near, ColorF? tint = null)
        {
            Draw(text, (Vector2F?)position, size, hAlign, vAlign, tint);
        }

        /// <summary>
        /// Draws the given text at the specified position with the given alignment and color tint.
        /// </summary>
        /// <param name="text">The text to draw.</param>
        /// <param name="position">The position at which the text will be drawn.</param>
        /// <param name="size">The size to which the text will be aligned.</param>
        /// <param name="hAlign">The horizontal positioning method of the drawn text.</param>
        /// <param name="vAlign">The vertical positioning method of the drawn text.</param>
        /// <param name="tint">The color of the text.</param>
        public void Draw(string text, Vector2F? position = null, Vector2F? size = null,
            Alignment hAlign = Alignment.Near, Alignment vAlign = Alignment.Near, ColorF? tint = null)
        {
            // Set up default values.
            position = position ?? Vector2F.Zero;
            size = size ?? Vector2.One;
            tint = tint ?? ColorF.White;

            TextRenderInfo renderInfo = new TextRenderInfo(this, text, (Vector2)size.Value, hAlign, vAlign);
            for (int i = 0; i < renderInfo.MappedFontChars.Count; i++)
            {
                FontChar fontChar = renderInfo.MappedFontChars[i];

                // Ignore non-existent mappings for unmapped characters.
                if (fontChar.ID == 0)
                {
                    continue;
                }

                // Get the screen and texture crop and draw the character with it.
                Rectangle screenRectangle = renderInfo.MappedRectangles[i];
                RectangleF textureRectangle = CharTexCoords[fontChar];
                PageImages[fontChar.Page].Draw(screenRectangle.Position + position, screenRectangle.Size, tint: tint,
                    crop: textureRectangle);
            }
        }

        /// <summary>
        /// Gets the size required to draw the specified text.
        /// </summary>
        /// <param name="text">The text to measure.</param>
        /// <returns>The size required to draw the text.</returns>
        public Vector2 Measure(string text)
        {
            TextRenderInfo renderInfo = new TextRenderInfo(this, text, Vector2.Zero, Alignment.Near, Alignment.Near);
            return renderInfo.RequiredSize;
        }

        /// <summary>
        /// Gets a value indicating whether the specified character is mapped in this font and can be drawn.
        /// </summary>
        /// <param name="character">The character to check.</param>
        /// <returns><c>true</c> if the character can be drawn, otherwise <c>false</c>.</returns>
        public bool SupportsCharacter(char character)
        {
            foreach (FontChar fontChar in CharTexCoords.Keys)
            {
                if (fontChar.ID == character)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release
        /// only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                }
                
                _isDisposed = true;
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void ReadMapping(ZipArchiveEntry entry)
        {
            // Deserialize the XML mapping file.
            using (Stream entryStream = entry.Open())
            {
                FontFile = FontFile.Instantiate(entryStream);
            }

            // Load the page textures out of the ZIP file.
            PageImages = new Image[FontFile.Pages.Count];
            foreach (FontPage page in FontFile.Pages)
            {
                foreach (ZipArchiveEntry imageEntry in entry.Archive.Entries)
                {
                    // Check if this is the image with the referenced file name.
                    if (imageEntry.Name.ToUpperInvariant() == page.File.ToUpperInvariant())
                    {
                        // Read in the bytes of the image, as seeking is required.
                        using (MemoryStream imageDataStream = new MemoryStream((int)imageEntry.Length))
                        using (BinaryReader imageReader = new BinaryReader(imageEntry.Open()))
                        {
                            imageDataStream.Write(imageReader.ReadBytes((int)imageEntry.Length), 0,
                                (int)imageEntry.Length);
                            PageImages[page.ID] = new Image(imageDataStream);
                        }
                        break;
                    }
                }
            }
        }
    
        private void CacheCharTexCoords()
        {
            CharTexCoords = new Dictionary<FontChar, RectangleF>(FontFile.Chars.Count);

            // Fill the texture crop for each character.
            foreach (FontChar fontChar in FontFile.Chars)
            {
                CharTexCoords.Add(fontChar, new RectangleF(fontChar.X, fontChar.Y, fontChar.Width, fontChar.Height));
            }
        }
    }

    /// <summary>
    /// Represents the list of possible styles which can modify the appearance of a <see cref="Font"/> typeface.
    /// </summary>
    [Flags]
    public enum FontStyle
    {
        /// <summary>
        /// The typeface outlines are outset.
        /// </summary>
        None = 0,

        /// <summary>
        /// The typeface outlines are outset.
        /// </summary>
        Bold = 1 << 0,

        /// <summary>
        /// The typeface outlines are skewed.
        /// </summary>
        Italic = 1 << 1
    }
}
