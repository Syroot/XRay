﻿using System;
using System.Collections.Generic;

namespace Syroot.XRay.Video.OpenGL
{
    /// <summary>
    /// Represents the base for any OpenGL-related resource which can be bound to multiple targets.
    /// </summary>
    /// <typeparam name="T">The type of object being an OpenGL object.</typeparam>
    /// <typeparam name="TTarget">The type of the target type making this object unique.</typeparam>
    internal abstract class OpenGLTargetObject<T, TTarget> : IDisposable
        where T : OpenGLTargetObject<T, TTarget>
        where TTarget : struct
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static List<T> _instances = new List<T>();

        private bool _isDisposed;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes static members of the <see cref="OpenGLTargetObject{T, TTarget}"/> class.
        /// </summary>
        static OpenGLTargetObject()
        {
            BoundInstances = new Dictionary<TTarget, T>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGLTargetObject{T, TTarget}"/> class.
        /// </summary>
        internal OpenGLTargetObject()
        {
            // Add to the list of instances created.
            _instances.Add((T)this);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the currently bound instances depending on the target.
        /// </summary>
        internal static Dictionary<TTarget, T> BoundInstances
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the handle of this OpenGL resource.
        /// </summary>
        internal int Handle
        {
            get;
            set;
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Retrieves an existing instance with the given handle or <c>null</c> if no instance has such handle.
        /// </summary>
        /// <param name="handle">The handle of the instance to retrieve.</param>
        /// <returns>The instance with the given handle or <c>null</c> if no instance has such handle.</returns>
        internal static T ByHandle(int handle)
        {
            foreach (T instance in _instances)
            {
                if (instance.Handle == handle)
                {
                    return instance;
                }
            }
            return null;
        }

        /// <summary>
        /// Makes sure that the object is the currently bound one of the given target and binds it if it is not.
        /// </summary>
        /// <param name="target">The target to bind the object to.</param>
        internal void Bind(TTarget target)
        {
            if (BoundInstances.TryGetValue(target, out T currentlyBound))
            {
                // Target is already in the list, find the currently bound instance and set the new one.
                if (currentlyBound == null || Handle != currentlyBound.Handle)
                {
                    DoBind(target);
                    BoundInstances[target] = (T)this;
                }
            }
            else
            {
                // Add the target and current object to the list of bound instances.
                DoBind(target);
                BoundInstances.Add(target, (T)this);
            }
        }

        /// <summary>
        /// Creates an <see cref="OpenGLTargetBindTask{T, TTarget}"/> for this instance. As soon as the returned
        /// <see cref="OpenGLTargetBindTask{T, TTarget}"/> is disposed, all instances of this type and target are
        /// unbound again.
        /// Do not use this if you can simply bind another object later. This task should be used if belated unbinding
        /// can change the state of the bound object, as it does for <see cref="Buffers.VertexArrayObject"/> instances,
        /// for example.
        /// </summary>
        /// <param name="target">The target to bind the object to.</param>
        /// <returns>An <see cref="OpenGLTargetBindTask{T, TTarget}"/> to be disposed to undo the binding.</returns>
        internal OpenGLTargetBindTask<T, TTarget> TemporaryBind(TTarget target)
        {
            return new OpenGLTargetBindTask<T, TTarget>(this, target);
        }

        /// <summary>
        /// Unbinds an active instance of the type and target of this object if one is bound.
        /// </summary>
        /// <param name="target">The target to unbind any object from.</param>
        internal void UnbindAny(TTarget target)
        {
            if (BoundInstances.TryGetValue(target, out T currentlyBound))
            {
                currentlyBound.DoUnbind(target);
                BoundInstances.Remove(target);
            }
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Called when this <see cref="OpenGLTargetObject{T, TTarget}"/> requires binding.
        /// </summary>
        /// <param name="target">The target to bind the object to.</param>
        protected abstract void DoBind(TTarget target);

        /// <summary>
        /// Called when this <see cref="OpenGLTargetObject{T, TTarget}"/> requires unbinding.
        /// </summary>
        /// <param name="target">The target to unbind any object from.</param>
        protected abstract void DoUnbind(TTarget target);
        
        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release
        /// only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    _instances.Remove((T)this);
                }

                _isDisposed = true;
            }
        }
    }
}
