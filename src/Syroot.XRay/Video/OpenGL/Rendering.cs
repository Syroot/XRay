﻿using OpenTK.Graphics.OpenGL;

namespace Syroot.XRay.Video.OpenGL
{
    /// <summary>
    /// Represents OpenGL states which can be set at any time and affect rendering.
    /// </summary>
    internal static class Rendering
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static int _activeTextureUnit;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the currently active texture unit on which texture operations apply.
        /// </summary>
        internal static int ActiveTextureUnit
        {
            get
            {
                return _activeTextureUnit;
            }
            set
            {
                // Only set the new unit if it is not the currently bound one.
                if (_activeTextureUnit != value)
                {
                    _activeTextureUnit = value;
                    Log.WriteInfo(LogCategory.OpenGL, "ActiveTexture({0})", (TextureUnit)(TextureUnit.Texture0
                        + _activeTextureUnit));
                    GL.ActiveTexture((TextureUnit)(TextureUnit.Texture0 + _activeTextureUnit));
                }
            }
        }
    }
}
