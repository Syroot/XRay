﻿using System;
using OpenTK.Graphics.OpenGL;
using Syroot.Maths;

namespace Syroot.XRay.Video.OpenGL.Textures
{
    /// <summary>
    /// Represents a 2-dimensional texture, mostly used for images.
    /// </summary>
    internal class Texture2D : TextureBase<Texture2D>
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="Texture2D"/> class.
        /// </summary>
        internal Texture2D()
            : base(TextureTarget.Texture2D)
        {
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Sets the data referenced by this texture and makes it visible to OpenGL. Call this whenever you make changes
        /// in your code to the referenced array. This binds the texture if necessary.
        /// </summary>
        /// <param name="data">The data array which will be uploaded to OpenGL in XY transform.</param>
        internal void SetData(Color[,] data)
        {
            if (data == null) throw new ArgumentNullException("data");
            if (data.GetLength(0) == 0 || data.GetLength(1) == 0) throw new ArgumentOutOfRangeException("data");

            Bind();

            Log.WriteInfo(LogCategory.OpenGL, "TexImage2D({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, <data>)",
                TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba8, data.GetLength(1), data.GetLength(0),
                0, PixelFormat.Rgba, PixelType.Byte);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba8, data.GetLength(1), data.GetLength(0),
                0, PixelFormat.Rgba, PixelType.UnsignedByte, data);
        }

        /// <summary>
        /// Sets the data referenced by this texture and makes it visible to OpenGL. Call this whenever you make changes
        /// in your code to the referenced array. This binds the texture if necessary.
        /// </summary>
        /// <param name="data">The raw RGBA data array which will be uploaded to OpenGL in XY transform.</param>
        /// <param name="size">The dimensions of the image, sequentially aligned in the byte array.</param>
        internal void SetData(byte[] data, Vector2 size)
        {
            if (data == null) throw new ArgumentNullException("data");
            if (data.Length == 0) throw new ArgumentOutOfRangeException("data");

            Bind();

            Log.WriteInfo(LogCategory.OpenGL, "TexImage2D({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, <data>)",
                TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, size.X, size.Y, 0, PixelFormat.Rgba,
                PixelType.UnsignedByte);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, size.X, size.Y, 0, PixelFormat.Rgba,
                PixelType.UnsignedByte, data);
        }

        /// <summary>
        /// Sets the size of the texture. This binds the texture if necessary.
        /// </summary>
        /// <param name="size">The dimensions of the image.</param>
        internal void SetSize(Vector2 size)
        {
            Bind();

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, size.X, size.Y, 0, PixelFormat.Rgba,
                PixelType.UnsignedByte, IntPtr.Zero);
        }
    }
}
