﻿using System;
using OpenTK.Graphics.OpenGL;

namespace Syroot.XRay.Video.OpenGL.Textures
{
    /// <summary>
    /// Represents a sampler object specifying how a texture is accessed through a shader sampler.
    /// </summary>
    internal class Sampler : OpenGLTargetObject<Sampler, int>, IDisposable
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private bool _isDisposed;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private TextureMinFilter _minificationFilter;
        private TextureMagFilter _magnificationFilter;
        private TextureWrapMode  _wrapMode;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="Sampler"/> class.
        /// </summary>
        internal Sampler()
        {
            Handle = GL.GenSampler();
            Log.WriteInfo(LogCategory.OpenGL, "GenSampler() = {0}", Handle);

            // Find out the default values for the properties.
            GL.GetSamplerParameter(Handle, SamplerParameterName.TextureMinFilter, out int value);
            _minificationFilter = (TextureMinFilter)value;
            Log.WriteInfo(LogCategory.OpenGL, "GetSamplerParameter({0}, {1}) = {2}", Handle,
                SamplerParameterName.TextureMinFilter, _minificationFilter);

            GL.GetSamplerParameter(Handle, SamplerParameterName.TextureMagFilter, out value);
            _magnificationFilter = (TextureMagFilter)value;
            Log.WriteInfo(LogCategory.OpenGL, "GetSamplerParameter({0}, {1}) = {2}", Handle,
                SamplerParameterName.TextureMagFilter, _magnificationFilter);

            GL.GetSamplerParameter(Handle, SamplerParameterName.TextureWrapS, out value);
            _wrapMode = (TextureWrapMode)value;
            Log.WriteInfo(LogCategory.OpenGL, "GetSamplerParameter({0}, {1}) = {2}", Handle,
                SamplerParameterName.TextureWrapS, _wrapMode);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the method used to draw textures smaller than their native size.
        /// </summary>
        internal TextureMinFilter MinificationFilter
        {
            get
            {
                return _minificationFilter;
            }
            set
            {
                if (_minificationFilter != value)
                {
                    _minificationFilter = value;
                    GL.SamplerParameter(Handle, SamplerParameterName.TextureMinFilter, (int)_minificationFilter);
                    Log.WriteInfo(LogCategory.OpenGL, "SamplerParameter({0}, {1}, {2})", Handle,
                        SamplerParameterName.TextureMinFilter, _minificationFilter);
                }
            }
        }

        /// <summary>
        /// Gets or sets the method used to draw textures bigger than their native size.
        /// </summary>
        internal TextureMagFilter MagnificationFilter
        {
            get
            {
                return _magnificationFilter;
            }
            set
            {
                if (_magnificationFilter != value)
                {
                    _magnificationFilter = value;
                    GL.SamplerParameter(Handle, SamplerParameterName.TextureMagFilter, (int)_magnificationFilter);
                    Log.WriteInfo(LogCategory.OpenGL, "SamplerParameter({0}, {1}, {2})", Handle,
                        SamplerParameterName.TextureMagFilter, _magnificationFilter);
                }
            }
        }

        /// <summary>
        /// Gets or sets the method used to draw textures outside their normalized coordinate range in all directions.
        /// </summary>
        internal TextureWrapMode WrapMode
        {
            get
            {
                return _wrapMode;
            }
            set
            {
                if (_wrapMode != value)
                {
                    _wrapMode = value;
                    GL.SamplerParameter(Handle, SamplerParameterName.TextureWrapS, (int)_wrapMode);
                    Log.WriteInfo(LogCategory.OpenGL, "SamplerParameter({0}, {1}, {2})", Handle,
                        SamplerParameterName.TextureWrapS, _wrapMode);
                    GL.SamplerParameter(Handle, SamplerParameterName.TextureWrapT, (int)_wrapMode);
                    Log.WriteInfo(LogCategory.OpenGL, "SamplerParameter({0}, {1}, {2})", Handle,
                        SamplerParameterName.TextureWrapT, _wrapMode);
                    GL.SamplerParameter(Handle, SamplerParameterName.TextureWrapR, (int)_wrapMode);
                    Log.WriteInfo(LogCategory.OpenGL, "SamplerParameter({0}, {1}, {2})", Handle,
                        SamplerParameterName.TextureWrapR, _wrapMode);
                }
            }
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Called when this <see cref="OpenGLTargetObject{T, TTarget}"/> requires binding.
        /// </summary>
        /// <param name="target">The target to bind the object to.</param>
        protected override void DoBind(int target)
        {
            Log.WriteInfo(LogCategory.OpenGL, "BindSampler({0}, {1})", target, Handle);
            GL.BindSampler(target, Handle);
        }
        
        /// <summary>
        /// Called when this <see cref="OpenGLTargetObject{T, TTarget}"/> requires unbinding.
        /// </summary>
        /// <param name="target">The target to unbind any object from.</param>
        protected override void DoUnbind(int target)
        {
            Log.WriteInfo(LogCategory.OpenGL, "BindSampler({0}, 0)", target);
            GL.BindSampler(target, 0);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release
        /// only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    Log.WriteInfo(LogCategory.OpenGL, "DeleteSampler({0})", Handle);
                    GL.DeleteSampler(Handle);
                }

                Handle = 0;

                _isDisposed = true;
            }

            base.Dispose(disposing);
        }
    }
}
