﻿using System;
using OpenTK.Graphics.OpenGL;

namespace Syroot.XRay.Video.OpenGL.Textures
{
    /// <summary>
    /// Represents the basic functionality of any OpenGL texture.
    /// </summary>
    /// <typeparam name="T">The texture class implementing a specific <see cref="TextureTarget"/>.</typeparam>
    internal abstract class TextureBase<T> : OpenGLObject<T>, IDisposable
        where T : TextureBase<T>
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly TextureTarget _target;

        private bool _isDisposed;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="TextureBase{T}"/> class for the given texture usage target.
        /// </summary>
        /// <param name="target">The <see cref="TextureTarget"/> to which this texture will be bound.</param>
        internal TextureBase(TextureTarget target)
        {
            _target = target;
            Handle = GL.GenTexture();
            Log.WriteInfo(LogCategory.OpenGL, "GenTexture() = {0}", Handle);
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Makes this texture the currently bound one.
        /// </summary>
        protected override void DoBind()
        {
            Log.WriteInfo(LogCategory.OpenGL, "BindTexture({0}, {1})", _target, Handle);
            GL.BindTexture(_target, Handle);
        }

        /// <summary>
        /// Unbinds any texture.
        /// </summary>
        protected override void DoUnbind()
        {
            Log.WriteInfo(LogCategory.OpenGL, "BindTexture({0}, {1})", _target, Handle);
            GL.BindTexture(_target, 0);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release
        /// only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    Log.WriteInfo(LogCategory.OpenGL, "DeleteTexture({0})", Handle);
                    GL.DeleteTexture(Handle);
                }

                _isDisposed = true;
            }

            base.Dispose(disposing);
        }
    }
}
