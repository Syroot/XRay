﻿using System;
using OpenTK.Graphics.OpenGL;

namespace Syroot.XRay.Video.OpenGL.Textures
{
    /// <summary>
    /// Represents a 1-dimensional texture, mostly used for data calculated with for further display.
    /// </summary>
    internal class Texture1D : TextureBase<Texture1D>
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="Texture1D"/> class.
        /// </summary>
        internal Texture1D()
            : base(TextureTarget.Texture1D)
        {
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Sets the data referenced by this texture and makes it visible to OpenGL. Call this whenever you make changes
        /// in your code to the referenced array. This binds the texture if necessary.
        /// </summary>
        /// <param name="data">The data array which will be uploaded to OpenGL.</param>
        internal void SetData(byte[] data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            Bind();

            // Set the data according to C# bytes.
            Log.WriteInfo(LogCategory.OpenGL, "TexImage1D({0}, {1}, {2}, {3}, {4}, {5}, {6}, <data>)",
                TextureTarget.Texture1D, 0, PixelInternalFormat.R8, data.Length, 0, PixelFormat.Red,
                PixelType.UnsignedByte);
            GL.TexImage1D(TextureTarget.Texture1D, 0, PixelInternalFormat.R8, data.Length, 0, PixelFormat.Red,
                PixelType.UnsignedByte, data);
        }
    }
}
