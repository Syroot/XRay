﻿using System;

namespace Syroot.XRay.Video.OpenGL
{
    /// <summary>
    /// Represents a temporary binding of an <see cref="OpenGLObject"/> which is unbound after the task has been
    /// disposed.
    /// </summary>
    /// <typeparam name="T">The type of object being an OpenGL object.</typeparam>
    internal class OpenGLBindTask<T> : IDisposable
        where T : OpenGLObject<T>
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGLBindTask{T}"/> class to temporarily bind the given
        /// <see cref="OpenGLObject"/>. The <see cref="OpenGLObject"/> is unbound after the task is disposed.
        /// </summary>
        /// <param name="openGLObject">A <see cref="OpenGLObject"/> to temporarily bind.</param>
        internal OpenGLBindTask(OpenGLObject<T> openGLObject)
        {
            OpenGLObject = openGLObject;
            OpenGLObject.Bind();
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the <see cref="OpenGLObject"/> which is temporarily bound.
        /// </summary>
        internal OpenGLObject<T> OpenGLObject
        {
            get;
            private set;
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Unbinds the <see cref="OpenGLObject"/>.
        /// </summary>
        public void Dispose()
        {
            OpenGLObject.UnbindAny();
        }
    }
}
