﻿using System;
using System.Runtime.Serialization;

namespace Syroot.XRay.Video.OpenGL.Shaders
{
    /// <summary>
    /// Raised when an exception has occurred in a <see cref="ShaderProgram"/>.
    /// </summary>
    [Serializable]
    internal class ShaderProgramException : Exception
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="ShaderProgramException"/> class.
        /// </summary>
        internal ShaderProgramException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShaderProgramException"/> class with a specified error message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        internal ShaderProgramException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShaderProgramException"/> class with a specified error message
        /// and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference
        /// (Nothing in Visual Basic) if no inner exception is specified.</param>
        internal ShaderProgramException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShaderProgramException"/> class for the specified
        /// <see cref="ShaderProgram"/> with the specified error message.
        /// </summary>
        /// <param name="program">The <see cref="ShaderProgram"/> which caused the exception.</param>
        /// <param name="message">The message to display in the error.</param>
        internal ShaderProgramException(ShaderProgram program, string message)
            : base(message)
        {
            Program = program;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShaderProgramException"/> class with serialized data.
        /// </summary>
        /// <param name="info">The <see cref="System.Runtime.Serialization.SerializationInfo"/> that holds the serialized
        /// object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="System.Runtime.Serialization.SerializationInfo"/> that contains
        /// contextual information about the source or destination.</param>
        protected ShaderProgramException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the <see cref="ShaderProgram"/> which caused the exception.
        /// </summary>
        internal ShaderProgram Program
        {
            get;
            private set;
        }
    }
}
