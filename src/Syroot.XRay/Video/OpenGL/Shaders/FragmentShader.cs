﻿using System.Collections.Generic;
using OpenTK.Graphics.OpenGL;

namespace Syroot.XRay.Video.OpenGL.Shaders
{
    /// <summary>
    /// A shader taking the output from a vertex shader and coloring the pixels.
    /// </summary>
    internal class FragmentShader : ShaderBase
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="FragmentShader"/> class with the specified GLSL code.
        /// </summary>
        /// <param name="code">The GLSL code of the shader.</param>
        internal FragmentShader(string code)
            : base(ShaderType.FragmentShader, new string[] { code })
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FragmentShader"/> class with the specified GLSL code units.
        /// </summary>
        /// <param name="codeUnits">The GLSL code units of the shader.</param>
        internal FragmentShader(IEnumerable<string> codeUnits)
            : base(ShaderType.VertexShader, codeUnits)
        {
        }
    }
}
