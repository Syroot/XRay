﻿using System.Collections.Generic;
using OpenTK.Graphics.OpenGL;

namespace Syroot.XRay.Video.OpenGL.Shaders
{
    /// <summary>
    /// A shader transforming vertex positions.
    /// </summary>
    internal class VertexShader : ShaderBase
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="VertexShader"/> class with the specified GLSL code.
        /// </summary>
        /// <param name="code">The GLSL code of the shader.</param>
        internal VertexShader(string code)
            : base(ShaderType.VertexShader, new string[] { code })
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VertexShader"/> class with the specified GLSL code units.
        /// </summary>
        /// <param name="codeUnits">The GLSL code units of the shader.</param>
        internal VertexShader(IEnumerable<string> codeUnits)
            : base(ShaderType.VertexShader, codeUnits)
        {
        }
    }
}
