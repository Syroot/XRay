﻿using System;
using System.Runtime.Serialization;

namespace Syroot.XRay.Video.OpenGL.Shaders
{
    /// <summary>
    /// Raised when an exception has occurred in a <see cref="ShaderBase"/>.
    /// </summary>
    [Serializable]
    internal class ShaderException : Exception
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="ShaderException"/> class.
        /// </summary>
        internal ShaderException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShaderException"/> class with a specified error message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        internal ShaderException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShaderException"/> class with a specified error message and a
        /// reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference
        /// (Nothing in Visual Basic) if no inner exception is specified.</param>
        internal ShaderException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShaderException"/> class for the specified
        /// <see cref="ShaderBase"/> with the specified error message.
        /// </summary>
        /// <param name="shader">The <see cref="ShaderBase"/> which caused the exception.</param>
        /// <param name="message">The message to display in the error.</param>
        internal ShaderException(ShaderBase shader, string message)
            : base(message)
        {
            Shader = shader;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShaderException"/> class with serialized data.
        /// </summary>
        /// <param name="info">The <see cref="System.Runtime.Serialization.SerializationInfo"/> that holds the serialized
        /// object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="System.Runtime.Serialization.SerializationInfo"/> that contains
        /// contextual information about the source or destination.</param>
        protected ShaderException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the <see cref="ShaderBase"/> which caused the exception.
        /// </summary>
        internal ShaderBase Shader
        {
            get;
            private set;
        }
    }
}
