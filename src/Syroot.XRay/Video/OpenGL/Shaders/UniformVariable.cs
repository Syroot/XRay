﻿using System.Globalization;
using OpenTK.Graphics.OpenGL;
using Syroot.Maths;
using Syroot.XRay.Maths;
using Matrix3 = Syroot.Maths.Matrix3;
using Matrix4 = Syroot.Maths.Matrix4;
using Vector2 = Syroot.Maths.Vector2;

namespace Syroot.XRay.Video.OpenGL.Shaders
{
    /// <summary>
    /// Represents a uniform variable in a shader program and allows the manipulation of its value.
    /// </summary>
    internal class UniformVariable
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="UniformVariable"/> class for the variable with the specified
        /// name in the given program.
        /// </summary>
        /// <param name="program">The program in which the variable resides.</param>
        /// <param name="name">The name of the variable in the GLSL code.</param>
        internal UniformVariable(ShaderProgram program, string name)
        {
            Program = program;
            Name = name;

            // Get the variable location
            Log.WriteInfo(LogCategory.OpenGL, "GetUniformLocation({0}, {1})", Program.Handle, Name);
            Location = GL.GetUniformLocation(Program.Handle, Name);
            if (Location == -1)
            {
                throw new ShaderProgramException(program, string.Format(CultureInfo.InvariantCulture,
                    "Uniform variable \"{0}\" not found in the specified program.", Name));
            }
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the OpenGL internal location of the variable in the shader program.
        /// </summary>
        internal int Location
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the name of the variable in the GLSL code.
        /// </summary>
        internal string Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the shader program in which the variable resides.
        /// </summary>
        internal ShaderProgram Program
        {
            get;
            private set;
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Sets the value of this uniform variable as 1 integer component. This binds the program if necessary.
        /// </summary>
        /// <param name="x">The integer component.</param>
        internal void SetData(int x)
        {
            Program.Bind();
            Log.WriteInfo(LogCategory.OpenGL, "Uniform1({0}[\"{1}\"], {2})", Location, Name, x);
            GL.Uniform1(Location, x);
        }

        /// <summary>
        /// Sets the value of this uniform variable as 1 float component. This binds the program if necessary.
        /// </summary>
        /// <param name="x">The float component.</param>
        internal void SetData(float x)
        {
            Program.Bind();
            Log.WriteInfo(LogCategory.OpenGL, "Uniform1({0}[\"{1}\"], {2})", Location, Name, x);
            GL.Uniform1(Location, x);
        }

        /// <summary>
        /// Sets the value of this uniform variable as 2 float components. This binds the program if necessary.
        /// </summary>
        /// <param name="x">The first float component.</param>
        /// <param name="y">The second float component.</param>
        internal void SetData(float x, float y)
        {
            Program.Bind();
            Log.WriteInfo(LogCategory.OpenGL, "Uniform1({0}[\"{1}\"], {2}, {3})", Location, Name, x, y);
            GL.Uniform2(Location, x, y);
        }

        /// <summary>
        /// Sets the value of this uniform variable as 3 float components. This binds the program if necessary.
        /// </summary>
        /// <param name="x">The first float component.</param>
        /// <param name="y">The second float component.</param>
        /// <param name="z">The third float component.</param>
        internal void SetData(float x, float y, float z)
        {
            Program.Bind();
            Log.WriteInfo(LogCategory.OpenGL, "Uniform1({0}[\"{1}\"], {2}, {3}, {4})", Location, Name, x, y, z);
            GL.Uniform3(Location, x, y, z);
        }

        /// <summary>
        /// Sets the value of this uniform variable as a color 2-dimensional integer vector. This binds the program if
        /// necessary.
        /// </summary>
        /// <param name="vector2">The 2-dimensional integer vector.</param>
        internal void SetData(Vector2 vector2)
        {
            Program.Bind();
            Log.WriteInfo(LogCategory.OpenGL, "Uniform2({0}[\"{1}\"], {2}", Location, Name, vector2);
            GL.Uniform2(Location, vector2.ToOpenTKVector2());
        }

        /// <summary>
        /// Sets the value of this uniform variable as a color 2-dimensional floating point vector. This binds the
        /// program if necessary.
        /// </summary>
        /// <param name="vector2F">The 2-dimensional floating point vector.</param>
        internal void SetData(Vector2F vector2F)
        {
            Program.Bind();
            Log.WriteInfo(LogCategory.OpenGL, "Uniform2({0}[\"{1}\"], {2}", Location, Name, vector2F);
            GL.Uniform2(Location, vector2F.ToOpenTKVector2());
        }

        /// <summary>
        /// Sets the value of this uniform variable as a color 4-component vector. This binds the program if necessary.
        /// </summary>
        /// <param name="colorF">The color.</param>
        internal void SetData(ColorF colorF)
        {
            Program.Bind();
            Log.WriteInfo(LogCategory.OpenGL, "Uniform4({0}[\"{1}\"], {2}", Location, Name, colorF);
            GL.Uniform4(Location, colorF.ToOpenTKColor4());
        }

        /// <summary>
        /// Sets the value of this uniform variable as a 3x3 matrix. This binds the program if necessary.
        /// Structures are passed by reference to avoid stack structure copying.
        /// </summary>
        /// <param name="matrix3">The 3x3 matrix.</param>
        internal void SetData(ref Matrix3 matrix3)
        {
            SetData(ref matrix3, false);
        }

        /// <summary>
        /// Sets the value of this uniform variable as a 3x3 matrix and optionally transposes it. This binds the program
        /// if necessary.
        /// Structures are passed by reference to avoid stack structure copying.
        /// </summary>
        /// <param name="matrix3">The 3x3 matrix.</param>
        /// <param name="transpose"><c>true</c> to transpose the matrix.</param>
        internal void SetData(ref Matrix3 matrix3, bool transpose)
        {
            Program.Bind();
            Log.WriteInfo(LogCategory.OpenGL, "Uniform1({0}[\"{1}\"], {2}, <matrix3>)", Location, Name, transpose);
            OpenTK.Matrix3 openTKMatrix3 = matrix3.ToOpenTKMatrix3();
            GL.UniformMatrix3(Location, transpose, ref openTKMatrix3);
        }

        /// <summary>
        /// Sets the value of this uniform variable as a 4x4 matrix. This binds the program if necessary.
        /// Structures are passed by reference to avoid stack structure copying.
        /// </summary>
        /// <param name="matrix4">The 4x4 matrix.</param>
        internal void SetData(ref Matrix4 matrix4)
        {
            SetData(ref matrix4, false);
        }

        /// <summary>
        /// Sets the value of this uniform variable as a 4x4 matrix and optionally transposes it. This binds the program
        /// if necessary.
        /// Structures are passed by reference to avoid stack structure copying.
        /// </summary>
        /// <param name="matrix4">The 4x4 matrix.</param>
        /// <param name="transpose"><c>true</c> to transpose the matrix.</param>
        internal void SetData(ref Matrix4 matrix4, bool transpose)
        {
            Program.Bind();
            Log.WriteInfo(LogCategory.OpenGL, "Uniform1({0}[\"{1}\"], {2}, <matrix4>)", Location, Name, transpose);
            OpenTK.Matrix4 openTkMatrix = matrix4.ToOpenTKMatrix4();
            GL.UniformMatrix4(Location, transpose, ref openTkMatrix);
        }
    }
}
