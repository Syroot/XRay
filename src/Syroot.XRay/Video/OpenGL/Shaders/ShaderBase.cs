﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenTK.Graphics.OpenGL;

namespace Syroot.XRay.Video.OpenGL.Shaders
{
    /// <summary>
    /// Represents a GLSL shader which is linked in a shader program.
    /// </summary>
    internal abstract class ShaderBase : IDisposable
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly ShaderType _type;
        private bool _isDisposed;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="ShaderBase"/> class of the specified shader type with the
        /// specified GLSL code.
        /// </summary>
        /// <param name="type">The type of the shader.</param>
        /// <param name="code">The GLSL code of the shader.</param>
        protected ShaderBase(ShaderType type, string code) : this(type, new string[] { code }) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShaderBase"/> class of the specified shader type with the
        /// specified GLSL code units.
        /// </summary>
        /// <param name="type">The type of the shader.</param>
        /// <param name="codeUnits">The GLSL code units of the shader.</param>
        protected ShaderBase(ShaderType type, IEnumerable<string> codeUnits)
        {
            _type = type;
            CodeUnits = new ShaderCodeUnitCollection(this, codeUnits);

            Handle = GL.CreateShader(_type);
            Log.WriteInfo(LogCategory.OpenGL, "CreateShader({0}) = {1}", _type, Handle);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the source code units of this shader.
        /// </summary>
        internal ShaderCodeUnitCollection CodeUnits { get; }

        /// <summary>
        /// Gets the logged information which was created after calling Compile. This is null if the shader has not yet
        /// been compiled.
        /// </summary>
        internal string CompilationLog { get; private set; }

        /// <summary>
        /// Gets the OpenGL internal handle for this shader.
        /// </summary>
        internal int Handle { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this shader has been compiled.
        /// </summary>
        internal bool IsCompiled { get; private set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Compiles this shader program.
        /// </summary>
        /// <exception cref="ShaderException">The shader could not be compiled due to a compilation error.</exception>
        internal void Compile()
        {
            if (IsCompiled)
            {
                throw new ShaderException(this, "Shader has already been compiled.");
            }

            // Set the source code units
            Log.WriteInfo(LogCategory.OpenGL, "ShaderSource({0}, {1}, <codeUnits>, <codeUnitLengths)", Handle,
                CodeUnits.Count);
            GL.ShaderSource(Handle, CodeUnits.Count, CodeUnits.ToArray(), CodeUnits.Lengths.ToArray());

            // Compile the shader
            Log.WriteInfo(LogCategory.OpenGL, "CompileShader({0})", Handle);
            GL.CompileShader(Handle);
            CompilationLog = GL.GetShaderInfoLog(Handle);

            // Check for errors
            GL.GetShader(Handle, ShaderParameter.CompileStatus, out int compileStatus);
            if (compileStatus != 1)
                throw new ShaderException(this, CompilationLog);

            IsCompiled = true;
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release
        /// only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    // Delete this shader (will not actually be removed until using programs detached it)
                    Log.WriteInfo(LogCategory.OpenGL, "DeleteShader({0})", Handle);
                    GL.DeleteShader(Handle);
                }

                Handle = 0;

                _isDisposed = true;
            }
        }
    }
}
