﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Syroot.XRay.Video.OpenGL.Shaders
{
    /// <summary>
    /// Collection managing the source code units of a shader. Cannot be changed when the shader has been compiled.
    /// </summary>
    internal class ShaderCodeUnitCollection : IList<string>
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private ShaderBase   _shader;
        private List<string> _codeUnits;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="ShaderCodeUnitCollection"/> class for the specified shader with
        /// the given source code units.
        /// </summary>
        /// <param name="shader">The shader for which this collection will be created.</param>
        /// <param name="codeUnits">The source code units.</param>
        internal ShaderCodeUnitCollection(ShaderBase shader, IEnumerable<string> codeUnits)
        {
            _shader = shader;
            _codeUnits = new List<string>(codeUnits);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the number of source code units contained in the <see cref="ShaderCodeUnitCollection"/>.
        /// </summary>
        public int Count
        {
            get { return _codeUnits.Count; }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="ShaderCodeUnitCollection"/> is read-only (the shader may be
        /// compiled already).
        /// </summary>
        public bool IsReadOnly
        {
            get { return _shader.IsCompiled; }
        }

        /// <summary>
        /// Gets an array of the lengths of each source code unit.
        /// </summary>
        public Collection<int> Lengths
        {
            get
            {
                Collection<int> lengths = new Collection<int>();
                foreach (string codeUnit in _codeUnits)
                {
                    lengths.Add(codeUnit.Length);
                }
                return lengths;
            }
        }

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the source code unit at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the source code unit to get or set.</param>
        /// <returns>The source code unit at the specified index.</returns>
        /// <exception cref="ArgumentOutOfRangeException">index is not a valid index in the
        /// <see cref="ShaderCodeUnitCollection"/>.</exception>
        /// <exception cref="NotSupportedException">The property is set and the <see cref="ShaderCodeUnitCollection"/>
        /// is read-only (the shader may be compiled already).</exception>
        public string this[int index]
        {
            get
            {
                return _codeUnits[index];
            }
            set
            {
                _codeUnits[index] = value;
            }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Adds a source code unit to the <see cref="ShaderCodeUnitCollection"/>.
        /// </summary>
        /// <param name="item">The source code unit to add to the <see cref="ShaderCodeUnitCollection"/>.</param>
        /// <exception cref="NotSupportedException">The <see cref="ShaderCodeUnitCollection"/> is read-only (the shader
        /// may be compiled already).</exception>
        public void Add(string item)
        {
            _codeUnits.Add(item);
        }

        /// <summary>
        /// Removes all source code units from the <see cref="ShaderCodeUnitCollection"/>.
        /// </summary>
        /// <exception cref="NotSupportedException">The <see cref="ShaderCodeUnitCollection"/> is read-only (the shader
        /// may be compiled already).</exception>
        public void Clear()
        {
            _codeUnits.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="ShaderCodeUnitCollection"/> contains a specific source code unit.
        /// </summary>
        /// <param name="item">The source code unit to locate in the <see cref="ShaderCodeUnitCollection"/>.</param>
        /// <returns><c>true</c> if the source code unit is found in the <see cref="ShaderCodeUnitCollection"/>;
        /// otherwise <c>false</c>.</returns>
        public bool Contains(string item)
        {
            return _codeUnits.Contains(item);
        }

        /// <summary>
        /// Copies the elements of the <see cref="ShaderCodeUnitCollection"/> to an <see cref="System.Array"/>, starting
        /// at a particular <see cref="System.Array"/> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="System.Array"/> that is the destination of the elements
        /// copied from <see cref="ShaderCodeUnitCollection"/>. The <see cref="System.Array"/> must have zero-based
        /// indexing.</param>
        /// <param name="arrayIndex">The zero-based index in array at which copying begins.</param>
        /// <exception cref="ArgumentException">array is null.</exception>
        /// <exception cref="ArgumentNullException">arrayIndex is less than 0.</exception>
        /// <exception cref="ArgumentOutOfRangeException">The number of elements in the source
        /// <see cref="ShaderCodeUnitCollection"/> is greater than the available space from arrayIndex to the end of
        /// the destination array.</exception>
        public void CopyTo(string[] array, int arrayIndex)
        {
            _codeUnits.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>A <see cref="System.Collections.Generic.IEnumerator{T}"/> that can be used to iterate through the
        /// collection.</returns>
        public IEnumerator<string> GetEnumerator()
        {
            return _codeUnits.GetEnumerator();
        }

        /// <summary>
        /// Determines the index of a specific source code unit in the <see cref="ShaderCodeUnitCollection"/>.
        /// </summary>
        /// <param name="item">The source code unit to locate in the <see cref="ShaderCodeUnitCollection"/>.</param>
        /// <returns>The index of source code unit if found in the list; otherwise, -1.</returns>
        public int IndexOf(string item)
        {
            return _codeUnits.IndexOf(item);
        }

        /// <summary>
        /// Inserts a source code unit to the <see cref="ShaderCodeUnitCollection"/> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which item should be inserted.</param>
        /// <param name="item">The source code unit to insert into the <see cref="ShaderCodeUnitCollection"/>.</param>
        /// <exception cref="ArgumentOutOfRangeException">index is not a valid index in the
        /// <see cref="ShaderCodeUnitCollection"/>.</exception>
        /// <exception cref="NotSupportedException">The <see cref="ShaderCodeUnitCollection"/> is read-only (the shader
        /// may be compiled already).</exception>
        public void Insert(int index, string item)
        {
            if (_shader.IsCompiled)
            {
                throw new InvalidOperationException("Cannot change the shader source code units after the shader has "
                    + "been compiled.");
            }
            else
            {
                _codeUnits.Insert(index, item);
            }
        }

        /// <summary>
        /// Removes the first occurrence of a specific source code unit from the <see cref="ShaderCodeUnitCollection"/>.
        /// </summary>
        /// <param name="item">The source code unit to remove from the <see cref="ShaderCodeUnitCollection"/>.</param>
        /// <returns><c>true</c> if the source code unit was successfully removed from the
        /// <see cref="ShaderCodeUnitCollection"/>; otherwise, <c>false</c>. This method also returns <c>false</c> if
        /// item is not found in the original <see cref="ShaderCodeUnitCollection"/>.</returns>
        /// <exception cref="NotSupportedException">The <see cref="ShaderCodeUnitCollection"/> is read-only (the shader
        /// may be compiled already).</exception>
        public bool Remove(string item)
        {
            return _codeUnits.Remove(item);
        }

        /// <summary>
        /// Removes the <see cref="ShaderCodeUnitCollection"/> source code unit at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the source code unit to remove.</param>
        /// <exception cref="ArgumentOutOfRangeException">index is not a valid index in the
        /// <see cref="ShaderCodeUnitCollection"/>.</exception>
        /// <exception cref="NotSupportedException">The <see cref="ShaderCodeUnitCollection"/> is read-only (the shader
        /// may be compiled already).</exception>
        public void RemoveAt(int index)
        {
            _codeUnits.RemoveAt(index);
        }

        /// <summary>
        /// Converts this collection into a string array.
        /// </summary>
        /// <returns>The converted string array.</returns>
        public string[] ToArray()
        {
            return _codeUnits.ToArray();
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>A <see cref="System.Collections.Generic.IEnumerator{T}"/> that can be used to iterate through the
        /// collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _codeUnits.GetEnumerator();
        }
    }
}
