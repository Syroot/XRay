﻿using System;
using System.Collections.ObjectModel;
using OpenTK.Graphics.OpenGL;

namespace Syroot.XRay.Video.OpenGL.Shaders
{
    /// <summary>
    /// Represents a shader program which links single shader instances together.
    /// </summary>
    internal class ShaderProgram : OpenGLObject<ShaderProgram>, IDisposable
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private bool _isDisposed;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="ShaderProgram"/> class.
        /// </summary>
        internal ShaderProgram()
            : this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShaderProgram"/> class with the specified shaders.
        /// </summary>
        /// <param name="shaders">The shaders for use in this program.</param>
        internal ShaderProgram(params ShaderBase[] shaders)
        {
            Handle = GL.CreateProgram();
            Log.WriteInfo(LogCategory.OpenGL, "CreateProgram() = {0}", Handle);

            Shaders = new Collection<ShaderBase>(shaders);
            Uniforms = new UniformCollection(this);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets a value indicating whether this program has been linked already.
        /// </summary>
        internal bool IsLinked
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the logged information which was created after calling <see cref="Link"/>. This is null if the program
        /// has not yet been linked.
        /// </summary>
        internal string LinkageLog
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the list of attached shaders which will be linked.
        /// </summary>
        internal Collection<ShaderBase> Shaders
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the list of <see cref="UniformVariable"/> instances retrievable in this program.
        /// </summary>
        internal UniformCollection Uniforms
        {
            get;
            private set;
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Attaches and links all shaders together. Shaders will be compiled first if necessary. After successful
        /// linkage, the shaders will be detached. Disposing them is up to you.
        /// </summary>
        internal void Link()
        {
            if (IsLinked)
            {
                throw new ShaderProgramException(this, "Program has already been linked.");
            }

            // Attach the shaders to the program
            foreach (ShaderBase shader in Shaders)
            {
                // Compile if necessary
                if (!shader.IsCompiled)
                {
                    shader.Compile();
                }
                Log.WriteInfo(LogCategory.OpenGL, "AttachShader({0}, {1})", Handle, shader.Handle);
                GL.AttachShader(Handle, shader.Handle);
            }

            // Link shaders together
            Log.WriteInfo(LogCategory.OpenGL, "LinkProgram({0})", Handle);
            GL.LinkProgram(Handle);
            LinkageLog = GL.GetProgramInfoLog(Handle);

            // Check for errors
            GL.GetProgram(Handle, GetProgramParameterName.LinkStatus, out int linkStatus);
            if (linkStatus != 1)
            {
                throw new ShaderProgramException(this, LinkageLog);
            }

            // We do not require the shaders to be attached anymore
            foreach (ShaderBase shader in Shaders)
            {
                Log.WriteInfo(LogCategory.OpenGL, "DetachShader({0}, {1})", Handle, shader.Handle);
                GL.DetachShader(Handle, shader.Handle);
            }

            IsLinked = true;
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Makes this shader program the currently bound one.
        /// </summary>
        protected override void DoBind()
        {
            Log.WriteInfo(LogCategory.OpenGL, "UseProgram({0})", Handle);
            GL.UseProgram(Handle);
        }

        /// <summary>
        /// Unbinds any shader program.
        /// </summary>
        protected override void DoUnbind()
        {
            Log.WriteInfo(LogCategory.OpenGL, "UseProgram(0)");
            GL.UseProgram(0);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release
        /// only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    // Detach all shaders (deletion is handled by their Dispose)
                    foreach (ShaderBase shader in Shaders)
                    {
                        Log.WriteInfo(LogCategory.OpenGL, "DetachShader({0}, {1})", Handle, shader.Handle);
                        GL.DetachShader(Handle, shader.Handle);
                        shader.Dispose();
                    }

                    // Delete this program
                    Log.WriteInfo(LogCategory.OpenGL, "DeleteProgram({0})", Handle);
                    GL.DeleteProgram(Handle);
                }

                Handle = 0;

                _isDisposed = true;
            }

            base.Dispose(disposing);
        }
    }
}
