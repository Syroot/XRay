﻿using System.Collections.Generic;

namespace Syroot.XRay.Video.OpenGL.Shaders
{
    /// <summary>
    /// Represents a dictionary remembering <see cref="UniformVariable"/> instances of a <see cref="ShaderProgram"/>.
    /// </summary>
    internal class UniformCollection
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Dictionary<string, UniformVariable> _cache;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="UniformCollection"/> class for the given
        /// <see cref="ShaderProgram"/>.
        /// </summary>
        /// <param name="program">The <see cref="ShaderProgram"/> for which this instance will be created.</param>
        internal UniformCollection(ShaderProgram program)
        {
            Program = program;
            _cache = new Dictionary<string, UniformVariable>();
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the program from which uniform variables are referenced.
        /// </summary>
        internal ShaderProgram Program
        {
            get;
            private set;
        }

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the <see cref="UniformVariable"/> with the given name. If the <see cref="UniformVariable"/> does not
        /// exist in the program, a <see cref="ShaderProgramException"/> is thrown.
        /// </summary>
        /// <param name="name">The name of the <see cref="UniformVariable"/> to retrieve.</param>
        /// <returns>The retrieved <see cref="UniformVariable"/>.</returns>
        internal UniformVariable this[string name]
        {
            get
            {
                if (!_cache.TryGetValue(name, out UniformVariable uniform))
                {
                    uniform = new UniformVariable(Program, name);
                    _cache.Add(name, uniform);
                }
                return uniform;
            }
        }
    }
}
