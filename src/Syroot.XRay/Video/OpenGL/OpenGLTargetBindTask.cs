﻿using System;

namespace Syroot.XRay.Video.OpenGL
{
    /// <summary>
    /// Represents a temporary binding of an <see cref="OpenGLTargetObject"/> which is unbound after the task has been
    /// disposed.
    /// </summary>
    /// <typeparam name="T">The type of object being an OpenGL object.</typeparam>
    /// <typeparam name="TTarget">The type of the target type making this binding unique.</typeparam>
    internal class OpenGLTargetBindTask<T, TTarget> : IDisposable
        where T : OpenGLTargetObject<T, TTarget>
        where TTarget : struct
    {
        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGLTargetBindTask{T, TTarget}"/> class to temporarily bind
        /// the given <see cref="OpenGLTargetObject"/>. The <see cref="OpenGLTargetObject"/> is unbound after the task
        /// is disposed.
        /// </summary>
        /// <param name="openGLTargetObject">A <see cref="OpenGLTargetObject"/> to temporarily bind.</param>
        /// <param name="target">The target to bind the object to.</param>
        internal OpenGLTargetBindTask(OpenGLTargetObject<T, TTarget> openGLTargetObject, TTarget target)
        {
            Target = target;
            OpenGLTargetObject = openGLTargetObject;
            OpenGLTargetObject.Bind(Target);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the <see cref="OpenGLTargetObject"/> which is temporarily bound.
        /// </summary>
        internal OpenGLTargetObject<T, TTarget> OpenGLTargetObject
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the target this object is bound to.
        /// </summary>
        internal TTarget Target
        {
            get;
            private set;
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Unbinds the <see cref="OpenGLTargetObject"/>.
        /// </summary>
        public void Dispose()
        {
            OpenGLTargetObject.UnbindAny(Target);
        }
    }
}
