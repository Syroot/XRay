﻿using System;
using System.Collections.Generic;

namespace Syroot.XRay.Video.OpenGL
{
    /// <summary>
    /// Represents the base for any OpenGL-related resource.
    /// </summary>
    /// <typeparam name="T">The type of object being an OpenGL object.</typeparam>
    internal abstract class OpenGLObject<T> : IDisposable
        where T : OpenGLObject<T>
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static List<T> _instances = new List<T>();

        private bool _isDisposed;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGLObject{T}"/> class.
        /// </summary>
        internal OpenGLObject()
        {
            // Add to the list of instances created.
            _instances.Add((T)this);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the currently bound instance.
        /// </summary>
        internal static T BoundInstance { get; private set; }

        /// <summary>
        /// Gets or sets the handle of this OpenGL resource.
        /// </summary>
        internal int Handle { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Retrieves an existing instance with the given handle or <c>null</c> if no instance has such handle.
        /// </summary>
        /// <param name="handle">The handle of the instance to retrieve.</param>
        /// <returns>The instance with the given handle or <c>null</c> if no instance has such handle.</returns>
        internal static T ByHandle(int handle)
        {
            foreach (T instance in _instances)
            {
                if (instance.Handle == handle)
                    return instance;
            }
            return null;
        }

        /// <summary>
        /// Makes sure that the object is the currently bound one and binds it if it is not.
        /// </summary>
        internal void Bind()
        {
            if (BoundInstance == null || Handle != BoundInstance.Handle)
            {
                // Binding of the current instance is required.
                DoBind();
                BoundInstance = (T)this;
            }
        }

        /// <summary>
        /// Creates an <see cref="OpenGLBindTask{T}"/> for this instance. As soon as the returned
        /// <see cref="OpenGLBindTask{T}"/> is disposed, all instances of this type are unbound again. Do not use this
        /// if you can simply bind another object later. This task should be used if belated unbinding can change the
        /// state of the bound object, as it does for <see cref="Buffers.VertexArrayObject"/> instances, for example.
        /// </summary>
        /// <returns>An <see cref="OpenGLBindTask{T}"/> to be disposed to undo the binding.</returns>
        internal OpenGLBindTask<T> TemporaryBind()
        {
            return new OpenGLBindTask<T>(this);
        }

        /// <summary>
        /// Unbinds an active instance of the type of this object if one is bound.
        /// </summary>
        internal void UnbindAny()
        {
            if (BoundInstance != null)
            {
                BoundInstance.DoUnbind();
                BoundInstance = null;
            }
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Called when this <see cref="OpenGLObject{T}"/> requires binding.
        /// </summary>
        protected abstract void DoBind();

        /// <summary>
        /// Called when this <see cref="OpenGLObject{T}"/> requires unbinding.
        /// </summary>
        protected abstract void DoUnbind();

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release
        /// only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                    _instances.Remove((T)this);
                
                _isDisposed = true;
            }
        }
    }
}
