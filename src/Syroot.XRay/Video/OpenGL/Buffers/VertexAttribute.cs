﻿using System.Globalization;
using OpenTK.Graphics.OpenGL;
using Syroot.XRay.Video.OpenGL.Shaders;

namespace Syroot.XRay.Video.OpenGL.Buffers
{
    /// <summary>
    /// Represents a format of data in a data buffer.
    /// </summary>
    internal class VertexAttribute
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private int                     _location;
        private int                     _count;
        private VertexAttribPointerType _dataType;
        private bool                    _normalized;
        private int                     _stride;
        private int                     _offset;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="VertexAttribute"/> class with the specified data format,
        /// un-normalized, with a stride of 0 components and an offset of 0.
        /// </summary>
        /// <param name="program">The program from which the attribute location will be retrieved.</param>
        /// <param name="attributeName">The name of the attribute which location will be retrieved.</param>
        /// <param name="dataType">The type of each component.</param>
        /// <param name="count">The number of components each vertex attribute.</param>
        internal VertexAttribute(ShaderProgram program, string attributeName, VertexAttribPointerType dataType,
            int count)
            : this(GetAttributeLocation(program, attributeName), dataType, count, false, 0, 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VertexAttribute"/> class with the specified data format,
        /// un-normalized and an offset of 0.
        /// </summary>
        /// <param name="program">The program from which the attribute location will be retrieved.</param>
        /// <param name="attributeName">The name of the attribute which location will be retrieved.</param>
        /// <param name="dataType">The type of each component.</param>
        /// <param name="count">The number of components each vertex attribute.</param>
        /// <param name="stride">The offset between consecutive vertex attributes in bytes.</param>
        internal VertexAttribute(ShaderProgram program, string attributeName, VertexAttribPointerType dataType,
            int count,
            int stride)
            : this(GetAttributeLocation(program, attributeName), dataType, count, false, stride, 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VertexAttribute"/> class with the specified data format,
        /// un-normalized.
        /// </summary>
        /// <param name="program">The program from which the attribute location will be retrieved.</param>
        /// <param name="attributeName">The name of the attribute which location will be retrieved.</param>
        /// <param name="dataType">The type of each component.</param>
        /// <param name="count">The number of components each vertex attribute.</param>
        /// <param name="stride">The offset between consecutive vertex attributes in bytes.</param>
        /// <param name="offset">The offset of the first vertex attribute in the array in bytes.</param>
        internal VertexAttribute(ShaderProgram program, string attributeName, VertexAttribPointerType dataType,
            int count, int stride, int offset)
            : this(GetAttributeLocation(program, attributeName), dataType, count, false, stride, offset)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VertexAttribute"/> class with the specified data format, with
        /// an offset of 0.
        /// </summary>
        /// <param name="program">The program from which the attribute location will be retrieved.</param>
        /// <param name="attributeName">The name of the attribute which location will be retrieved.</param>
        /// <param name="dataType">The type of each component.</param>
        /// <param name="count">The number of components each vertex attribute.</param>
        /// <param name="normalized">A value indicating whether the values will be normalized.</param>
        /// <param name="stride">The offset between consecutive vertex attributes in bytes.</param>
        internal VertexAttribute(ShaderProgram program, string attributeName, VertexAttribPointerType dataType,
            int count, bool normalized, int stride)
            : this(GetAttributeLocation(program, attributeName), dataType, count, normalized, stride, 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VertexAttribute"/> class with the specified data format.
        /// </summary>
        /// <param name="program">The program from which the attribute location will be retrieved.</param>
        /// <param name="attributeName">The name of the attribute which location will be retrieved.</param>
        /// <param name="dataType">The type of each component.</param>
        /// <param name="count">The number of components each vertex attribute.</param>
        /// <param name="normalized">A value indicating whether the values will be normalized.</param>
        /// <param name="stride">The offset between consecutive vertex attributes in bytes.</param>
        /// <param name="offset">The offset of the first vertex attribute in the array in bytes.</param>
        internal VertexAttribute(ShaderProgram program, string attributeName, VertexAttribPointerType dataType,
            int count, bool normalized, int stride, int offset)
            : this(GetAttributeLocation(program, attributeName), dataType, count, normalized, stride, offset)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VertexAttribute"/> class with the specified data format,
        /// un-normalized, with a stride of 0 components and an offset of 0.
        /// </summary>
        /// <param name="location">The index of this vertex attribute.</param>
        /// <param name="dataType">The type of each component.</param>
        /// <param name="count">The number of components each vertex attribute.</param>
        internal VertexAttribute(int location, VertexAttribPointerType dataType, int count)
            : this(location, dataType, count, false, 0, 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VertexAttribute"/> class with the specified data format,
        /// un-normalized and an offset of 0.
        /// </summary>
        /// <param name="location">The index of this vertex attribute.</param>
        /// <param name="dataType">The type of each component.</param>
        /// <param name="count">The number of components each vertex attribute.</param>
        /// <param name="stride">The offset between consecutive vertex attributes in bytes.</param>
        internal VertexAttribute(int location, VertexAttribPointerType dataType, int count, int stride)
            : this(location, dataType, count, false, stride, 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VertexAttribute"/> class with the specified data format,
        /// un-normalized.
        /// </summary>
        /// <param name="location">The index of this vertex attribute.</param>
        /// <param name="dataType">The type of each component.</param>
        /// <param name="count">The number of components each vertex attribute.</param>
        /// <param name="stride">The offset between consecutive vertex attributes in bytes.</param>
        /// <param name="offset">The offset of the first vertex attribute in the array in bytes.</param>
        internal VertexAttribute(int location, VertexAttribPointerType dataType, int count, int stride, int offset)
            : this(location, dataType, count, false, stride, offset)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VertexAttribute"/> class with the specified data format, with
        /// an offset of 0.
        /// </summary>
        /// <param name="location">The index of this vertex attribute.</param>
        /// <param name="dataType">The type of each component.</param>
        /// <param name="count">The number of components each vertex attribute.</param>
        /// <param name="normalized">A value indicating whether the values will be normalized.</param>
        /// <param name="stride">The offset between consecutive vertex attributes in bytes.</param>
        internal VertexAttribute(int location, VertexAttribPointerType dataType, int count, bool normalized, int stride)
            : this(location, dataType, count, normalized, stride, 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VertexAttribute"/> class with the specified data format.
        /// </summary>
        /// <param name="location">The index of this vertex attribute.</param>
        /// <param name="type">The type of each component.</param>
        /// <param name="count">The number of components each vertex attribute.</param>
        /// <param name="normalized">A value indicating whether the values will be normalized.</param>
        /// <param name="stride">The offset between consecutive vertex attributes in bytes.</param>
        /// <param name="offset">The offset of the first vertex attribute in the array in bytes.</param>
        internal VertexAttribute(int location, VertexAttribPointerType type, int count, bool normalized, int stride,
            int offset)
        {
            _location = location;
            _count = count;
            _dataType = type;
            _normalized = normalized;
            _stride = stride;
            _offset = offset;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the index of this vertex attribute.
        /// </summary>
        internal int Location
        {
            get
            {
                return _location;
            }
            set
            {
                if (_location != value)
                {
                    _location = value;
                    SetFormat();
                }
            }
        }

        /// <summary>
        /// Gets or sets the number of components each vertex attribute.
        /// </summary>
        internal int Count
        {
            get
            {
                return _count;
            }
            set
            {
                if (_count != value)
                {
                    _count = value;
                    SetFormat();
                }
            }
        }

        /// <summary>
        /// Gets or sets the type of each component.
        /// </summary>
        internal VertexAttribPointerType DataType
        {
            get
            {
                return _dataType;
            }
            set
            {
                if (_dataType != value)
                {
                    _dataType = value;
                    SetFormat();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the values will be normalized.
        /// </summary>
        internal bool Normalized
        {
            get
            {
                return _normalized;
            }
            set
            {
                if (_normalized != value)
                {
                    _normalized = value;
                    SetFormat();
                }
            }
        }

        /// <summary>
        /// Gets or sets the offset between consecutive vertex attributes.
        /// </summary>
        internal int Stride
        {
            get
            {
                return _stride;
            }
            set
            {
                if (_stride != value)
                {
                    _stride = value;
                    SetFormat();
                }
            }
        }

        /// <summary>
        /// Gets or sets the offset of the first vertex attribute in the array.
        /// </summary>
        internal int Offset
        {
            get
            {
                return _offset;
            }
            set
            {
                if (_offset != value)
                {
                    _offset = value;
                    SetFormat();
                }
            }
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Enables this vertex attribute for the index and the format is has been set up for.
        /// </summary>
        internal void Enable()
        {
            SetFormat();
            // Enable this format input.
            Log.WriteInfo(LogCategory.OpenGL, "EnableVertexAttribArray({0})", Location);
            GL.EnableVertexAttribArray(Location);
        }

        /// <summary>
        /// Disables this vertex attribute.
        /// </summary>
        internal void Disable()
        {
            // Disable this format input
            GL.DisableVertexAttribArray(Location);
        }
        
        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static int GetAttributeLocation(ShaderProgram program, string attributeName)
        {
            // Get the index of the attribute
            Log.WriteInfo(LogCategory.OpenGL, "GetAttribLocation({0}, {1})", program.Handle, attributeName);
            int attributeLocation = GL.GetAttribLocation(program.Handle, attributeName);

            if (attributeLocation == -1)
            {
                throw new ShaderProgramException(program, string.Format(CultureInfo.InvariantCulture,
                    "Attribute \"{0}\" not found in program.", attributeName));
            }
            return attributeLocation;
        }

        private void SetFormat()
        {
            // Set the format of the input
            Log.WriteInfo(LogCategory.OpenGL, "VertexAttribPointer({0}, {1}, {2}, {3}, {4}, {5})", Location, Count,
                DataType, Normalized, Stride, Offset);
            GL.VertexAttribPointer(Location, Count, DataType, Normalized, Stride, Offset);
        }
    }
}
