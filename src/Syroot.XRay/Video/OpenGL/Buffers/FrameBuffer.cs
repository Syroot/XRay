﻿using OpenTK.Graphics.OpenGL;
using Syroot.XRay.Video.OpenGL.Textures;

namespace Syroot.XRay.Video.OpenGL.Buffers
{
    /// <summary>
    /// Represents a 2-dimensional buffer which can be rendered to.
    /// </summary>
    internal class FrameBuffer : OpenGLObject<FrameBuffer>
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private bool _isDisposed;
        private Texture2D _attachedTexture;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="FrameBuffer"/> class.
        /// </summary>
        internal FrameBuffer()
        {
            Handle = GL.GenFramebuffer();
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the <see cref="Texture2D"/> on which is rendered. Setting binds the <see cref="FrameBuffer"/>
        /// if necessary.
        /// </summary>
        internal Texture2D AttachedTexture
        {
            get => _attachedTexture;
            set
            {
                if (_attachedTexture != value)
                {
                    Bind();
                    GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0,
                        TextureTarget.Texture2D, value.Handle, 0);
                    _attachedTexture = value;
                }
            }
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Called when this <see cref="FrameBuffer"/> requires binding.
        /// </summary>
        protected override void DoBind()
        {
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, Handle);
        }

        /// <summary>
        /// Called when this <see cref="FrameBuffer"/> requires unbinding.
        /// </summary>
        protected override void DoUnbind()
        {
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release
        /// only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    Log.WriteInfo(LogCategory.OpenGL, "DeleteFramebuffer({0})", Handle);
                    GL.DeleteFramebuffer(Handle);
                }

                _isDisposed = true;
            }

            base.Dispose(disposing);
        }
    }
}
