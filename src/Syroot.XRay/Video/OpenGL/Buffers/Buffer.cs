﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Syroot.XRay.Video.OpenGL.Buffers
{
    /// <summary>
    /// Represents the base functionality of any OpenGL data buffer.
    /// </summary>
    /// <typeparam name="T">The buffer class implementing a specific <see cref="BufferTarget"/>.</typeparam>
    internal class Buffer<T> : OpenGLTargetObject<Buffer<T>, BufferTarget>, IDisposable
        where T : struct
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly BufferTarget _target;
        private int _dataLength;

        private bool _isDisposed;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="Buffer{T}"/> class and buffer usage target.
        /// </summary>
        /// <param name="target">The <see cref="BufferTarget"/> to which this buffer will be bound.</param>
        /// <param name="usageHint">The kind of usage this buffer will be used for.</param>
        internal Buffer(BufferTarget target, BufferUsageHint usageHint)
        {
            _target = target;
            UsageHint = usageHint;

            Handle = GL.GenBuffer();
            Log.WriteInfo(LogCategory.OpenGL, "GenBuffer() = {0}", Handle);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the usage type set for this buffer.
        /// </summary>
        internal BufferUsageHint UsageHint { get; }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Makes sure that the object is the currently bound one of the target with which it was constructed and binds
        /// it if it is not.
        /// </summary>
        internal void Bind()
        {
            Bind(_target);
        }

        /// <summary>
        /// Unbinds an active instance of the type and target of this object if one is bound.
        /// </summary>
        internal void UnbindAny()
        {
            UnbindAny(_target);
        }

        /// <summary>
        /// Sets the data referenced by this buffer and makes it visible to OpenGL. Call this whenever you make changes
        /// in your code to the referenced array. This binds the buffer if necessary.
        /// </summary>
        /// <param name="data">The data array which will be uploaded to OpenGL.</param>
        /// <typeparam name="TData">The type of the data in the array to be uploaded. The data type must have an
        /// explicit layout to be blittable.</typeparam>
        internal void SetData<TData>(TData[] data)
            where TData : struct
        {
            Runtime.ThrowIfNull(data, "data");

            // Throw an exception if the data cannot be blitted.
            Debug.Assert(BlittableValueType<TData>.Check(), "Type of BufferData must be blittable.");

            // Throw a warning if the data is getting updated and the buffer has bad static usage.
            Runtime.AssertDebug(_dataLength == 0 || UsageHint != BufferUsageHint.StaticCopy && UsageHint
                != BufferUsageHint.StaticDraw && UsageHint != BufferUsageHint.StaticRead, () =>
                Log.WriteWarn(LogCategory.Video, "Updating vertices of array buffer with suboptimal '{0}' usage hint.",
                    UsageHint));

            // Activate the buffer.
            Bind();

            // Update the data.
            int sizeOfTData = Marshal.SizeOf(typeof(TData));
            if (_dataLength == 0 || _dataLength != data.Length)
            {
                // First time uploading data or uploading data with new length, allocate new memory.
                _dataLength = data.Length;
                Log.WriteInfo(LogCategory.OpenGL, "BufferData({0}, {1}, <data>, {2})", _target,
                    _dataLength * sizeOfTData, UsageHint);
                GL.BufferData(_target, new IntPtr(_dataLength * sizeOfTData), data, UsageHint);
            }
            else
            {
                // Data with the same length is uploaded again, no need to allocate new memory.
                Log.WriteInfo(LogCategory.OpenGL, "BufferSubData({0}, 0, {1}, <data>)", _target,
                    _dataLength * sizeOfTData, UsageHint);
                GL.BufferSubData(_target, IntPtr.Zero, new IntPtr(_dataLength * sizeOfTData), data);
            }
        }

        /// <summary>
        /// Sets a part of the data referenced by this buffer and makes it visible to OpenGL. Call this whenever you
        /// make changes in your code to the referenced array. This binds the buffer if necessary.
        /// </summary>
        /// <param name="data">The data array which will be uploaded to OpenGL. Make sure that the data does not exceed
        /// the size of the buffer and mind the offset.</param>
        /// <param name="offset">The offset at which the data will be updated, in bytes.</param>
        /// <typeparam name="TData">The type of the data in the array to be uploaded.</typeparam>
        internal void SetData<TData>(TData[] data, int offset)
            where TData : struct
        {
            Runtime.ThrowIfNull(data, "data");

            // Throw a warning if the data is getting updated and the buffer has bad static usage.
            if (UsageHint == BufferUsageHint.StaticCopy || UsageHint == BufferUsageHint.StaticDraw
                || UsageHint == BufferUsageHint.StaticRead)
            {
                Log.WriteWarn(LogCategory.Video, "Updating vertices of array buffer with suboptimal '{0}' usage hint.",
                    UsageHint);
            }

            // Activate the buffer.
            Bind();

            // Upload the part of the changed data.
            int sizeOfTData = Marshal.SizeOf(typeof(TData));
            Log.WriteInfo(LogCategory.OpenGL, "BufferSubData({0}, {1}, {2}, <data>)", _target, offset,
                data.Length * sizeOfTData);
            GL.BufferSubData(_target, new IntPtr(offset), new IntPtr(data.Length * sizeOfTData), data);
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Called when this <see cref="OpenGLTargetObject{T, TTarget}"/> requires binding.
        /// </summary>
        /// <param name="target">The target to bind the object to.</param>
        protected override void DoBind(BufferTarget target)
        {
            Log.WriteInfo(LogCategory.OpenGL, "BindBuffer({0}, {1})", target, Handle);
            GL.BindBuffer(target, Handle);
        }

        /// <summary>
        /// Called when this <see cref="OpenGLTargetObject{T, TTarget}"/> requires unbinding.
        /// </summary>
        /// <param name="target">The target to unbind any object from.</param>
        protected override void DoUnbind(BufferTarget target)
        {
            Log.WriteInfo(LogCategory.OpenGL, "BindBuffer({0}, 0)", target);
            GL.BindBuffer(target, 0);
        }

        protected override void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    Log.WriteInfo(LogCategory.OpenGL, "DeleteBuffer({0})", Handle);
                    GL.DeleteBuffer(Handle);
                }

                Handle = 0;

                _isDisposed = true;
            }

            base.Dispose(disposing);
        }
    }
}
