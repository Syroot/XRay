﻿using System;
using OpenTK.Graphics.OpenGL;

namespace Syroot.XRay.Video.OpenGL.Buffers
{
    /// <summary>
    /// Collects information about the layout of data in vertex data buffers.
    /// </summary>
    internal class VertexArrayObject : OpenGLObject<VertexArrayObject>, IDisposable
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private bool _isDisposed;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="VertexArrayObject"/> class.
        /// </summary>
        internal VertexArrayObject()
        {
            Handle = GL.GenVertexArray();
            Log.WriteInfo(LogCategory.OpenGL, "GenVertexArray() = {0}", Handle);
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Makes sure that the object is the currently bound one and binds it if it is not.
        /// </summary>
        protected override void DoBind()
        {
            Log.WriteInfo(LogCategory.OpenGL, "BindVertexArray({0})", Handle);
            GL.BindVertexArray(Handle);
        }

        /// <summary>
        /// Unbinds an active instance of the type of this object if one is bound.
        /// </summary>
        protected override void DoUnbind()
        {
            Log.WriteInfo(LogCategory.OpenGL, "BindVertexArray(0)");
            GL.BindVertexArray(0);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release
        /// only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    Log.WriteInfo(LogCategory.OpenGL, "DeleteVertexArray({0})", Handle);
                    GL.DeleteVertexArray(Handle);
                }

                _isDisposed = true;
            }

            base.Dispose(disposing);
        }
    }
}
