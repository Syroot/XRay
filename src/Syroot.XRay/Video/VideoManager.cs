﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using OpenTK.Graphics.OpenGL;
using Syroot.Maths;
using Syroot.XRay.Maths;
using Syroot.XRay.Video.Sprite;

namespace Syroot.XRay.Video
{
    /// <summary>
    /// Represents the graphical core of a <see cref="Game"/>, providing methods to manipulate what is seen on screen.
    /// </summary>
    public static class VideoManager
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------
        
        private static List<Image> _imageObjects;
        private static ColorF      _clearColor;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the color which is used when emptying the graphics in the output window by calling the
        /// <see cref="Clear()"/> method.
        /// </summary>
        /// <value>The color to clear the output window with.</value>
        public static ColorF ClearColor
        {
            get
            {
                return _clearColor;
            }
            set
            {
                if (_clearColor != value)
                {
                    _clearColor = value;
                    GL.ClearColor(_clearColor.ToOpenTKColor4());
                }
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="TextureFiltering"/> quality used to draw images and text.
        /// </summary>
        public static TextureFiltering ImageFiltering
        {
            get
            {
                return SpriteBatch.Filtering;
            }
            set
            {
                SpriteBatch.Filtering = value;
            }
        }

        /// <summary>
        /// Gets the list of created <see cref="Image"/> instances.
        /// </summary>
        public static ReadOnlyCollection<Image> ImageObjects
        {
            get { return _imageObjects.AsReadOnly(); }
        }
        
        /// <summary>
        /// Gets the <see cref="SpriteBatch"/> instance used to handle draw calls optimized.
        /// </summary>
        internal static SpriteBatch SpriteBatch
        {
            get;
            private set;
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Clears the screen or currently active frame buffer with the <see cref="ClearColor"/>.
        /// </summary>
        public static void Clear()
        {
            Log.WriteInfo(LogCategory.OpenGL, "Clear({0})", ClearBufferMask.ColorBufferBit
                | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit
                | ClearBufferMask.StencilBufferBit);

            // Remove any queued images which would have been drawn at Present().
            SpriteBatch.Clear();
        }

        /// <summary>
        /// Presents the rendered graphics to the screen or currently active frame buffer.
        /// </summary>
        public static void Present()
        {
            // Draw the queued images.
            SpriteBatch.Present();

            Game.Window.SwapBuffers();
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Called when the <see cref="Game"/> is run and the window initialized.
        /// </summary>
        internal static void Initialize()
        {
            // Attach to the game.
            Game.Disposing += Game_Disposing;

            // Initialize book-keeping lists.
            _imageObjects = new List<Image>();

            // Enable alpha transparency composition.
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);

            // Initialize dependent classes.
            SpriteBatch = new SpriteBatch();
        }

        /// <summary>
        /// Registers the given <see cref="Image"/> and adds it to the list of tracked instances.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> to add.</param>
        internal static void Register(Image image)
        {
            if (_imageObjects.Contains(image))
            {
                throw new InvalidOperationException("Image already registered.");
            }
            else
            {
                _imageObjects.Add(image);
            }
        }
        
        /// <summary>
        /// Deregisters the given <see cref="Image"/> and removes it from the list of tracked instances.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> to add.</param>
        internal static void Unregister(Image image)
        {
            Debug.Assert(_imageObjects.Contains(image), "Image to remove was not registered.");
            _imageObjects.Remove(image);
        }
        
        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

        private static void Game_Disposing(object sender, EventArgs e)
        {
            // Dispose all images.
            for (int i = 0; i < _imageObjects.Count; i++)
            {
                _imageObjects[i].Dispose();
            }
        }
    }
}