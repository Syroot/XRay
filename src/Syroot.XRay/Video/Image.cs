﻿using System;
using System.IO;
using Syroot.Maths;
using Syroot.XRay.Video.Decoders;
using Syroot.XRay.Video.OpenGL.Textures;

namespace Syroot.XRay.Video
{
    /// <summary>
    /// Represents a bitmap displayed on the screen at pixel-based locations and sizes.
    /// </summary>
    public class Image : IDisposable
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private bool _isDisposed;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="Image"/> class.
        /// </summary>
        internal Image()
        {
            VideoManager.Register(this);
            
            // Create the texture.
            Texture = new Texture2D();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Image"/> class with the given size and transparent pixels.
        /// </summary>
        /// <param name="size">The size of the image.</param>
        public Image(Vector2 size)
            : this()
        {
            // Set up the texture.
            Texture.SetSize(size);
            Size = size;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Image"/> class from the given <see cref="Stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to create the image from.</param>
        public Image(Stream stream)
            : this()
        {
            // Set up the texture.
            using (ImageDecoderBase decoder = ImageDecoderBase.Instantiate(stream))
            {
                Texture.SetData(decoder.ReadPixels());
                Size = decoder.Size;
            }
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the original width and height of the loaded <see cref="Texture"/> representing the image.
        /// </summary>
        public Vector2 Size
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the <see cref="Texture2D"/> holding the texture data.
        /// </summary>
        internal Texture2D Texture
        {
            get;
            private set;
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Draws the image into the specified rectangle with the given angle, position origin, rotation origin, tint
        /// and crop.
        /// </summary>
        /// <param name="rectangle">The rectangle to draw the image in, in pixel.</param>
        /// <param name="rotation">The rotation angle in radians.</param>
        /// <param name="origin">The origin in the image to which the position is relative, normalized.</param>
        /// <param name="rotationOrigin">The origin in the image around which will be rotated, normalized.</param>
        /// <param name="tint">The colorization of the image, supporting the alpha component.</param>
        /// <param name="crop">The crop determining the part of the texture to be visible, in pixel.</param>
        public void Draw(Rectangle? rectangle = null, float rotation = 0f, Vector2F? origin = null,
            Vector2F? rotationOrigin = null, ColorF? tint = null, Rectangle? crop = null)
        {
            // If no rectangle specified, try taking the crop size, otherwise the image size, at the top left position.
            Rectangle finalRectangle = rectangle ?? new Rectangle(Vector2.Zero, crop.HasValue ? crop.Value.Size : Size);

            VideoManager.SpriteBatch.Draw(Texture, Size, origin ?? Vector2.Zero, finalRectangle,
                rotationOrigin ?? Vector2F.Half, rotation, tint ?? ColorF.White,
                crop ?? new Rectangle(Vector2.Zero, Size));
        }

        /// <summary>
        /// Draws the image into the specified rectangle with the given angle, position origin, rotation origin, tint
        /// and crop.
        /// </summary>
        /// <param name="rectangle">The rectangle to draw the image in, in pixel.</param>
        /// <param name="rotation">The rotation angle in radians.</param>
        /// <param name="origin">The origin in the image to which the position is relative, normalized.</param>
        /// <param name="rotationOrigin">The origin in the image around which will be rotated, normalized.</param>
        /// <param name="tint">The colorization of the image, supporting the alpha component.</param>
        /// <param name="crop">The crop determining the part of the texture to be visible, in pixel.</param>
        public void Draw(RectangleF? rectangle = null, float rotation = 0f, Vector2F? origin = null,
            Vector2F? rotationOrigin = null, ColorF? tint = null, RectangleF? crop = null)
        {
            // If no rectangle specified, try taking the crop size, otherwise the image size, at the top left position.
            RectangleF finalRectangle = rectangle ?? new RectangleF(Vector2.Zero,
                crop.HasValue ? crop.Value.Size : Size);

            VideoManager.SpriteBatch.Draw(Texture, Size, origin ?? Vector2F.Zero, finalRectangle,
                rotationOrigin ?? Vector2F.Half, rotation, tint ?? ColorF.White,
                crop ?? new RectangleF(Vector2F.Zero, Size));
        }

        /// <summary>
        /// Draws the image at the specified position with the given size, angle, position origin, rotation origin,
        /// tint and crop.
        /// </summary>
        /// <param name="position">The position to draw the image at, in pixel.</param>
        /// <param name="size">The size in which to draw the image, in pixel.</param>
        /// <param name="rotation">The rotation angle in radians.</param>
        /// <param name="origin">The origin in the image to which the position is relative, normalized.</param>
        /// <param name="rotationOrigin">The origin in the image around which will be rotated, normalized.</param>
        /// <param name="tint">The colorization of the image, supporting the alpha component.</param>
        /// <param name="crop">The crop determining the part of the texture to be visible, in pixel.</param>
        public void Draw(Vector2? position = null, Vector2? size = null, float rotation = 0f,
            Vector2F? origin = null, Vector2F? rotationOrigin = null, ColorF? tint = null, Rectangle? crop = null)
        {
            // If no size specified, try taking the crop size, otherwise the image size.
            Rectangle finalRectangle = new Rectangle(position ?? Vector2.Zero,
                size ?? (crop.HasValue ? crop.Value.Size : Size));

            VideoManager.SpriteBatch.Draw(Texture, Size, origin ?? Vector2.Zero, finalRectangle,
                rotationOrigin ?? Vector2F.Half, rotation, tint ?? ColorF.White,
                crop ?? new Rectangle(Vector2.Zero, Size));
        }

        /// <summary>
        /// Draws the image at the specified position with the given size, angle, position origin, rotation origin,
        /// tint and crop.
        /// </summary>
        /// <param name="position">The position to draw the image at, in pixel.</param>
        /// <param name="size">The size in which to draw the image, in pixel.</param>
        /// <param name="rotation">The rotation angle in radians.</param>
        /// <param name="origin">The origin in the image to which the position is relative, normalized.</param>
        /// <param name="rotationOrigin">The origin in the image around which will be rotated, normalized.</param>
        /// <param name="tint">The colorization of the image, supporting the alpha component.</param>
        /// <param name="crop">The crop determining the part of the texture to be visible, in pixel.</param>
        public void Draw(Vector2F? position = null, Vector2F? size = null, float rotation = 0f,
            Vector2F? origin = null, Vector2F? rotationOrigin = null, ColorF? tint = null, RectangleF? crop = null)
        {
            // If no size specified, try taking the crop size, otherwise the image size.
            RectangleF finalRectangle = new RectangleF(position ?? Vector2F.Zero,
                size ?? (crop.HasValue ? crop.Value.Size : Size));

            VideoManager.SpriteBatch.Draw(Texture, Size, origin ?? Vector2F.Zero, finalRectangle,
                rotationOrigin ?? Vector2F.Half, rotation, tint ?? ColorF.White,
                crop ?? new RectangleF(Vector2F.Zero, Size));
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release
        /// only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    VideoManager.Unregister(this);
                }
                
                _isDisposed = true;
            }
        }
    }
}
