﻿using System;
using System.Collections.Generic;
using Syroot.Maths;
using Syroot.XRay.Video.BMFont;

namespace Syroot.XRay.Video
{
    /// <summary>
    /// Represents the information needed to draw specific text.
    /// </summary>
    internal class TextRenderInfo
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private static readonly string[] _newLineSeparators = new string[] { Environment.NewLine };

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="TextRenderInfo"/> class providing rendering details required
        /// to draw the provided text.
        /// </summary>
        /// <param name="font">The <see cref="Font"/> used to draw the value.</param>
        /// <param name="value">The text to draw.</param>
        /// <param name="providedSize">The size to which the text will be aligned, or <see cref="Vector2.Zero"/> to not
        /// limit the size.</param>
        /// <param name="horizontalAlignment">The horizontal positioning method of the drawn text.</param>
        /// <param name="verticalAlignment">The vertical positioning method of the drawn text.</param>
        internal TextRenderInfo(Font font, string value, Vector2 providedSize, Alignment horizontalAlignment,
            Alignment verticalAlignment)
        {
            Font = font;
            Value = value;
            ProvidedSize = providedSize;
            HorizontalAlignment = horizontalAlignment;
            VerticalAlignment = verticalAlignment;
            ComputeMappings();
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the <see cref="Font"/> to which rendering information is provided.
        /// </summary>
        internal Font Font
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the text which rendering information is provided.
        /// </summary>
        internal string Value
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the size to which the text will be aligned.
        /// </summary>
        internal Vector2 ProvidedSize
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the horizontal positioning method of the drawn text.
        /// </summary>
        internal Alignment HorizontalAlignment
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the vertical positioning method of the drawn text.
        /// </summary>
        internal Alignment VerticalAlignment
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the size required for rendering the complete text.
        /// </summary>
        internal Vector2 RequiredSize
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the list of <see cref="Rectangle"/> instances to which <see cref="FontChar"/> instances map.
        /// </summary>
        internal List<Rectangle> MappedRectangles
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the list of <see cref="FontChar"/> instances which are mapped.
        /// </summary>
        internal List<FontChar> MappedFontChars
        {
            get;
            private set;
        }
        
        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------
        
        private void ComputeMappings()
        {
            MappedFontChars = new List<FontChar>();
            MappedRectangles = new List<Rectangle>();

            if (!String.IsNullOrEmpty(Value))
            {
                Vector2 requiredSize = new Vector2();

                // Go through each line and character in it.
                string[] lines = Value.Split(_newLineSeparators, StringSplitOptions.None);
                int verticalOffset = GetVerticalAlignOffset(lines.Length);
                foreach (string line in lines)
                {
                    // Start with the first character in the line.
                    int i = 0;
                    int lineX = 0;
                    FontChar fontChar = GetFontChar(line[0]);
                    List<Rectangle> lineMappedRectangles = new List<Rectangle>();

                    // Repeat until no new next characters have been found (set while computing the kerning).
                    while (fontChar != null)
                    {
                        // Get the next character for calculating the kerning.
                        FontChar nextFontChar = null;
                        int kerning = 0;
                        if (i < line.Length - 1)
                        {
                            char nextChar = line[i + 1];
                            nextFontChar = GetFontChar(nextChar);
                            if (nextFontChar != null && nextFontChar.ID != 0/*skip missing characters*/)
                            {
                                kerning = GetFontCharKerning(line[i], nextChar);
                            }
                        }

                        // Add the mappings to the lists.
                        MappedFontChars.Add(fontChar);
                        lineMappedRectangles.Add(new Rectangle(lineX + fontChar.XOffset + kerning,
                            requiredSize.Y + fontChar.YOffset + verticalOffset, fontChar.Width, fontChar.Height));

                        // Advance to the next character in the line.
                        i++;
                        lineX += fontChar.XAdvance;
                        fontChar = nextFontChar;
                    }

                    // Align the characters on the line before adding them to the total collection of all characters.
                    AlignHorizontally(lineX, lineMappedRectangles);
                    MappedRectangles.AddRange(lineMappedRectangles);

                    // Check for a new longer line length.
                    requiredSize.X = Math.Max(requiredSize.X, lineX);
                    // Advance the line position.
                    requiredSize.Y += Font.FontFile.Common.LineHeight;
                }

                RequiredSize = requiredSize;
            }
        }

        private FontChar GetFontChar(char c)
        {
            foreach (FontChar fontChar in Font.FontFile.Chars)
            {
                if (fontChar.ID == c)
                {
                    return fontChar;
                }
            }

            // Log the missing character and return an empty character (so that remainder of line can be parsed).
            Log.WriteWarn(LogCategory.Video, "No char mapping found for '{0}' in font \"{1}\".", c,
                Font.FontFile.Info.Face);
            return new FontChar();
        }

        private int GetFontCharKerning(char currentChar, char nextChar)
        {
            foreach (FontKerning kerning in Font.FontFile.Kernings)
            {
                if (kerning.First == currentChar && kerning.Second == nextChar)
                {
                    return kerning.Amount;
                }
            }

            return 0;
        }

        private int GetVerticalAlignOffset(int lineCount)
        {
            // Get the offset by which each character is translated to align the text vertically.
            if (ProvidedSize.Y > 0)
            {
                if (VerticalAlignment == Alignment.Center)
                    return ProvidedSize.Y / 2 - lineCount * Font.LineHeight / 2;
                else if (VerticalAlignment == Alignment.Far)
                    return ProvidedSize.Y - lineCount * Font.LineHeight;
            }
            return 0;
        }

        private void AlignHorizontally(int lineWidth, List<Rectangle> rectangles)
        {
            if (ProvidedSize.X > 0)
            {
                // Compute the offset for each character.
                int horizontalOffset = 0;
                if (HorizontalAlignment == Alignment.Center)
                    horizontalOffset = ProvidedSize.X / 2 - lineWidth / 2;
                else if (HorizontalAlignment == Alignment.Far)
                    horizontalOffset = ProvidedSize.X - lineWidth;
                // Go through each character and add the offset to its screen mapping.
                for (int j = 0; j < rectangles.Count; j++)
                {
                    Rectangle rectangle = rectangles[j];
                    rectangle.X += horizontalOffset;
                    rectangles[j] = rectangle;
                }
            }
        }
    }
}
