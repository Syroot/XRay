﻿using System;
using OpenTK.Graphics.OpenGL;

namespace Syroot.XRay.Video
{
    /// <summary>
    /// Represents the types of filtering which is applied on a <see cref="OpenGL.Textures.TextureBase{T}"/> if it
    /// is drawn in larger or smaller sizes than the native one.
    /// </summary>
    public enum TextureFiltering
    {
        /// <summary>
        /// No filtering is applied, the output pixelates.
        /// </summary>
        Nearest,

        /// <summary>
        /// Linear filtering is applied, smoothing the output simple.
        /// </summary>
        Linear
    }

    /// <summary>
    /// Represents a collection of static extension methods for <see cref="TextureFiltering"/> objects.
    /// </summary>
    internal static class TextureFilteringExtensions
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------
        
        /// <summary>
        /// Converts the <see cref="TextureFiltering"/> value to a matching one of <see cref="TextureMagFilter"/>.
        /// </summary>
        /// <param name="filtering">The extended <see cref="TextureFiltering"/>.</param>
        /// <returns>The converted <see cref="TextureMagFilter"/>.</returns>
        internal static TextureMagFilter ToTextureMagFilter(this TextureFiltering filtering)
        {
            switch (filtering)
            {
                case TextureFiltering.Nearest:
                    return TextureMagFilter.Nearest;
                case TextureFiltering.Linear:
                    return TextureMagFilter.Linear;
                default:
                    throw new ArgumentOutOfRangeException("filtering");
            }
        }

        /// <summary>
        /// Converts the <see cref="TextureFiltering"/> value to a matching one of <see cref="TextureMinFilter"/>.
        /// </summary>
        /// <param name="filtering">The extended <see cref="TextureFiltering"/>.</param>
        /// <returns>The converted <see cref="TextureMinFilter"/>.</returns>
        internal static TextureMinFilter ToTextureMinFilter(this TextureFiltering filtering)
        {
            switch (filtering)
            {
                case TextureFiltering.Nearest:
                    return TextureMinFilter.Nearest;
                case TextureFiltering.Linear:
                    return TextureMinFilter.Linear;
                default:
                    throw new ArgumentOutOfRangeException("filtering");
            }
        }
    }

    /// <summary>
    /// Represents a collection of static extension methods for <see cref="TextureMagFilter"/> objects.
    /// </summary>
    internal static class TextureMagFilterExtensions
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------
        
        /// <summary>
        /// Converts the <see cref="TextureMagFilter"/> value to a matching one of <see cref="TextureFiltering"/>.
        /// </summary>
        /// <param name="magFilter">The extended <see cref="TextureMagFilter"/>.</param>
        /// <returns>The converted <see cref="TextureFiltering"/>.</returns>
        internal static TextureFiltering ToTextureFiltering(this TextureMagFilter magFilter)
        {
            switch (magFilter)
            {
                case TextureMagFilter.Nearest:
                    return TextureFiltering.Nearest;
                case TextureMagFilter.Linear:
                    return TextureFiltering.Linear;
                default:
                    throw new ArgumentOutOfRangeException("magFilter");
            }
        }
    }

    /// <summary>
    /// Represents a collection of static extension methods for <see cref="TextureMinFilter"/> objects.
    /// </summary>
    internal static class TextureMinFilterExtensions
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------
        
        /// <summary>
        /// Converts the <see cref="TextureMinFilter"/> value to a matching one of <see cref="TextureFiltering"/>.
        /// </summary>
        /// <param name="minFilter">The extended <see cref="TextureMinFilter"/>.</param>
        /// <returns>The converted <see cref="TextureFiltering"/>.</returns>
        internal static TextureFiltering ToTextureFiltering(this TextureMinFilter minFilter)
        {
            switch (minFilter)
            {
                case TextureMinFilter.Nearest:
                    return TextureFiltering.Nearest;
                case TextureMinFilter.Linear:
                    return TextureFiltering.Linear;
                default:
                    throw new ArgumentOutOfRangeException("minFilter");
            }
        }
    }
}
