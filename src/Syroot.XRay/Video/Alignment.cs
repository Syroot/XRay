﻿namespace Syroot.XRay.Video
{
    /// <summary>
    /// Represents the possible text alignments with which text can be positioned.
    /// </summary>
    public enum Alignment
    {
        /// <summary>
        /// Aligns horizontal text on the left and vertical text at the top.
        /// </summary>
        Near,

        /// <summary>
        /// Aligns text centered to the given rectangle.
        /// </summary>
        Center,

        /// <summary>
        /// Aligns horizontal text on the right and vertical text at the bottom.
        /// </summary>
        Far
    }
}
