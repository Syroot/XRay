﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Syroot.Maths;
using Color = Syroot.Maths.Color;

namespace Syroot.XRay.Video.Decoders
{
    /// <summary>
    /// Represents a decoder for image formats supported by .NET, using the System.Drawing namespace.
    /// </summary>
    internal class NetImageDecoder : ImageDecoderBase, IDisposable
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _pixelSize = 4;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Bitmap _bitmap;

        private bool _isDisposed;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="NetImageDecoder"/> class which is used for probing image
        /// formats and checking if the decoder supports them.
        /// </summary>
        internal NetImageDecoder()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NetImageDecoder"/> class with the data read from the given
        /// <see cref="Stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to read the data from.</param>
        internal NetImageDecoder(Stream stream)
        {
            _bitmap = (Bitmap)System.Drawing.Image.FromStream(stream);
            Size = new Vector2(_bitmap.Width, _bitmap.Height);
        }
        
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Reads all pixels into the returned buffer.
        /// </summary>
        /// <returns>The new pixel buffer.</returns>
        internal override Color[,] ReadPixels()
        {
            // Lock the bitmap data in ARGB format.
            BitmapData bitmapData = _bitmap.LockBits(new System.Drawing.Rectangle(Point.Empty, _bitmap.Size),
                ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            Color[,] pixels = new Color[_bitmap.Height, _bitmap.Width];

            unsafe
            {
                // Traverse through it to get the colors.
                byte* dataPointer = (byte*)bitmapData.Scan0;
                for (int y = 0; y < _bitmap.Height; y++)
                for (int x = 0; x < _bitmap.Width;  x++)
                {
                    // Get the RGBA color from the ARGB data.
                    pixels[y, x] = new Color(dataPointer[2], dataPointer[1], dataPointer[0], dataPointer[3]);
                    dataPointer += _pixelSize;
                }
            }

            // Unlock the bitmap data again.
            _bitmap.UnlockBits(bitmapData);

            return pixels;
        }
        
        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------
        
        /// <summary>
        /// Gets a value indicating whether this decoder can load the image data available from the given stream or not.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to read the data from.</param>
        /// <returns>true if this decoder can load the image data; otherwise false.</returns>
        protected override bool CanAccept(Stream stream)
        {
            try
            {
                // We have no better method here to ask if .NET supports the stream.
                System.Drawing.Image.FromStream(stream);
                return true;
            }
            catch
            {
                return false;
            }
        }
        
        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release
        /// only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    _bitmap.Dispose();
                }

                _bitmap = null;

                _isDisposed = true;
            }
        }
    }
}
