﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Syroot.Maths;

namespace Syroot.XRay.Video.Decoders
{
    /// <summary>
    /// Represents the base for all image decoding classes.
    /// </summary>
    internal abstract class ImageDecoderBase : IDisposable
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static List<ImageDecoderBase> _decoders;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------
        
        /// <summary>
        /// Initializes static members of the <see cref="ImageDecoderBase"/> class.
        /// </summary>
        static ImageDecoderBase()
        {
            // Create instances of image decoder classes for probing.
            _decoders = new List<ImageDecoderBase>();
            Type decoderBaseType = typeof(ImageDecoderBase);
            foreach (Type type in Assembly.GetAssembly(decoderBaseType).GetTypes())
            {
                // Check if the type found in the assembly implements this base and isn't the base itself.
                if (decoderBaseType.IsAssignableFrom(type) && decoderBaseType != type)
                {
                    // Instantiate it and remember it in the decoder list.
                    _decoders.Add((ImageDecoderBase)Activator.CreateInstance(type, true));
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageDecoderBase"/> class which is used for probing image
        /// formats and checking if the decoder supports them.
        /// </summary>
        internal ImageDecoderBase() { }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the count of pixels on the X and Y range.
        /// </summary>
        internal Vector2 Size { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Instantiates a correctly typed <see cref="ImageDecoderBase"/> object, which is capable of parsing the data
        /// available from the given <see cref="Stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to read the data from.</param>
        /// <returns><c>true</c> if the data can be loaded with this format; otherwise <c>false</c>.</returns>
        internal static ImageDecoderBase Instantiate(Stream stream)
        {
            foreach (ImageDecoderBase decoder in _decoders)
            {
                // Call the CanAccept method to check if the found decoder can load the image format.
                long previousPosition = stream.Position;
                bool canAccept = decoder.CanAccept(stream);
                stream.Position = previousPosition;

                if (canAccept)
                {
                    // Compatible decoder found, get an instance for delivering the data and return it.
                    return (ImageDecoderBase)Activator.CreateInstance(decoder.GetType(),
                        BindingFlags.NonPublic | BindingFlags.Instance, null, new object[] { stream }, null);
                }
            }

            // No implementation found which supports the image data format.
            throw new ImageDecoderException("Unsupported audio file format.");
        }

        /// <summary>
        /// Reads all pixels into the returned buffer.
        /// </summary>
        /// <returns>The new pixel buffer.</returns>
        internal abstract Color[,] ReadPixels();
        
        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Gets a value indicating whether this decoder can load the image data available from the given stream or not.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to read the data from.</param>
        /// <returns>true if this decoder can load the image data; otherwise false.</returns>
        protected abstract bool CanAccept(Stream stream);
        
        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release
        /// only unmanaged resources.</param>
        protected abstract void Dispose(bool disposing);
    }
}
