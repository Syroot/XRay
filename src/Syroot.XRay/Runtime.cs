﻿using System;
using System.Diagnostics;

namespace Syroot.XRay
{
    /// <summary>
    /// Represents a collection of static methods required for runtime checks.
    /// </summary>
    internal static class Runtime
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Runs the given <see cref="Action"/> if the condition returns <c>false</c>. This action is only run in debug
        /// compilations when the condition returns <c>true</c>.
        /// </summary>
        /// <param name="condition">The condition which should return <c>true</c> for normal behavior.</param>
        /// <param name="action">The <see cref="Action"/> to run if the condition returns <c>false</c>.</param>
        [Conditional("DEBUG")]
        internal static void AssertDebug(bool condition, Action action)
        {
            if (!condition)
            {
                action.Invoke();
            }
        }

        /// <summary>
        /// Raises an <see cref="ArgumentNullException"/> if the given object is <c>null</c>.
        /// </summary>
        /// <param name="obj">The object to check.</param>
        /// <param name="name">The parameter name to display in the <see cref="ArgumentNullException"/>.</param>
        internal static void ThrowIfNull(object obj, string name)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(name);
            }
        }
    }
}
