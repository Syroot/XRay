﻿using System;
using Syroot.Maths;
using Syroot.XRay;
using Syroot.XRay.Audio;
using Syroot.XRay.Input;
using Syroot.XRay.Maths;
using Syroot.XRay.Video;

namespace Syroot.TestGame
{
    /// <summary>
    /// Main class of the program containing the application entry point.
    /// </summary>
    internal class Program
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static ResourceLoader _resources;
        private static Image _imageBackground;
        private static Image _imageRay;
        private static Font _font;
        private static Music _music;
        private static Sound _sound;

        private static Random _random;
        private static ColorF _backgroundTint;
        private static Vector2F _rayOrigin;
        private static Vector2F _raySize;
        private static float _rayRotation;

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void Main(string[] args)
        {
            _resources = new ResourceLoader("Syroot.XRay.TestGame.Resources");

            // Create the game.
            Game.Title = "Test Game";
            Game.Resolution = new Vector2(1024, 768);
            Game.Start += Game_Start;
            Game.Update += Game_Update;
            Game.Render += Game_Render;

            // Run the game.
            Game.Run();
        }

        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

        private static void Game_Start(object sender, EventArgs e)
        {
            _random = new Random();

            VideoManager.ClearColor = ColorF.CornflowerBlue;

            // Create test images.
            _imageBackground = new Image(_resources.GetStream("Background.png"));
            _backgroundTint = ColorF.White;

            _imageRay = new Image(_resources.GetStream("Ray.png"));
            _rayOrigin = Vector2F.Half;
            _raySize = _imageRay.Size;

            // Create test font.
            _font = new Font(_resources.GetStream("Oxygen24Regular1pxOutline.zip"));

            // Create test music.
            _music = new Music(_resources.GetStream("PlayerSelect.ogg")) { LoopSample = 100387 };

            // Create test sounds.
            _sound = new Sound(_resources.GetStream("ButtonClick.wav"));
        }

        private static void Game_Update(object sender, EventArgs e)
        {
            // Exit game (Escape)
            if (Keyboard.IsPressed(Key.Escape))
                Game.Exit();

            // Switch fullscreen / windowed mode (Alt+Enter)
            if ((Keyboard.IsDown(Key.AltLeft) || Keyboard.IsDown(Key.AltRight)) && Keyboard.IsPressed(Key.Enter))
                Game.Fullscreen = !Game.Fullscreen;

            // Music state (L/P/S)
            if (Keyboard.IsPressed(Key.P))
            {
                if (_music.State == AudioState.Playing)
                    _music.Pause();
                else
                    _music.Play();
            }
            if (Keyboard.IsPressed(Key.S))
                _music.Stop();
            if (Keyboard.IsPressed(Key.L))
                _music.IsLooping = !_music.IsLooping;

            // Music pitch (+/-)
            if (Keyboard.IsDown(Key.KeypadPlus))
                _music.Pitch = Math.Min(4f, _music.Pitch + 0.01f);
            if (Keyboard.IsDown(Key.KeypadMinus))
                _music.Pitch = Math.Max(0.01f, _music.Pitch - 0.01f);

            // Background tint, play sound
            if (Mouse.IsPressed(MouseButton.Left))
            {
                _sound.Play();
                _backgroundTint = new ColorF((float)_random.NextDouble(), (float)_random.NextDouble(),
                    (float)_random.NextDouble());
            }

            // Image size (PageUp/Down)
            if (Keyboard.IsDown(Key.PageUp))
                _raySize *= 1.01f;
            if (Keyboard.IsDown(Key.PageDown))
                _raySize /= 1.01f;

            // Image rotation (Home/End)
            if (Keyboard.IsDown(Key.Home))
                _rayRotation -= Algebra.PiOver8 / 8f;
            if (Keyboard.IsDown(Key.End))
                _rayRotation += Algebra.PiOver8 / 8f;

            // Image filtering (*)
            if (Keyboard.IsPressed(Key.KeypadMultiply))
            {
                VideoManager.ImageFiltering = VideoManager.ImageFiltering == TextureFiltering.Linear
                    ? TextureFiltering.Nearest : TextureFiltering.Linear;
            }

            if (Keyboard.IsPressed(Key.F1))
                Game.Resolution = new Vector2(800, 600);
            else if (Keyboard.IsPressed(Key.F2))
                Game.Resolution = new Vector2(1024, 768);
            else if (Keyboard.IsPressed(Key.F3))
                Game.Resolution = new Vector2(500, 500);
        }

        private static void Game_Render(object sender, EventArgs e)
        {
            VideoManager.Clear();

            _imageBackground.Draw(Vector2.Zero, Game.Resolution, tint: _backgroundTint);
            _imageRay.Draw(Mouse.Position, _raySize, _rayRotation, _rayOrigin);

            string text = String.Format("Graphics: {1}x{2}@{3}fps    Audio: Loop={4}    Pitch={5}{0}"
                + "Escape: Exit    Alt+Enter: Fullscreen/Windowed    Keypad*: Image filtering{0}"
                + "Click: Background tint    Home/End: Image rotation    PageUp/Down: Image scaling{0}"
                + "P: Pause/Play music    L: Music loop    S: Stop music    Keypad+/-: Music pitch",
                Environment.NewLine, Game.Resolution.X, Game.Resolution.Y,
                Math.Round(Time.FramesPerSecond), _music.IsLooping, _music.Pitch.ToString("0.00"));
            _font.Draw(text, size: (Vector2F)Game.Resolution, hAlign: Alignment.Center);

            VideoManager.Present();
        }
    }
}