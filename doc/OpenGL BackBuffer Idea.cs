Image bufferImage = new Image(800, 600);
VideoManager.Buffer = bufferImage;
R.DrawStuff();
VideoManager.Present();
VideoManager.Buffer = VideoManager.BackBuffer;
bufferImage.Draw(20, 10);



/*
- Virtual Back Buffer (Image VideoManager.BackBuffer)
  - Created at the start.
  - Needs a dummy depth buffer texture, even if depth is not required.
  - 