# XRay
A very simplistic 2D game library featuring only basic stuff, demonstrating a possible solution on how to abstract away from low level operations like having to work with OpenGL or sound buffers directly.

It currently supports the following:
- Loading and drawing (scaled or rotated) images with OpenGL 3 via OpenTK.
- Playing and streaming WAV and OGG audio files, including support for custom loop samples.
- Font rendering via implementation of the popular BMFont format.

It has only been tested on Windows, but should support Mono aswell due to the nature of OpenTK.
